# Change log

## [tag 1.0.1]

Change structure of adapter :
- rewrite query of AggrParamResponseRepository
- rewrite query of AggrParaRequestRepository
- rewrite query of RepositoryCustomImpl
- rewrite query of StandardParamResponseRepository
- rewrite query of StandardParamRequestRepository

## API CRUD system_config

Coding API crud system config :
- create file
v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/api/SystemConfigController.java                                 
- update file
v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/enums/SystemConfigStatus.java                                  
- create file
v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/SystemConfigModel.java                                   
- update file
v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/request/SystemConfigRequest.java                       
- update file
v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/processor/SystemConfigProcessor.java                        
- update file
v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/SystemConfigRepository.java                          
- update file
v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/service/SystemConfigService.java                              
- update file
v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/transformer/EntitiesMapper.java                                 
- update file
v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/transformer/SystemConfigTransformer.java                       
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/utils/HttpRequestUtil.java

## [tag 1.0.2]

Change structure of adapter :

- rewrite query of AggrParamResponseRepository
- rewrite query of AggrParaRequestRepository
- rewrite query of RepositoryCustomImpl

## Change structure

Change structure :

- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/AggrParamResponseRepository.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/AggrParaRequestRepository.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/StandardParamResponseRepository.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/impl/RepositoryCustomImpl.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/service/AggrParamResponseService.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/service/AggrParamService.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/processor/AggrParamRequestProcessor.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/processor/AggrParamResponseProcessor.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/processor/SystemConfigProcessor.java
- create file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/MapDataRequestModel.java
- create file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/MapDataResponseModel.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/ConfigResponseModel.java
- update file v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/AggrParamRequestModel.java

## Refactor code http client request (2022-07-12 16:00:00)

    - modified: v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/processor/AggrParamRequestProcessor.java
    - modified: v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/AggrParaRequestRepository.java
    - new file: v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/entity/AggrParamRequest.java => more response is data_type

## [tag 1.0.3]

## Change structure of adapter :

- new feature: getCallBackModelResponse
- modified feature: getModelResponse
- new API : getGameProvider

- create file src/main/java/co/arctx/apim/adapter/entity/Agent.java
- update file src/main/java/co/arctx/apim/adapter/entity/AggrEndpoint.java
- create file src/main/java/co/arctx/apim/adapter/entity/GameProvider.java
- update file src/main/java/co/arctx/apim/adapter/entity/StandardEndpoint.java
- create file src/main/java/co/arctx/apim/adapter/entity/StandardEndpointAgent.java
- update file src/main/java/co/arctx/apim/adapter/entity/SystemConfig.java
- create file src/main/java/co/arctx/apim/adapter/entity/SystemConfigGameProvider.java
- update file src/main/java/co/arctx/apim/adapter/model/ConfigResponseModel.java
- create file src/main/java/co/arctx/apim/adapter/model/GameProviderModel.java
- update file src/main/java/co/arctx/apim/adapter/model/StandardEndpointRequestModel.java
- create file src/main/java/co/arctx/apim/adapter/processor/GameProviderProcessor.java
- update file src/main/java/co/arctx/apim/adapter/processor/StandardEndpointProcessor.java
- update file src/main/java/co/arctx/apim/adapter/processor/SystemConfigProcessor.java
- update file src/main/java/co/arctx/apim/adapter/repository/impl/RepositoryCustomImpl.java
- create file src/main/java/co/arctx/apim/adapter/repository/AgentRepository.java
- create file src/main/java/co/arctx/apim/adapter/repository/GameProviderRepository.java
- update file src/main/java/co/arctx/apim/adapter/repository/RepositoryCustom.java
- update file src/main/java/co/arctx/apim/adapter/repository/StandardEndpointRepository.java
- create file src/main/java/co/arctx/apim/adapter/service/GameProviderService.java
- update file src/main/java/co/arctx/apim/adapter/service/StandardEndpointService.java
- update file src/main/java/co/arctx/apim/adapter/service/SystemConfigService.java
- create file src/main/java/co/arctx/apim/adapter/transformer/GameProviderTransformer.java
- update file src/main/resources/schema.sql

## [tag 1.0.4]

## Refactor adapter gamelist method :

- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/entity/GameList.java
- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/GameListModel.java
- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/GameListTempModel.java
- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/SystemConfigGameProviderModel.java
- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/processor/GameListProcessor.java
- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/GameListRepository.java
- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/SystemConfigGameProviderRepository.java
- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/service/GameListService.java
- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/service/SystemConfigGameProviderService.java
- new file:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/transformer/GameListTransformer.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/api/internal/AdapterInternalApi.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/entity/GameList.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/entity/GameProvider.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/entity/SystemConfigGameProvider.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/GameListModel.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/GameListTempModel.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/SystemConfigGameProviderModel.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/processor/GameListProcessor.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/GameListRepository.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/GameProviderRepository.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/SystemConfigGameProviderRepository.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/service/GameListService.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/service/SystemConfigGameProviderService.java
- modified:   v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/transformer/GameListTransformer.java
- modified:   v1.0_apim-adapter/src/main/resources/schema.sql

create schema `apim_adapter` default character set utf8 collate utf8_unicode_ci;

use
apim_adapter;

create table standard_endpoint
(
    id           varchar(255)  not null
        primary key,
    endpoint     varchar(255) null,
    created_by   varchar(255) null,
    created_date datetime null,
    update_by    varchar(255) null,
    update_date  datetime null,
    is_call_back int default 0 not null,
    constraint endpoint_index
        unique (endpoint)
);

CREATE TABLE aggr_endpoint
(
    id           VARCHAR(255) NOT NULL,
    endpoint     VARCHAR(255) NULL,
    method       VARCHAR(255) NULL,
    created_by   VARCHAR(255) NULL,
    created_date datetime NULL,
    update_by    VARCHAR(255) NULL,
    updated_date datetime NULL,
    CONSTRAINT pk_aggrendpoint PRIMARY KEY (id)
);

create table system_config
(
    id           varchar(255) not null
        primary key,
    system_name  varchar(255) null,
    base_url     varchar(255) null,
    created_by   varchar(255) null,
    created_date datetime null,
    updated_by   varchar(255) null,
    updated_date datetime null,
    status       int default 1 null,
    description  varchar(255) null,
    token        varchar(255) null
)

CREATE TABLE standard_param_request
(
    id                     VARCHAR(255) NOT NULL,
    standard_param_request VARCHAR(255) NULL,
    standard_endpoint_id   VARCHAR(255) NULL,
    parent_id              VARCHAR(255) NULL,
    created_by             VARCHAR(255) NULL,
    created_date           datetime NULL,
    update_by              VARCHAR(255) NULL,
    update_date            datetime NULL,
    CONSTRAINT pk_standardparamrequest PRIMARY KEY (id)
);

CREATE TABLE standard_param_response
(
    id                      VARCHAR(255) NOT NULL,
    standard_param_response VARCHAR(255) NULL,
    standard_endpoint_id    VARCHAR(255) NULL,
    parent_id               VARCHAR(255) NULL,
    created_by              VARCHAR(255) NULL,
    created_date            datetime NULL,
    updated_by              VARCHAR(255) NULL,
    updated_date            datetime NULL,
    CONSTRAINT pk_standardparamresponse PRIMARY KEY (id)
);

CREATE TABLE aggr_param_request
(
    id                 VARCHAR(255) NOT NULL,
    aggr_param_request VARCHAR(255) NULL,
    object_type        VARCHAR(255) NULL,
    aggr_endpoint_id   VARCHAR(255) NULL,
    parent_id          VARCHAR(255) NULL,
    created_by         VARCHAR(255) NULL,
    created_date       datetime NULL,
    update_by          VARCHAR(255) NULL,
    update_date        datetime NULL,
    data_type          VARCHAR(255) NULL,
    CONSTRAINT pk_aggrparamrequest PRIMARY KEY (id)
);

CREATE TABLE aggr_param_response
(
    id                  VARCHAR(255) NOT NULL,
    aggr_param_response VARCHAR(255) NULL,
    object_type         VARCHAR(255) NULL,
    aggr_endpoint_id    VARCHAR(255) NULL,
    parent_id           VARCHAR(255) NULL,
    created_by          VARCHAR(255) NULL,
    created_date        datetime NULL,
    updated_by          VARCHAR(255) NULL,
    updated_date        datetime NULL,
    CONSTRAINT pk_aggrparamresponse PRIMARY KEY (id)
);

CREATE TABLE standard_endpoint_system_config
(
    id                   BIGINT AUTO_INCREMENT NOT NULL,
    standard_endpoint_id VARCHAR(255) NULL,
    system_config_id     VARCHAR(255) NULL,
    aggr_endpoint_id     VARCHAR(255) NULL,
    CONSTRAINT pk_standardendpointsystemconfig PRIMARY KEY (id)
);

CREATE TABLE standard_param_aggr_param_request
(
    id                BIGINT AUTO_INCREMENT NOT NULL,
    standard_param_id VARCHAR(255) NULL,
    aggr_param_id     VARCHAR(255) NULL,
    CONSTRAINT pk_standardparamaggrparamrequest PRIMARY KEY (id)
);

CREATE TABLE standard_param_aggr_param_response
(
    id                         BIGINT AUTO_INCREMENT NOT NULL,
    standard_param_response_id VARCHAR(255) NULL,
    aggr_param_response_id     VARCHAR(255) NULL,
    CONSTRAINT pk_standardparamaggrparamresponse PRIMARY KEY (id)
);

create table agent
(
    id           varchar(255) not null
        primary key,
    agent_name   varchar(255) null,
    token        varchar(255) null,
    url          varchar(255) null,
    description  varchar(255) null,
    created_by   varchar(255) null,
    created_date datetime null,
    updated_by   varchar(255) null,
    updated_date date null,
    status       int default 1 null
);
CREATE TABLE game_provider
(
    id            VARCHAR(255) NOT NULL,
    provider_name VARCHAR(255) NULL,
    created_by    VARCHAR(255) NULL,
    created_date  datetime NULL,
    updated_by    VARCHAR(255) NULL,
    updated_date  datetime NULL,
    CONSTRAINT pk_gameprovider PRIMARY KEY (id)
);

create table standard_endpoint_agent
(
    id                   bigint auto_increment
        primary key,
    standard_endpoint_id varchar(255) null,
    agent_id             varchar(255) null,
    aggr_endpoint        varchar(255) null
);

CREATE TABLE system_config_game_provider
(
    id               BIGINT AUTO_INCREMENT NOT NULL,
    status           INT NULL,
    expiry_date      VARCHAR(255) NULL,
    created_by       VARCHAR(255) NULL,
    created_date     datetime NULL,
    updated_by       VARCHAR(255) NULL,
    updated_date     datetime NULL,
    system_config_id VARCHAR(255) NULL,
    game_provider_id VARCHAR(255) NULL,
    CONSTRAINT pk_systemconfiggameprovider PRIMARY KEY (id)
);

CREATE TABLE game_list
(
    id                             VARCHAR(255) NOT NULL,
    game_list                      text,
    system_config_game_provider_id BIGINT NULL,
    CONSTRAINT pk_gamelist PRIMARY KEY (id)
);

package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * @author: Nam Vu
 * Jul 19, 2022
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SystemConfigGameProvider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Integer status;
    String expiryDate;
    String createdBy;
    String createdDate;
    String updatedBy;
    String updatedDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "system_config_id")
    SystemConfig systemConfig;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "game_provider_id")
    GameProvider gameProvider;

    @OneToOne(mappedBy = "sysConfigGameProvider")
    GameList gameList;
}

package co.arctx.apim.adapter.transformer;

import co.arctx.apim.adapter.entity.StandardParamResponse;
import co.arctx.apim.adapter.model.StandardParamResponseModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author: Nam Vu
 * Jun 27, 2022
 */
@Mapper(componentModel = "spring")
public interface StandardParamResponseTransformer extends EntitiesMapper<StandardParamResponse, StandardParamResponseModel> {

    @Override
    @Mapping(target = "parentId", source = "standardParamResponse.id")
    StandardParamResponseModel toModel(StandardParamResponse standardParamResponse);

    @Override
    @Mapping(target = "parentId.id", source = "parentId")
    StandardParamResponse toEntity(StandardParamResponseModel standardParamResponseModel);
}

package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.model.ConfigResponseModel;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Repository
public interface RepositoryCustom {

    ConfigResponseModel getModelResponse(String endpoint,String token);

    ConfigResponseModel getCallBackModelResponse(String endpoint,String token);

}

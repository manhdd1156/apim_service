package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.AggrParamRequest;
import co.arctx.apim.adapter.model.MapDataRequestModel;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public interface AggrParaRequestRepository extends CommonRepository<AggrParamRequest, String> {


    @Query("SELECT new co.arctx.apim.adapter.model.MapDataRequestModel(apr.aggrParamRequest,apr.objectType, apr.dataType, spr.standardParamRequest) FROM SystemConfig sc " +
            "JOIN StandardEndpointSystemConfig sesc on sc.id = sesc.systemConfig " +
            "JOIN StandardEndpoint se on se.id=sesc.standardEndpoint " +
            "JOIN StandardParamRequest spr on spr.standardEndpointId=se.id " +
            "JOIN StandardParamAggrParamRequest spap on spap.standardParamRequest=spr.id " +
            "JOIN AggrParamRequest apr on apr.id=spap.aggrParamRequest " +
            "WHERE se.endpoint = ?1 and sc.status =1 and apr.aggrEndpointId.id=?2")
    List<MapDataRequestModel> getAggrParamRequestByEndpoint(String endpoint, String aggrEndpointId);


}

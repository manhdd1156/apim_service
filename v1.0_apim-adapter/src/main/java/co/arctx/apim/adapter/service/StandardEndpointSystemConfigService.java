package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.StandardEndpointSystemConfig;
import co.arctx.apim.adapter.repository.StandardEndpointSystemConfigRepository;
import co.arctx.apim.adapter.utils.Message;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Service
public class StandardEndpointSystemConfigService extends CommonService<StandardEndpointSystemConfig, Long, StandardEndpointSystemConfigRepository> {


    protected StandardEndpointSystemConfigService(StandardEndpointSystemConfigRepository repo) {
        super(repo);
    }


    public List<StandardEndpointSystemConfig> getAllStandardEndpointSystemConfig() {
        return all(Sort.Order.asc("id"));
    }

    public StandardEndpointSystemConfig getById(Long id) throws NotFoundEntityException {
        return getOrElseThrow(id);
    }

    public StandardEndpointSystemConfig saveStandardEndpointSystemConfig(StandardEndpointSystemConfig systemConfig) {
        return save(systemConfig);
    }

    public List<StandardEndpointSystemConfig> saveStandardEndpointSystemConfig(List<StandardEndpointSystemConfig> systemConfigs) {
        return save(systemConfigs);
    }

    public StandardEndpointSystemConfig updateStandardEndpointSystemConfig(StandardEndpointSystemConfig systemConfig) throws NotFoundEntityException {
        return updateOnField(systemConfig.getId(), e -> {
            e.setStandardEndpoint(systemConfig.getStandardEndpoint());
            e.setSystemConfig(systemConfig.getSystemConfig());
        });
    }

    public StandardEndpointSystemConfig deleteStandardEndpoint(Long id) {
        return deleteIfExisted(id);
    }

    @Override
    protected String notFoundMessage() {
        return Message.STANDARD_ENDPOINT_SYSTEM_CONFIG_NOT_FOUND;
    }
}

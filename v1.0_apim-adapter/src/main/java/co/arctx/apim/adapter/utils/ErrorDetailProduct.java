package co.arctx.apim.adapter.utils;

import co.arctx.core.model.ErrorDetail;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.stream.Collectors;
/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Component
public class ErrorDetailProduct {

    private ErrorDetailProduct() {
    }


    public List<ErrorDetail> getErrorDetail(BindingResult bindingResult) {
        return bindingResult.getFieldErrors()
                .stream()
                .map(fieldError ->
                        ErrorDetail.create(fieldError.getField(), fieldError.getDefaultMessage())).collect(Collectors.toList());
    }
}

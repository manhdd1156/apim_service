package co.arctx.apim.adapter.annotations.impl;

import co.arctx.apim.adapter.annotations.ValidDomain;
import org.springframework.util.ObjectUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public class DomainValidator implements ConstraintValidator<ValidDomain,String> {

    private static final String DOMAIN_REGEX ="^(?:https?:\\/\\/)?(?:[^@\\/\\n]+@)?(?:www\\.)?([^:\\/\\n]+)";
    @Override
    public void initialize(ValidDomain constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if(ObjectUtils.isEmpty(s)){
            return false;
        }
        return Pattern.compile(DOMAIN_REGEX).matcher(s).matches();
    }
}

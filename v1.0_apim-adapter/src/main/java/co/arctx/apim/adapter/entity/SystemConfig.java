package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SystemConfig {
    @Id
    String id;
    String systemName;
    String baseUrl;
    String aggToken;
    String createdBy;
    LocalDateTime createdDate;
    String updatedBy;
    LocalDateTime updatedDate;
    Integer status;
    String description;

    @OneToMany(mappedBy = "systemConfig")
    List<StandardEndpointSystemConfig> systemConfigs;

    @OneToMany(mappedBy = "systemConfig")
    List<SystemConfigGameProvider> gameProviders;
}

package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.StandardEndpoint;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public interface StandardEndpointRepository extends CommonRepository<StandardEndpoint, String> {

    @Query("SELECT s.id FROM StandardEndpoint s WHERE s.endpoint=?1 ")
    Long findStandardEndpointIdByEnpointAndToken(String endpoint);



}

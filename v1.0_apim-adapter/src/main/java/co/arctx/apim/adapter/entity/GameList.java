package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * @author: Nam Vu
 * Jul 22, 2022
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameList {

    @Id
    String id;
    String gameList;

    @OneToOne
    @JoinColumn(name = "system_config_game_provider_id")
    SystemConfigGameProvider sysConfigGameProvider;

}

package co.arctx.apim.adapter.processor;

import co.arctx.apim.adapter.entity.GameList;
import co.arctx.apim.adapter.entity.QGameList;
import co.arctx.apim.adapter.entity.SystemConfigGameProvider;
import co.arctx.apim.adapter.model.GameListAdapterModel;
import co.arctx.apim.adapter.model.GameListModel;
import co.arctx.apim.adapter.model.SystemConfigGameProviderModel;
import co.arctx.apim.adapter.service.GameListService;
import co.arctx.apim.adapter.service.SystemConfigGameProviderService;
import co.arctx.apim.adapter.transformer.GameListTransformerImpl;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.utils.JsonMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: Nam Vu
 * Jul 22, 2022
 */

@Component
public class GameListProcessor extends CommonProcessor<GameListService, QGameList, GameListTransformerImpl> {

    private final SystemConfigGameProviderService configGameProviderService;

    protected GameListProcessor(GameListService service, GameListTransformerImpl transformer, SystemConfigGameProviderService configGameProviderService) {
        super(QGameList.gameList1, service, transformer);
        this.configGameProviderService = configGameProviderService;
    }

    public GameListModel saveGameList(GameList gameList) {
        return transformer.toModel(service.save(gameList));
    }


    public boolean saveGameList(List<GameListAdapterModel> gameList) {
        List<Long> systemProviderIds = gameList.stream().map(GameListAdapterModel::getSystemProviderId).collect(Collectors.toList());
        List<GameList> inIds = service.findInSystemProviderIds(systemProviderIds);
        List<SystemConfigGameProvider> configGameProviders = configGameProviderService.findInIds(systemProviderIds);
        gameList.forEach(g -> {
            if (!Objects.isNull(g.getId())) {
                inIds.forEach(s -> {
                    if (s.getId().equals(g.getId())) {
                        s.setGameList(g.getGameList());
                    }
                });
            } else {
                configGameProviders.forEach(c -> {
                    if (g.getSystemProviderId().equals(c.getId())) {
                        GameList gameListSave = new GameList(UUID.randomUUID().toString(), g.getGameList(), c);
                        inIds.add(gameListSave);
                    }
                });
            }
        });
        return !service.saveGameList(inIds).isEmpty();
    }

    public List<Object> findGameListAvailable() {
        List<GameList> gameListAvailable = service.findGameListAvailable();
        List<Object> listGame = new ArrayList<>();
        gameListAvailable.stream().forEach(g ->
                listGame.addAll(JsonMapper.read(g.getGameList(), new TypeReference<List<Object>>() {
                }))
        );
        return listGame;
    }
}


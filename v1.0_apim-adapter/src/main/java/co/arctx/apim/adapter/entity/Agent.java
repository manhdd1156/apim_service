package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * @author: Nam Vu
 * Jul 19, 2022
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Agent {
    @Id
    String id;
    String agentName;
    String agentToken;
    String url;
    Integer status;
    String description;
    String createdBy;
    String createdDate;
    String updatedBy;
    String updatedDate;

    @OneToMany(mappedBy = "agent")
    List<StandardEndpointAgent> standardEndpointAgent;
}

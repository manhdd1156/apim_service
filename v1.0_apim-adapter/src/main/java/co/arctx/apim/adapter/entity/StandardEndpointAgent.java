package co.arctx.apim.adapter.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.mapping.Join;

import javax.persistence.*;

/**
 * @author: Nam Vu
 * Jul 19, 2022
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StandardEndpointAgent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "standard_endpoint_id")
    StandardEndpoint standardEndpoint;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id")
    Agent agent;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aggr_endpoint")
    AggrEndpoint aggrEndpoint;
}

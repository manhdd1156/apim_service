package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StandardEndpoint {

    @Id
    String id;
    String endpoint;
    @Column(length = 1)
    Integer isCallBack;
    String createdBy;
    LocalDateTime createdDate;
    String updateBy;
    LocalDateTime updateDate;
    @OneToMany(mappedBy = "standardEndpoint")
    List<StandardEndpointSystemConfig> configs;

    @OneToMany(mappedBy = "standardEndpointId")
    List<StandardParamRequest> standardParamRequests;

    @OneToMany(mappedBy = "standardEndpoint")
    List<StandardParamResponse> standardParamResponses;

    @OneToMany(mappedBy = "standardEndpoint")
    List<StandardEndpointAgent> endpointAgents;
}

package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.AggrParamRequest;
import co.arctx.apim.adapter.model.MapDataRequestModel;
import co.arctx.apim.adapter.repository.AggrParaRequestRepository;
import co.arctx.apim.adapter.utils.Message;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Service
public class AggrParamService extends CommonService<AggrParamRequest, String, AggrParaRequestRepository> {


    protected AggrParamService(AggrParaRequestRepository repo) {
        super(repo);
    }


    public List<MapDataRequestModel> getAggrParamByEndpointId(String endpoint, String aggrEndpointId) {
        return repo.getAggrParamRequestByEndpoint(endpoint, aggrEndpointId);
    }


    public List<AggrParamRequest> findAllAggrParam() {
        return all(Sort.Order.asc("aggrParamRequest"));
    }

    public AggrParamRequest findAggrParamByIds(String id) throws NotFoundEntityException {
        return getOrElseThrow(id);
    }

    public List<AggrParamRequest> saveAggrParam(List<AggrParamRequest> aggrParamRequests) {
        return save(aggrParamRequests);
    }

    public AggrParamRequest saveAggrParam(AggrParamRequest aggrParamRequest) {
        return save(aggrParamRequest);
    }

    public AggrParamRequest updateAggrParam(AggrParamRequest aggrParamRequest) throws NotFoundEntityException {
        return updateOnField(aggrParamRequest.getId(), e -> {
            e.setAggrParamRequest(aggrParamRequest.getAggrParamRequest());
            e.setAggrEndpointId(aggrParamRequest.getAggrEndpointId());
            e.setParentId(aggrParamRequest.getParentId());
            e.setUpdateBy(aggrParamRequest.getUpdateBy());
            e.setUpdateDate(LocalDateTime.now());
        });
    }

    public AggrParamRequest deleteAggrParamById(String id) {
        return deleteIfExisted(id);
    }


    @Override
    protected String notFoundMessage() {
        return Message.AGGR_PARAM_NOT_FOUND;
    }
}

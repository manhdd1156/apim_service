package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Entity
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class StandardEndpointSystemConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "standard_endpoint_id")
    StandardEndpoint standardEndpoint;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "system_config_id")
    SystemConfig systemConfig;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aggr_endpoint_id")
    AggrEndpoint endpoint;

}

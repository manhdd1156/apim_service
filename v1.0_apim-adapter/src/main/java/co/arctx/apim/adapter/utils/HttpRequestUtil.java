package co.arctx.apim.adapter.utils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;


/**
 * @author: quangdtptit
 * Jul 04, 2022
 */
@Slf4j
public final class HttpRequestUtil {

    private HttpRequestUtil() {

    }

    public static final String DEFAULT_USERNAME = "system";

    public static HttpServletRequest getCurrentHttpRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            return ((ServletRequestAttributes) requestAttributes).getRequest();
        }
        log.debug("Not called in the context of an HTTP request");
        return null;
    }

    public static String getUserName() {
        HttpServletRequest currentRequest = getCurrentHttpRequest();
        if (currentRequest != null) {
            final String requestTokenHeader = currentRequest.getHeader(HttpHeaders.AUTHORIZATION);
            if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
                String jwtToken = requestTokenHeader.substring(7);
                String username = getUserName(jwtToken);
                if (StringUtils.isNotBlank(username))
                    return username;
            }
        }
        return DEFAULT_USERNAME;
    }

    public static String getUserName(String bearerToken) {
        try {
            String[] splitString = bearerToken.split("\\.");
            String base64EncodedBody = splitString[1];
            String body = new String(Base64.getDecoder().decode(base64EncodedBody));
            UserInfo userInfo = jsonToObj(body, UserInfo.class);
            if (userInfo != null)
                return userInfo.getUsername();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }

    public static <T> T jsonToObj(String json, Class<T> clazz) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class UserInfo {

        private String username;
    }

}

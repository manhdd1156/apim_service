package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.StandardParamAggrParamRequest;
import co.arctx.core.common.CommonRepository;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public interface StandardParamAggrParamRequestRepository extends CommonRepository<StandardParamAggrParamRequest, Long> {
}

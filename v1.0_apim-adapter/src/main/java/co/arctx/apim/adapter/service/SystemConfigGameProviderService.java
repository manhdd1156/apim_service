package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.SystemConfigGameProvider;
import co.arctx.apim.adapter.model.SystemConfigGameProviderModel;
import co.arctx.apim.adapter.repository.SystemConfigGameProviderRepository;
import co.arctx.core.common.CommonService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Nam Vu
 * Jul 22, 2022
 */
@Service
public class SystemConfigGameProviderService extends CommonService<SystemConfigGameProvider, Long, SystemConfigGameProviderRepository> {

    protected SystemConfigGameProviderService(SystemConfigGameProviderRepository repo) {
        super(repo);
    }

    public List<SystemConfigGameProviderModel> findSystemConfigGameProviderModels() {
        return repo.findSystemConfigGameProviderModels();
    }
    public List<SystemConfigGameProvider> findInIds(List<Long> ids){
        return repo.findInIds(ids);
    }

    @Override
    protected String notFoundMessage() {
        return null;
    }
}

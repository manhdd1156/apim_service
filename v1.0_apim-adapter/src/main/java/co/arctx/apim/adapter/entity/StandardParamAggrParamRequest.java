package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StandardParamAggrParamRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "standard_param_id")
    StandardParamRequest standardParamRequest;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aggr_param_id")
    AggrParamRequest aggrParamRequest;

}

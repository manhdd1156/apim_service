package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.SystemConfig;
import co.arctx.core.common.CommonRepository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public interface SystemConfigRepository extends CommonRepository<SystemConfig, String> {

    @Query("SELECT s FROM SystemConfig s LEFT JOIN s.systemConfigs sm LEFT JOIN sm.standardEndpoint st WHERE st.endpoint=?1 AND s.status=1")
    SystemConfig findSystemConfigByStandardEndpointId(String endpoint);


    @Query("SELECT sys FROM SystemConfig sys WHERE sys.baseUrl = :baseUrl AND (:id IS NULL OR sys.id <> :id)")
    Optional<SystemConfig> findByBaseUrl(String baseUrl, String id);

    @Transactional
    @Modifying
    @Query("UPDATE SystemConfig SET status = 0 WHERE id <> :idSystemConfigActive")
    void updateDisable(String idSystemConfigActive);

    @Query("SELECT sc FROM SystemConfig sc WHERE sc.aggToken=?1")
    SystemConfig findSystemConfigByToken(String token);

    @Query("SELECT sc FROM SystemConfig  sc WHERE sc.status=1")
    List<SystemConfig> getAllSystemConfigActive();
}

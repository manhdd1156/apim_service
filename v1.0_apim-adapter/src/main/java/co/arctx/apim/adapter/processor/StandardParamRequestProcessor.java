package co.arctx.apim.adapter.processor;

import co.arctx.apim.adapter.entity.QStandardParamRequest;
import co.arctx.apim.adapter.entity.StandardParamRequest;
import co.arctx.apim.adapter.model.StandardParamRequestModel;
import co.arctx.apim.adapter.service.StandardParamRequestService;
import co.arctx.apim.adapter.transformer.StandardParamRequestTransformerImpl;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.jwt.JwtReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */

@Component
@Slf4j
public class StandardParamRequestProcessor extends CommonProcessor<StandardParamRequestService, QStandardParamRequest, StandardParamRequestTransformerImpl> {


    protected StandardParamRequestProcessor(StandardParamRequestService service, StandardParamRequestTransformerImpl transformer) {
        super(QStandardParamRequest.standardParamRequest1, service, transformer);
    }

    List<StandardParamRequest> findStandardParamRequestByIds(List<String> ids) {
        return service.findStandardParamRequestByIds(ids);
    }

    public StandardParamRequestModel getDetail(String id) {
        log.info("Get Detail Standard Param Id {}", id);
        try {
            return transformer.toModel(service.findById(id));
        } catch (NotFoundEntityException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public List<StandardParamRequestModel> getDetail() {
        log.info("Get detail of all Standard param");
        List<StandardParamRequestModel> standardParamRequestModels = transformer.toModels(service.findAllStandardParam());
        log.info("Success");
        return standardParamRequestModels;
    }


}

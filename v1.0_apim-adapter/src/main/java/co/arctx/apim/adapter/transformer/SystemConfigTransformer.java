package co.arctx.apim.adapter.transformer;

import co.arctx.apim.adapter.entity.SystemConfig;
import co.arctx.apim.adapter.model.SystemConfigModel;
import co.arctx.apim.adapter.model.request.SystemConfigRequest;
import org.mapstruct.*;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SystemConfigTransformer extends EntitiesMapper<SystemConfig, SystemConfigModel> {

    @Mapping(target = "createdDate", expression = "java(java.time.LocalDateTime.now())")
    @Mapping(target = "createdBy", expression = "java(co.arctx.apim.adapter.utils.HttpRequestUtil.getUserName())")
    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID().toString())")
    @Mapping(target = "status", constant = "0")
    SystemConfig toSystemConfig(SystemConfigRequest systemConfigRequest);

    @Mapping(target = "updatedDate", expression = "java(java.time.LocalDateTime.now())")
    @Mapping(target = "updatedBy", expression = "java(co.arctx.apim.adapter.utils.HttpRequestUtil.getUserName())")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void partialUpdate(@MappingTarget SystemConfig entity, SystemConfigRequest dto);

}

package co.arctx.apim.adapter.transformer;

import co.arctx.apim.adapter.entity.Agent;
import co.arctx.apim.adapter.model.AgentModel;
import org.mapstruct.Mapper;

/**
 * @author: Nam Vu
 * Jul 20, 2022
 */

@Mapper(componentModel = "spring")
public interface AgentTransformer extends EntitiesMapper<Agent, AgentModel> {
}

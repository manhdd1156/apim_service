package co.arctx.apim.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@SpringBootApplication(scanBasePackages = {
        "co.arctx.core",
        "co.arctx.apim.adapter"
})
@EnableFeignClients
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}

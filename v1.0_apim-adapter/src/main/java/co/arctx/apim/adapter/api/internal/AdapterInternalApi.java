package co.arctx.apim.adapter.api.internal;

import co.arctx.apim.adapter.entity.GameList;
import co.arctx.apim.adapter.model.GameListAdapterModel;
import co.arctx.apim.adapter.model.SystemConfigGameProviderModel;
import co.arctx.apim.adapter.model.SystemConfigModel;
import co.arctx.apim.adapter.processor.GameListProcessor;
import co.arctx.apim.adapter.processor.SystemConfigProcessor;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@RestController
@RequestMapping("/internal/api/v1/adapters")
@AllArgsConstructor
public class AdapterInternalApi {

    private final GameListProcessor gameListProcessor;

    private final SystemConfigProcessor systemConfigProcessor;

    @PostMapping("/create-game-list")
    public ResponseEntity<Object> createGameList(@RequestBody List<GameListAdapterModel> listGame) {
        return ResponseEntity.ok().body(gameListProcessor.saveGameList(listGame));
    }

    @GetMapping("/get-game-list")
    public ResponseEntity<Object> getGameList() {
        return ResponseEntity.ok(gameListProcessor.findGameListAvailable());
    }

    @GetMapping("/get-system-provider-available")
    public ResponseEntity<List<SystemConfigGameProviderModel>> getSystemProviderAvailable() {
        return ResponseEntity.ok(systemConfigProcessor.getSystemProviderActive());
    }
}

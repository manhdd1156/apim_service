package co.arctx.apim.adapter.transformer;

import co.arctx.apim.adapter.entity.GameList;
import co.arctx.apim.adapter.model.GameListModel;
import org.mapstruct.Mapper;

/**
 * @author: Nam Vu
 * Jul 22, 2022
 */
@Mapper(componentModel = "spring")
public interface GameListTransformer extends EntitiesMapper<GameList, GameListModel> {
}

package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.SystemConfigGameProvider;
import co.arctx.apim.adapter.model.SystemConfigGameProviderModel;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: Nam Vu
 * Jul 22, 2022
 */

public interface SystemConfigGameProviderRepository extends CommonRepository<SystemConfigGameProvider, Long> {

    @Query("SELECT new co.arctx.apim.adapter.model.SystemConfigGameProviderModel(scgp.id,scgp.systemConfig.id,scgp.gameProvider.id,gp.providerName, gl.id) " +
            "FROM SystemConfigGameProvider scgp " +
            "JOIN GameProvider gp on scgp.gameProvider=gp.id " +
            "LEFT JOIN GameList gl on scgp.id= gl.sysConfigGameProvider.id " +
            "WHERE scgp.status=1 AND scgp.expiryDate > CURRENT_DATE")
    List<SystemConfigGameProviderModel> findSystemConfigGameProviderModels();


    @Query("SELECT scgp FROM SystemConfigGameProvider scgp WHERE scgp.id in ?1")
    List<SystemConfigGameProvider> findInIds(List<Long> ids);
}

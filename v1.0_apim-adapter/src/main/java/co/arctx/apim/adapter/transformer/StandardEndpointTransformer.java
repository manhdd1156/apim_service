package co.arctx.apim.adapter.transformer;

import co.arctx.apim.adapter.entity.StandardEndpoint;
import co.arctx.apim.adapter.model.StandardEndpointModel;
import org.mapstruct.Mapper;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */

@Mapper(componentModel = "spring")
public interface StandardEndpointTransformer extends EntitiesMapper<StandardEndpoint, StandardEndpointModel> {
}

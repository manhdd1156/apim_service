package co.arctx.apim.adapter.api.internal;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */

@RestController
@RequestMapping("/internal/api/v1/adapters")
@AllArgsConstructor
public class AdapterInternalController {







}

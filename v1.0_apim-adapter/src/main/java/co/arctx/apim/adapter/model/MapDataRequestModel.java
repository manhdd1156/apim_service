package co.arctx.apim.adapter.model;

import co.arctx.core.common.DataType;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jul 06, 2022
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MapDataRequestModel {
    String aggrParamRequest;
    String aggrObjectType;
    DataType aggrDataType;
    String standardParamRequest;
}

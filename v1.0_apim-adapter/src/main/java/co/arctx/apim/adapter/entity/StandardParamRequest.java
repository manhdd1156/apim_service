package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StandardParamRequest {

    @Id
    String id;
    String standardParamRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "standard_endpoint_id")
    StandardEndpoint standardEndpointId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    StandardParamRequest parentId;
    String createdBy;
    LocalDateTime createdDate;
    String updateBy;
    LocalDateTime updateDate;

    @OneToMany(mappedBy = "standardParamRequest")
    List<StandardParamAggrParamRequest> aggrParams;

}

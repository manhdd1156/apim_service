package co.arctx.apim.adapter.repository.impl;

import co.arctx.apim.adapter.model.ConfigResponseModel;
import co.arctx.apim.adapter.repository.RepositoryCustom;
import co.arctx.core.exception.NotFoundEntityException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Optional;


/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Component
@RequiredArgsConstructor
public class RepositoryCustomImpl implements RepositoryCustom {

    private final EntityManager entityManager;

    @Override
    public ConfigResponseModel getModelResponse(String endpoint, String token) {
        String query = "SELECT new co.arctx.apim.adapter.model.ConfigResponseModel(sc.baseUrl,ae.endpoint,ae.method,ae.id,sc.aggToken,sc.systemName)FROM SystemConfig sc JOIN StandardEndpointSystemConfig sesc ON sc.id=sesc.systemConfig " +
                "JOIN AggrEndpoint ae ON sesc.endpoint=ae.id JOIN StandardEndpoint se ON sesc.standardEndpoint = se.id " +
                "WHERE sc.aggToken=:token AND sc.status=1 AND se.endpoint=:endpoint";
        return entityManager.createQuery(query, ConfigResponseModel.class)
                .setParameter("endpoint", endpoint)
                .setParameter("token", token)
                .getSingleResult();

    }

    @Override
    public ConfigResponseModel getCallBackModelResponse(String endpoint, String token) {
        String query = "SELECT new co.arctx.apim.adapter.model.ConfigResponseModel(ag.url,ae.endpoint,ae.method,ae.id,ag.agentName) FROM Agent ag " +
                "JOIN StandardEndpointAgent sea on ag.id=sea.agent " +
                "JOIN StandardEndpoint se on sea.standardEndpoint=se.id " +
                "JOIN AggrEndpoint ae on sea.aggrEndpoint = ae.id " +
                "WHERE ag.agentToken=:token AND ag.status= 1 AND se.endpoint=:endpoint";
        return entityManager.createQuery(query, ConfigResponseModel.class)
                .setParameter("token", token)
                .setParameter("endpoint", endpoint)
                .getSingleResult();
    }


}

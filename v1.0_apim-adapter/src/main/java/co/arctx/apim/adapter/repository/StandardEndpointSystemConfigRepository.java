package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.StandardEndpointSystemConfig;
import co.arctx.core.common.CommonRepository;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public interface StandardEndpointSystemConfigRepository extends CommonRepository<StandardEndpointSystemConfig, Long> {
}

package co.arctx.apim.adapter.entity;

import co.arctx.core.common.DataType;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AggrParamRequest {

    @Id
    String id;
    String aggrParamRequest;
    String objectType;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aggr_endpoint_id")
    AggrEndpoint aggrEndpointId;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    AggrParamRequest parentId;
    String createdBy;
    LocalDateTime createdDate;
    String updateBy;
    LocalDateTime updateDate;

    @OneToMany(mappedBy = "aggrParamRequest")
    List<StandardParamAggrParamRequest> aggrParamList;

    @Enumerated(EnumType.STRING)
    @Column(name = "data_type")
    DataType dataType;
}

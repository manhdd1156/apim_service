package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.StandardEndpoint;
import co.arctx.apim.adapter.repository.StandardEndpointRepository;
import co.arctx.apim.adapter.utils.Message;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Service
public class StandardEndpointService extends CommonService<StandardEndpoint, String, StandardEndpointRepository> {


    protected StandardEndpointService(StandardEndpointRepository repo) {
        super(repo);
    }

    public Long getStandardEndpointIdByEndpointAndToken(String endpoint) {
        return repo.findStandardEndpointIdByEnpointAndToken(endpoint);
    }


    public List<StandardEndpoint> findAllStandardEndpoint() {
        return all(Sort.Order.asc("endpoint"));
    }

    public StandardEndpoint findStandardEndpointById(String id) throws NotFoundEntityException {
        return getOrElseThrow(id);
    }

    public List<StandardEndpoint> saveStandardEndpoint(List<StandardEndpoint> standardEndpoints) {
        return save(standardEndpoints);
    }

    public StandardEndpoint saveStandardEndpoint(StandardEndpoint standardEndpoint) {
        return save(standardEndpoint);
    }

    public StandardEndpoint updateStandardEndpoint(StandardEndpoint standardEndpoint) throws NotFoundEntityException {
        return updateOnField(standardEndpoint.getId(), e -> {
            e.setEndpoint(standardEndpoint.getEndpoint());
            e.setUpdateDate(LocalDateTime.now());
            e.setUpdateBy(standardEndpoint.getUpdateBy());
        });
    }

    public StandardEndpoint deleted(String id) {
        return deleteIfExisted(id);
    }


    @Override
    protected String notFoundMessage() {
        return Message.STANDARD_ENDPOINT_NOT_FOUND;
    }


}

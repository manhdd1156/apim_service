package co.arctx.apim.adapter.api;

import co.arctx.apim.adapter.model.SystemConfigModel;
import co.arctx.apim.adapter.model.request.SystemConfigRequest;
import co.arctx.apim.adapter.processor.SystemConfigProcessor;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.exception.ValidateException;
import co.arctx.core.model.ResponseModel;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: quangdtptit
 * Jul 04, 2022
 */
@RestController
@RequestMapping("/api/v1/adapters/system-config")
@RequiredArgsConstructor
public class SystemConfigController {

    private final SystemConfigProcessor processor;

    @ApiOperation("Active system config")
    @PutMapping("/active/status")
    public ResponseEntity<ResponseModel<SystemConfigModel>> updateActiveStatus(@RequestParam("baseUrl") String baseUrl) throws NotFoundEntityException {
        return ResponseEntity.ok(ResponseModel.ok(processor.updateActiveStatus(baseUrl), "Active system config success"));
    }

    @ApiOperation("Get all system config")
    @GetMapping
    public ResponseEntity<ResponseModel<List<SystemConfigModel>>> getAll() {
        return ResponseEntity.ok(ResponseModel.ok(processor.getAll(), "Get all system config"));
    }

    @ApiOperation("Get by id system config")
    @GetMapping("/{id}")
    public ResponseEntity<ResponseModel<SystemConfigModel>> getById(@PathVariable("id") String id) throws NotFoundEntityException {
        return ResponseEntity.ok(ResponseModel.ok(processor.getDetail(id), String.format("Get system config by id %s", id)));
    }

    @ApiOperation("Create new system config")
    @PostMapping
    public ResponseEntity<ResponseModel<SystemConfigModel>> create(@Valid @RequestBody SystemConfigRequest systemConfigRequest) throws ValidateException {
        return ResponseEntity.ok(ResponseModel.ok(processor.create(systemConfigRequest), "Create system config success"));
    }

    @ApiOperation("Update system config")
    @PutMapping("/{id}")
    public ResponseEntity<ResponseModel<SystemConfigModel>> update(@PathVariable("id") String id, @Valid @RequestBody SystemConfigRequest systemConfigRequest)
        throws NotFoundEntityException, ValidateException {
        return ResponseEntity.ok(ResponseModel.ok(processor.update(id, systemConfigRequest), "Update system config success"));
    }

    @ApiOperation("Delete system config by id")
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseModel<String>> delete(@PathVariable("id") String id) throws NotFoundEntityException {
        return ResponseEntity.ok(
            ResponseModel.ok(String.format("Delete success system config with id = %s", processor.delete(id).getId()), "Delete system config success"));
    }

}
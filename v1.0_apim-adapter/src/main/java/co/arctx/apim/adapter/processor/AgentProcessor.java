package co.arctx.apim.adapter.processor;

import co.arctx.apim.adapter.entity.Agent;
import co.arctx.apim.adapter.entity.QAgent;
import co.arctx.apim.adapter.service.AgentService;
import co.arctx.apim.adapter.transformer.AgentTransformerImpl;
import co.arctx.core.common.CommonProcessor;
import org.springframework.stereotype.Component;

/**
 * @author: Nam Vu
 * Jul 20, 2022
 */

@Component
public class AgentProcessor extends CommonProcessor<AgentService, QAgent, AgentTransformerImpl> {

    protected AgentProcessor(AgentService service, AgentTransformerImpl transformer) {
        super(QAgent.agent, service, transformer);
    }

    public Agent findAgentByToken(String token) {
        return service.findAgentByToken(token);
    }
}

package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

/**
 * @author: Nam Vu
 * Jul 19, 2022
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameProvider {

    @Id
    String id;
    String providerName;
    String createdBy;
    String createdDate;
    String updatedBy;
    String updatedDate;

    @OneToMany(mappedBy = "gameProvider")
    List<SystemConfigGameProvider> configGameProviders;
}

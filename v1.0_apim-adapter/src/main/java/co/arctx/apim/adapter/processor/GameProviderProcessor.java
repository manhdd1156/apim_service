package co.arctx.apim.adapter.processor;

import co.arctx.apim.adapter.entity.GameProvider;
import co.arctx.apim.adapter.entity.QGameProvider;
import co.arctx.apim.adapter.model.GameProviderModel;
import co.arctx.apim.adapter.model.SystemConfigModel;
import co.arctx.apim.adapter.service.GameProviderService;
import co.arctx.apim.adapter.transformer.GameProviderTransformerImpl;
import co.arctx.core.common.CommonProcessor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: Nam Vu
 * Jul 20, 2022
 */

@Component
public class GameProviderProcessor extends CommonProcessor<GameProviderService, QGameProvider, GameProviderTransformerImpl> {

    protected GameProviderProcessor(GameProviderService service, GameProviderTransformerImpl transformer) {
        super(QGameProvider.gameProvider, service, transformer);
    }

    public List<String> getAllGameProvidersBySystemStatus(Integer status) {
        return service.getAllGameProvidersBySystemStatus(status);
    }


}

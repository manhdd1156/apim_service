package co.arctx.apim.adapter.annotations;

import co.arctx.apim.adapter.annotations.impl.DomainValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Documented
@Constraint(validatedBy = DomainValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidDomain {
    String message() default "Domain is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

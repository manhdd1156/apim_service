package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.GameProvider;
import co.arctx.apim.adapter.model.GameProviderModel;
import co.arctx.apim.adapter.repository.GameProviderRepository;
import co.arctx.core.common.CommonService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Nam Vu
 * Jul 20, 2022
 */

@Service
public class GameProviderService extends CommonService<GameProvider, String, GameProviderRepository> {
    protected GameProviderService(GameProviderRepository repo) {
        super(repo);
    }

    public List<String> getAllGameProvidersBySystemStatus(Integer status) {
        return repo.getAllGameProvidersBySystemStatus(status);
    }


    @Override
    protected String notFoundMessage() {
        return null;
    }
}

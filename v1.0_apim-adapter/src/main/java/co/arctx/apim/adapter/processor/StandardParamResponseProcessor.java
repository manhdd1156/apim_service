package co.arctx.apim.adapter.processor;

import co.arctx.apim.adapter.entity.QStandardParamResponse;
import co.arctx.apim.adapter.entity.StandardParamResponse;
import co.arctx.apim.adapter.service.StandardParamResponseService;
import co.arctx.apim.adapter.transformer.StandardParamResponseTransformerImpl;
import co.arctx.core.common.CommonProcessor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 27, 2022
 */

@Component
public class StandardParamResponseProcessor extends CommonProcessor<StandardParamResponseService, QStandardParamResponse, StandardParamResponseTransformerImpl> {
    protected StandardParamResponseProcessor(StandardParamResponseService service, StandardParamResponseTransformerImpl transformer) {
        super(QStandardParamResponse.standardParamResponse1, service, transformer);
    }

    List<StandardParamResponse> findStandardParamResponseByIds(List<String> ids) {
        return service.findStandardParamResponseByIds(ids);
    }
}

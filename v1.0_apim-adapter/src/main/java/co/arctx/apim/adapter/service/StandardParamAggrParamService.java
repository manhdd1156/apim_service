package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.StandardParamAggrParamRequest;
import co.arctx.apim.adapter.repository.StandardParamAggrParamRequestRepository;
import co.arctx.apim.adapter.utils.Message;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Service
public class StandardParamAggrParamService extends CommonService<StandardParamAggrParamRequest, Long, StandardParamAggrParamRequestRepository> {


    protected StandardParamAggrParamService(StandardParamAggrParamRequestRepository repo) {
        super(repo);
    }

    public List<StandardParamAggrParamRequest> findAllStandardParamAggrParam() {
        return all(Sort.Order.asc("id"));
    }

    public StandardParamAggrParamRequest findStandardParamAggrParamById(Long id) throws NotFoundEntityException {
        return getOrElseThrow(id);
    }

    public List<StandardParamAggrParamRequest> saveStandardParamAggrParam(List<StandardParamAggrParamRequest> standardParamAggrParamRequests) {
        return save(standardParamAggrParamRequests);
    }

    public StandardParamAggrParamRequest saveStandardParamAggrParam(StandardParamAggrParamRequest standardParamAggrParamRequest) {
        return save(standardParamAggrParamRequest);
    }

    public StandardParamAggrParamRequest updateStandardParamAggrParam(StandardParamAggrParamRequest standardParamAggrParamRequest) throws NotFoundEntityException {
        return updateOnField(standardParamAggrParamRequest.getId(), e -> {
            e.setAggrParamRequest(standardParamAggrParamRequest.getAggrParamRequest());
            e.setAggrParamRequest(standardParamAggrParamRequest.getAggrParamRequest());
        });
    }

    public StandardParamAggrParamRequest deletedStandardParamAggrParam(Long id) {
        return deleteIfExisted(id);
    }

    @Override
    protected String notFoundMessage() {
        return Message.STANDARD_PARAM_AGGR_PARAM;
    }
}

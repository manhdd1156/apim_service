package co.arctx.apim.adapter.transformer;

import co.arctx.apim.adapter.entity.AggrParamResponse;
import co.arctx.apim.adapter.model.AggrParamResponseModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author: Nam Vu
 * Jun 23, 2022
 */

@Mapper(componentModel = "spring")
public interface AggrParamResponseTransformer extends EntitiesMapper<AggrParamResponse, AggrParamResponseModel> {

    @Override
    @Mapping(target = "aggrEndpointId", source = "aggrEndpointId.id")
    @Mapping(target = "parentId", source = "parentId.id")
    AggrParamResponseModel toModel(AggrParamResponse aggrParamResponse);

    @Override
    @Mapping(target = "parentId.id", source = "aggrEndpointId")
    @Mapping(target = "aggrEndpointId.id", source = "aggrEndpointId")
    AggrParamResponse toEntity(AggrParamResponseModel aggrParamResponseModel);
}

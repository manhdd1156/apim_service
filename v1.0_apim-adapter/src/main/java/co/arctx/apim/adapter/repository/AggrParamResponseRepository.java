package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.AggrParamResponse;
import co.arctx.apim.adapter.model.MapDataResponseModel;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 23, 2022
 */

public interface AggrParamResponseRepository extends CommonRepository<AggrParamResponse, String> {


    @Query("SELECT new co.arctx.apim.adapter.model.MapDataResponseModel(apr.aggrParamResponse,apr.objectType,spr.standardParamResponse) FROM SystemConfig sc " +
            "JOIN StandardEndpointSystemConfig sesc on sc.id = sesc.systemConfig " +
            "JOIN StandardEndpoint se on se.id=sesc.standardEndpoint " +
            "JOIN StandardParamResponse spr on spr.standardEndpoint=se.id " +
            "JOIN StandardParamAggrParamResponse spapr on spr.id =spapr.standardParamResponse " +
            "JOIN AggrParamResponse apr on apr.id = spapr.aggrParamResponse " +
            "JOIN AggrEndpoint ae on apr.aggrEndpointId =ae.id " +
            "WHERE se.endpoint=?1 AND sc.status = 1 and apr.aggrEndpointId.id=?2")
    List<MapDataResponseModel> getAggrParamResponseByEndPoint(String endpoint, String aggrEndpoint);


}

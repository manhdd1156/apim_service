package co.arctx.apim.adapter.enums;

import lombok.Getter;

/**
 * @author: Nam Vu Jun 21, 2022
 */
public enum SystemConfigStatus {
    NOT_USE(0, "Disable"),
    USE(1, "Acticve");

    @Getter
    private Integer value;
    @Getter
    private String description;

    SystemConfigStatus(Integer value, String description) {
        this.value = value;
        this.description = description;
    }
}

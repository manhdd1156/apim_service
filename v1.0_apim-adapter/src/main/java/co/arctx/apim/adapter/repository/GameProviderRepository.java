package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.GameProvider;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: Nam Vu
 * Jul 20, 2022
 */

public interface GameProviderRepository extends CommonRepository<GameProvider, String> {

    @Query("SELECT DISTINCT gp.providerName FROM SystemConfig sc " +
            "JOIN SystemConfigGameProvider scgp on sc.id=scgp.systemConfig " +
            "JOIN GameProvider gp on scgp.gameProvider= gp.id " +
            "WHERE scgp.status = 1 and scgp.expiryDate > CURRENT_DATE ")
    List<String> getAllGameProvidersBySystemStatus(Integer status);

}

package co.arctx.apim.adapter.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;


/**
 * @author: quangdtptit
 * Jul 04, 2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class SystemConfigRequest {

    @NotBlank(message = "System name not blank")
    @Length(max = 255, message = "System name not exceed 255 character")
    private String systemName;

    @Length(max = 255, message = "Auth type not exceed 255 character")
    private String authType;

    @NotBlank(message = "Base url not blank")
    @Length(max = 255, message = "Base url not exceed 255 character")
    private String baseUrl;

    @Length(max = 255, message = "Description not exceed 255 character")
    private String description;

}
package co.arctx.apim.adapter.api.internal;

import co.arctx.apim.adapter.service.ReadGameListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author: Son Tran
 * Aug 01, 2022
 */
@RestController
public class ReadFileAPI {
    @Autowired
    private ReadGameListService service;
    @GetMapping("/file")
    public ResponseEntity<?> readGameList() throws IOException {
        return ResponseEntity.ok(service.readGameList());
    }
}

package co.arctx.apim.adapter.transformer;

import co.arctx.apim.adapter.entity.AggrParamRequest;
import co.arctx.apim.adapter.model.AggrParamRequestModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */

@Mapper(componentModel = "spring")
public interface AggrParamRequestTransformer extends EntitiesMapper<AggrParamRequest, AggrParamRequestModel> {

    @Override

    @Mapping(target = "aggrEndpointId", source = "aggrParamRequest.aggrEndpointId.id")
    @Mapping(target = "parentId", source = "aggrParamRequest.id")
    AggrParamRequestModel toModel(AggrParamRequest aggrParamRequest);

    @Override

    @Mapping(target = "aggrEndpointId.id", source = "aggrEndpointId")
    @Mapping(target = "parentId.id", source = "parentId")
    AggrParamRequest toEntity(AggrParamRequestModel aggrParamRequestModel);
}

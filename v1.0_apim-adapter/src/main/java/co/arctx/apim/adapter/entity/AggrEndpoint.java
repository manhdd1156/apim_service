package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AggrEndpoint {

    @Id
    String id;
    @Column(unique = true)
    String endpoint;
    String method;

    String createdBy;
    LocalDateTime createdDate;
    String updateBy;
    LocalDateTime updatedDate;

    @OneToMany(mappedBy = "aggrEndpointId")
    List<AggrParamRequest> aggrParamRequestNames;

    @OneToMany(mappedBy = "aggrEndpointId")
    List<AggrParamResponse> aggrParamResponses;

    @OneToOne(mappedBy = "endpoint")
    StandardEndpointSystemConfig systemConfig;

    @OneToOne(mappedBy = "aggrEndpoint")
    StandardEndpointAgent endpointAgent;

}

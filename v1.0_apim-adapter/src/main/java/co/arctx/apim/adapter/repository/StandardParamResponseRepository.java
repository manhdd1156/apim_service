package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.StandardParamResponse;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 23, 2022
 */

public interface StandardParamResponseRepository extends CommonRepository<StandardParamResponse, String> {


    @Query("SELECT distinct spr FROM SystemConfig sc JOIN StandardEndpointSystemConfig sesc ON sc.id = sesc.systemConfig " +
            "JOIN AggrEndpoint ae on sesc.endpoint = ae.id " +
            "JOIN AggrParamResponse apr on ae.id=apr.aggrEndpointId " +
            "JOIN StandardParamAggrParamResponse spar ON apr.id = spar.aggrParamResponse " +
            "JOIN StandardParamResponse spr ON spar.standardParamResponse=spr.id " +
            "WHERE sc.status =1 and apr.id in ?1 ")
    List<StandardParamResponse> getStandardParamResponseInIds(List<String> ids);
}

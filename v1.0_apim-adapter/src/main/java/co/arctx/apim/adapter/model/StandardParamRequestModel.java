package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StandardParamRequestModel {

    Long id;
    String standardParam;
    String objectType;
    Long parentId;
    String createdBy;
    LocalDateTime createdDate;
    String updateBy;
    LocalDateTime updateDate;
}

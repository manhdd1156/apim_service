package co.arctx.apim.adapter.utils;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public class Message {

    public static final String FIELD_INVALID = "Field input not valid";

    public static final String AGGR_PARAM_NOT_FOUND = "Aggr Param not exist";

    public static final String AGGR_ENDPOINT_NOT_FOUND = "Aggr Endpoint not exist";

    public static final String STANDARD_ENDPOINT_NOT_FOUND = "Standard Param not exist";

    public static final String STANDARD_ENDPOINT_SYSTEM_CONFIG_NOT_FOUND = "Standard Endpoint and SystemConfig not exist";

    public static final String STANDARD_PARAM_NOT_FOUND = "Standard Param not exist";

    public static final String SYSTEM_CONFIG_NOT_FOUND = "System Config not exist";

    public static final String STANDARD_PARAM_AGGR_PARAM = "Standard Param and Aggr Param not exist";


    private Message() {
    }

}

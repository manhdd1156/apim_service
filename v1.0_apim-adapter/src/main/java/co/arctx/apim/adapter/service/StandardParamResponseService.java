package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.StandardParamResponse;
import co.arctx.apim.adapter.repository.StandardParamResponseRepository;
import co.arctx.core.common.CommonService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 27, 2022
 */

@Service
public class StandardParamResponseService extends CommonService<StandardParamResponse, String, StandardParamResponseRepository> {

    protected StandardParamResponseService(StandardParamResponseRepository repo) {
        super(repo);
    }

    public List<StandardParamResponse> findStandardParamResponseByIds(List<String> ids) {
        return repo.getStandardParamResponseInIds(ids);
    }

    @Override
    protected String notFoundMessage() {
        return null;
    }
}

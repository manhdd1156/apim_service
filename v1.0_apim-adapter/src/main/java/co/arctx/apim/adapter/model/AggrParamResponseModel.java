package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

/**
 * @author: Nam Vu
 * Jun 23, 2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AggrParamResponseModel {

    Long id;
    String aggrParamResponse;
    Long aggrEndpointId;
    Long parentId;
    String createdBy;
    LocalDateTime createdDate;
    String updateBy;
    LocalDateTime updateDate;

}

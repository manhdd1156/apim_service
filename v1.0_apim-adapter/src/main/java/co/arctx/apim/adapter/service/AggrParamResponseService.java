package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.AggrParamResponse;
import co.arctx.apim.adapter.model.MapDataResponseModel;
import co.arctx.apim.adapter.repository.AggrParamResponseRepository;
import co.arctx.core.common.CommonService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 23, 2022
 */

@Service
public class AggrParamResponseService extends CommonService<AggrParamResponse, String, AggrParamResponseRepository> {

    protected AggrParamResponseService(AggrParamResponseRepository repo) {
        super(repo);
    }

        public List<MapDataResponseModel> findAggrParamResponsesByEndpoint(String endpoint,String aggrEndpoint) {
        return repo.getAggrParamResponseByEndPoint(endpoint,aggrEndpoint);
    }


    @Override
    protected String notFoundMessage() {
        return null;
    }
}

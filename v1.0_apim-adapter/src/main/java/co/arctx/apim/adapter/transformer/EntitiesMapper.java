package co.arctx.apim.adapter.transformer;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public interface EntitiesMapper<E, M> {

    M toModel(E e);

    E toEntity(M m);

    List<M> toModels(List<E> eList);

    List<E> toEntities(List<M> mList);

}

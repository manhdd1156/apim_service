package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.SystemConfig;
import co.arctx.apim.adapter.enums.SystemConfigStatus;
import co.arctx.apim.adapter.model.ConfigResponseModel;
import co.arctx.apim.adapter.repository.RepositoryCustom;
import co.arctx.apim.adapter.repository.SystemConfigRepository;
import co.arctx.apim.adapter.utils.Message;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Service
public class SystemConfigService extends CommonService<SystemConfig, String, SystemConfigRepository> {


    private final RepositoryCustom repositoryCustom;

    protected SystemConfigService(SystemConfigRepository repo, RepositoryCustom repositoryCustom) {
        super(repo);
        this.repositoryCustom = repositoryCustom;
    }

    public ConfigResponseModel getModelResponseBySystemConfigId(String endpoint, String token) {
        return repositoryCustom.getModelResponse(endpoint, token);
    }

    public ConfigResponseModel getCallBackResponseByEndpointAndToken(String endpoint, String token) {
        return repositoryCustom.getCallBackModelResponse(endpoint, token);
    }

    public SystemConfig getSystemConfigByEndpointAndToken(String endpoint) {
        return repo.findSystemConfigByStandardEndpointId(endpoint);
    }


    public SystemConfig findById(String id) throws NotFoundEntityException {
        return getOrElseThrow(id);
    }

    public List<SystemConfig> findAll() {
        return all(Order.asc("systemName"));
    }


    public List<SystemConfig> saveSystemConfig(List<SystemConfig> systemConfigs) {
        return save(systemConfigs);
    }

    public SystemConfig saveSystemConfig(SystemConfig systemConfigs) {
        return save(systemConfigs);
    }

    public SystemConfig updateSystemConfig(SystemConfig systemConfig) throws NotFoundEntityException {
        return updateOnField(systemConfig.getId(), e -> {
            e.setSystemName(systemConfig.getSystemName());
            e.setBaseUrl(systemConfig.getBaseUrl());
            e.setUpdatedBy(systemConfig.getUpdatedBy());
            e.setUpdatedDate(LocalDateTime.now());
            e.setStatus(systemConfig.getStatus());
            e.setDescription(systemConfig.getDescription());
        });
    }

    public SystemConfig disableSystemConfig(String id, String userName) throws NotFoundEntityException {
        SystemConfig systemConfig = findById(id);
        systemConfig.setUpdatedBy(userName);
        systemConfig.setStatus(0);
        systemConfig.setUpdatedDate(LocalDateTime.now());
        return save(systemConfig);
    }

    @Override
    protected String notFoundMessage() {
        return Message.SYSTEM_CONFIG_NOT_FOUND;
    }

    @Transactional
    public SystemConfig updateActive(String baseUrl) throws NotFoundEntityException {
        return repo.findByBaseUrl(baseUrl, null)
                .map(e -> {
                    if (SystemConfigStatus.NOT_USE.getValue().equals(e.getStatus())) {
                        e.setStatus(SystemConfigStatus.USE.getValue());
                        repo.updateDisable(e.getId());
                        repo.save(e);
                    }
                    return e;
                })
                .orElseThrow(() -> NotFoundEntityException.of(Message.SYSTEM_CONFIG_NOT_FOUND));
    }

    public Optional<SystemConfig> findByBaseUrl(String baseUrl, String id) {
        return repo.findByBaseUrl(baseUrl, id);
    }

    public SystemConfig findByToken(String token) {
        return repo.findSystemConfigByToken(token);
    }

    public List<SystemConfig> findSystemActive() {
        return repo.getAllSystemConfigActive();
    }

}

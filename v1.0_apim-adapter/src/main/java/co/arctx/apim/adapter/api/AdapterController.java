package co.arctx.apim.adapter.api;

import co.arctx.apim.adapter.model.ConfigResponseModel;
import co.arctx.apim.adapter.model.StandardEndpointRequestModel;
import co.arctx.apim.adapter.model.SystemConfigModel;
import co.arctx.apim.adapter.processor.GameProviderProcessor;
import co.arctx.apim.adapter.processor.SystemConfigProcessor;
import co.arctx.core.exception.InputInvalidException;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@RestController
@RequestMapping("/api/v1/adapters")
@AllArgsConstructor
public class AdapterController {

    private final SystemConfigProcessor systemConfigProcessor;

    private final GameProviderProcessor gameProviderProcessor;

    @PostMapping("/get-config")
    public ResponseEntity<Object> getConfig(@RequestBody StandardEndpointRequestModel endpoint) throws InputInvalidException {
        ConfigResponseModel configResponseModel = systemConfigProcessor.getModelResponse(endpoint.getEndpoint(), endpoint.getToken());
        return ResponseEntity.ok(configResponseModel);
    }

    @GetMapping("/get-game-provider/{status}")
    public ResponseEntity<List<String>> getGameProvider(@PathVariable("status") Integer status) {
        List<String> providers = gameProviderProcessor.getAllGameProvidersBySystemStatus(status);
        return ResponseEntity.ok(providers);
    }

    @GetMapping("/get-system-config")
    public ResponseEntity<List<SystemConfigModel>> getSystemConfig() {
        List<SystemConfigModel> systemConfigsActive = systemConfigProcessor.getAllSystemConfigActive();
        return ResponseEntity.ok(systemConfigsActive);
    }
}

package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.GameList;
import co.arctx.apim.adapter.repository.GameListRepository;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Nam Vu
 * Jul 22, 2022
 */

@Service
public class GameListService extends CommonService<GameList, String, GameListRepository> {
    protected GameListService(GameListRepository repo) {
        super(repo);
    }

    public GameList saveGameList(GameList gameList) {
        return save(gameList);
    }

    public List<GameList> saveGameList(List<GameList> gameLists) {
        return save(gameLists);
    }

    public GameList updateGameList(GameList gameLists) throws NotFoundEntityException {
        return updateOnField(gameLists.getId(), e -> {
            e.setGameList(gameLists.getGameList());
        });
    }

    public List<GameList> findInSystemProviderIds(List<Long> id) {
        return repo.findInListSystemGameProviderId(id);
    }

    public List<GameList> findGameListAvailable() {
        return repo.findGameListAvailable();
    }

    public List<GameList> findInIds(List<String> ids) {
        return repo.findInIds(ids);
    }


    @Override
    protected String notFoundMessage() {
        return null;
    }
}

package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Nam Vu
 * Jul 22, 2022
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameListModel {
    String id;
    String aggProviderId;
    String gameList;
}

package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.Agent;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author: Nam Vu
 * Jul 19, 2022
 */

public interface AgentRepository extends CommonRepository<Agent, String> {

    @Query("SELECT ag FROM Agent ag WHERE ag.agentToken=?1")
    Agent getAgentByToken(String token);
}

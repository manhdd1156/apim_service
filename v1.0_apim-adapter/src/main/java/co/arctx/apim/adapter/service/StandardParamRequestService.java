package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.StandardParamRequest;
import co.arctx.apim.adapter.repository.StandardParamRequestRepository;
import co.arctx.apim.adapter.utils.Message;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Service
public class StandardParamRequestService extends CommonService<StandardParamRequest, String, StandardParamRequestRepository> {


    protected StandardParamRequestService(StandardParamRequestRepository repo) {
        super(repo);
    }


    public List<StandardParamRequest> findAllStandardParam() {
        return all(Sort.Order.asc("standardParamRequest"));
    }

    public List<StandardParamRequest> findStandardParamRequestByIds(List<String> ids) {
        return repo.findStandardParamRequestInListIds(ids);
    }

    public StandardParamRequest findById(String id) throws NotFoundEntityException {
        return getOrElseThrow(id);
    }

    public List<StandardParamRequest> saveStandardParam(List<StandardParamRequest> standardParamRequests) {
        return save(standardParamRequests);
    }

    public StandardParamRequest saveStandardParam(StandardParamRequest standardParamsRequest) {
        return save(standardParamsRequest);
    }

    public StandardParamRequest updateStandardParam(StandardParamRequest standardParamRequest) throws NotFoundEntityException {
        return updateOnField(standardParamRequest.getId(), e -> {
            e.setStandardParamRequest(standardParamRequest.getStandardParamRequest());
            e.setStandardEndpointId(standardParamRequest.getStandardEndpointId());
            e.setUpdateDate(LocalDateTime.now());
            e.setUpdateBy(standardParamRequest.getUpdateBy());
        });
    }


    public StandardParamRequest deleteById(String id) {
        return deleteIfExisted(id);
    }


    @Override
    protected String notFoundMessage() {
        return Message.STANDARD_PARAM_NOT_FOUND;
    }
}

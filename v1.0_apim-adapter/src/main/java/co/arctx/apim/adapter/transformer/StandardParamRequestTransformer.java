package co.arctx.apim.adapter.transformer;

import co.arctx.apim.adapter.entity.StandardParamRequest;
import co.arctx.apim.adapter.model.StandardParamRequestModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Mapper(componentModel = "spring")
public interface StandardParamRequestTransformer extends EntitiesMapper<StandardParamRequest, StandardParamRequestModel> {

    @Override
    @Mapping(target = "parentId", source = "standardParamRequest.id")
    StandardParamRequestModel toModel(StandardParamRequest standardParamRequest);

    @Override
    @Mapping(target = "parentId.id", source = "parentId")
    StandardParamRequest toEntity(StandardParamRequestModel standardParamRequestModel);
}

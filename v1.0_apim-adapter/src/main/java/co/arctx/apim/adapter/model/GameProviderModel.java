package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Nam Vu
 * Jul 20, 2022
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameProviderModel {
    String id;
    String providerName;
    Integer status;
    String expiryDate;
}

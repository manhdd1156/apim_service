package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.Agent;
import co.arctx.apim.adapter.repository.AgentRepository;
import co.arctx.core.common.CommonService;
import org.springframework.stereotype.Service;

/**
 * @author: Nam Vu
 * Jul 20, 2022
 */

@Service
public class AgentService extends CommonService<Agent, String, AgentRepository> {

    protected AgentService(AgentRepository repo) {
        super(repo);
    }

    public Agent findAgentByToken(String token) {
        return repo.getAgentByToken(token);
    }

    @Override
    protected String notFoundMessage() {
        return null;
    }
}

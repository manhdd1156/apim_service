package co.arctx.apim.adapter.model;

import lombok.Data;

import java.util.List;

/**
 * @author: Son Tran
 * Jun 30, 2022
 */
@Data
public class CreateRequestParam {
    private String stdRequest;
    private List<AggrParamRequestModel> request;
}

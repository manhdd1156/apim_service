package co.arctx.apim.adapter.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.core.config.yaml.YamlConfiguration;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * @author: Son Tran
 * Aug 01, 2022
 */
@Service
public class ReadGameListService {



    public Object readGameList() throws IOException {
        File file = new File("D:\\BGaming games.yml");
        if (!file.exists())
            return null;
        String ymlString = new String(Files.readAllBytes(file.toPath()));
        Yaml yaml= new Yaml();
        List<Map<String,Object>> gameList = yaml.load(ymlString);
        Map gameListGroup = gameList.stream().collect(Collectors.groupingBy(g -> g.get("producer")));
        return gameListGroup;
    }

}

package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 22, 2022
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AggrParamResponse {

    @Id
    String id;
    String aggrParamResponse;
    String objectType;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aggr_endpoint_id")
    AggrEndpoint aggrEndpointId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    AggrParamResponse parentId;
    String createdBy;
    LocalDateTime createdDate;
    String updatedBy;
    LocalDateTime updatedDate;

    @OneToMany(mappedBy = "aggrParamResponse")
    List<StandardParamAggrParamResponse> standardParamAggrParamResponses;


}

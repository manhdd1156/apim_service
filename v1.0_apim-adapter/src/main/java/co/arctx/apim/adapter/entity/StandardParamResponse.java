package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author: Nam Vu
 * Jun 22, 2022
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StandardParamResponse {

    @Id
    String id;
    String standardParamResponse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "standard_endpoint_id")
    StandardEndpoint standardEndpoint;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    StandardParamResponse parentId;
    String createdBy;
    LocalDateTime createdDate;
    String updatedBy;
    LocalDateTime updatedDate;
}

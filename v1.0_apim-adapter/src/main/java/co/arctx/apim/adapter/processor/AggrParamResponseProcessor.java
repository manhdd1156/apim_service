package co.arctx.apim.adapter.processor;

import co.arctx.apim.adapter.entity.QAggrParamResponse;
import co.arctx.apim.adapter.model.MapDataResponseModel;
import co.arctx.apim.adapter.service.AggrParamResponseService;
import co.arctx.apim.adapter.transformer.AggrParamResponseTransformerImpl;
import co.arctx.core.common.CommonProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: Nam Vu
 * Jun 23, 2022
 */

@Component
@Slf4j
public class AggrParamResponseProcessor extends CommonProcessor<AggrParamResponseService, QAggrParamResponse, AggrParamResponseTransformerImpl> {


    protected AggrParamResponseProcessor(AggrParamResponseService service, AggrParamResponseTransformerImpl transformer ) {
        super(QAggrParamResponse.aggrParamResponse1, service, transformer);
    }

    @Transactional
    public Map<String, List<String>> getParamResponse(String endpoint,String aggrEndpoint) {
        HashMap<String, List<String>> response = new HashMap<>();
        List<MapDataResponseModel> list = service.findAggrParamResponsesByEndpoint(endpoint,aggrEndpoint);
        for (MapDataResponseModel data : list) {
            response.put(data.getAggrParamResponse(),
                    Arrays.asList(data.getStandardParamResponse(), data.getAggrObjectType()));
        }
        return response;
    }
}

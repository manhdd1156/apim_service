package co.arctx.apim.adapter.processor;

import co.arctx.apim.adapter.entity.QStandardEndpoint;
import co.arctx.apim.adapter.service.StandardEndpointService;
import co.arctx.apim.adapter.transformer.StandardEndpointTransformerImpl;
import co.arctx.core.common.CommonProcessor;
import org.springframework.stereotype.Component;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Component
public class StandardEndpointProcessor extends CommonProcessor<StandardEndpointService, QStandardEndpoint, StandardEndpointTransformerImpl> {

    protected StandardEndpointProcessor(StandardEndpointService service, StandardEndpointTransformerImpl transformer) {
        super(QStandardEndpoint.standardEndpoint, service, transformer);
    }




}

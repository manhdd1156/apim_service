package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StandardEndpointModel {
    Long id;
    String endpoint;
    String token;
    String createdBy;
    LocalDateTime createdDate;
    String updateBy;
    LocalDateTime updateDate;

}

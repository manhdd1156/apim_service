package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.StandardParamRequest;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public interface StandardParamRequestRepository extends CommonRepository<StandardParamRequest, String> {

    @Query("SELECT spr FROM SystemConfig sc JOIN StandardEndpointSystemConfig sesc on sc.id=sesc.systemConfig " +
            "JOIN AggrEndpoint ae on sesc.endpoint=ae.id " +
            "JOIN AggrParamRequest apr on ae.id=apr.aggrEndpointId " +
            "JOIN StandardParamAggrParamRequest spap on apr.id=spap.aggrParamRequest " +
            "JOIN StandardParamRequest spr on spap.standardParamRequest =spr.id " +
            "WHERE sc.status = 1 and apr.id IN ?1")
    List<StandardParamRequest> findStandardParamRequestInListIds(List<String> ids);
}

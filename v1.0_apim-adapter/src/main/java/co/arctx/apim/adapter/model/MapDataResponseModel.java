package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jul 06, 2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MapDataResponseModel {
    String aggrParamResponse;
    String aggrObjectType;
    String standardParamResponse;
}

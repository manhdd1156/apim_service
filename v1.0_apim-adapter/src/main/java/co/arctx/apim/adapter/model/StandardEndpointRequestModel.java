package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StandardEndpointRequestModel {
    String endpoint;
    String token;
}

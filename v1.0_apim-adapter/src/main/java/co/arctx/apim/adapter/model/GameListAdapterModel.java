package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Duy Manh
 * Jul 26, 2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameListAdapterModel {
    String id;
    String gameList;
    Long systemProviderId;
}

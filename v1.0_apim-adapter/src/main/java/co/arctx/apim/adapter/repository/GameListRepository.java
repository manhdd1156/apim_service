package co.arctx.apim.adapter.repository;

import co.arctx.apim.adapter.entity.GameList;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author: Nam Vu
 * Jul 22, 2022
 */

public interface GameListRepository extends CommonRepository<GameList, String> {

    @Query("SELECT gl FROM GameList gl " +
            "JOIN SystemConfigGameProvider scgp ON gl.sysConfigGameProvider.id=scgp.id " +
            " WHERE scgp.status =1 AND  gl.sysConfigGameProvider.id IN :ids ")
    List<GameList> findInListSystemGameProviderId(@Param("ids") List<Long> id);

    @Query("SELECT gl FROM GameList gl " +
            "JOIN SystemConfigGameProvider scgp ON gl.sysConfigGameProvider.id=scgp.id " +
            "WHERE scgp.status=1 AND scgp.expiryDate > current_date ")
    List<GameList> findGameListAvailable();

    @Query("SELECT gl FROM GameList gl WHERE gl.id IN ?1")
    List<GameList> findInIds(List<String> id);
}

package co.arctx.apim.adapter.processor;

import co.arctx.apim.adapter.entity.Agent;
import co.arctx.apim.adapter.entity.QSystemConfig;
import co.arctx.apim.adapter.entity.SystemConfig;
import co.arctx.apim.adapter.model.ConfigResponseModel;
import co.arctx.apim.adapter.model.SystemConfigGameProviderModel;
import co.arctx.apim.adapter.model.SystemConfigModel;
import co.arctx.apim.adapter.model.request.SystemConfigRequest;
import co.arctx.apim.adapter.service.SystemConfigGameProviderService;
import co.arctx.apim.adapter.service.SystemConfigService;
import co.arctx.apim.adapter.transformer.SystemConfigTransformerImpl;
import co.arctx.apim.adapter.utils.Message;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.exception.InputInvalidException;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.exception.ValidateException;
import co.arctx.core.jwt.JwtReader;
import co.arctx.core.model.ErrorDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Component
@Slf4j
public class SystemConfigProcessor extends CommonProcessor<SystemConfigService, QSystemConfig, SystemConfigTransformerImpl> {

    private final JwtReader reader;

    private final AggrParamRequestProcessor paramRequestProcessor;

    private final AggrParamResponseProcessor paramResponseProcessor;

    private final AgentProcessor agentProcessor;

    private final SystemConfigGameProviderService configGameProviderService;

    protected SystemConfigProcessor(SystemConfigService service, SystemConfigTransformerImpl transformer, JwtReader reader, AggrParamRequestProcessor paramRequestProcessor, AggrParamResponseProcessor paramResponseProcessor, AgentProcessor agentProcessor, SystemConfigGameProviderService configGameProviderService) {
        super(QSystemConfig.systemConfig, service, transformer);
        this.reader = reader;
        this.paramRequestProcessor = paramRequestProcessor;
        this.paramResponseProcessor = paramResponseProcessor;
        this.agentProcessor = agentProcessor;
        this.configGameProviderService = configGameProviderService;
    }

    public List<SystemConfigModel> getAllSystemConfigActive() {
        return transformer.toModels(service.findSystemActive());
    }

    public List<SystemConfigGameProviderModel> getSystemProviderActive() {
        return configGameProviderService.findSystemConfigGameProviderModels();
    }

    public ConfigResponseModel getModelResponse(String endpoint, String token) throws InputInvalidException {
        ConfigResponseModel configResponseModelBySystemConfigId = null;
        SystemConfig systemConfig = service.findByToken(token);

        if (!Objects.isNull(systemConfig)) {
            configResponseModelBySystemConfigId = service.getModelResponseBySystemConfigId(endpoint, token);
        } else {
            Agent agent = agentProcessor.findAgentByToken(token);
            if (!Objects.isNull(agent)) {
                configResponseModelBySystemConfigId = service.getCallBackResponseByEndpointAndToken(endpoint, token);
            } else {
                throw new InputInvalidException(null, "Token is not valid");
            }
        }
        try {
            Map<String, List<String>> paramRequest = paramRequestProcessor.getParamRequest(endpoint, configResponseModelBySystemConfigId.getAggrEndpointId());
            Map<String, List<String>> paramResponse = paramResponseProcessor.getParamResponse(endpoint, configResponseModelBySystemConfigId.getAggrEndpointId());

            configResponseModelBySystemConfigId.setMapParamsRequest(paramRequest);
            configResponseModelBySystemConfigId.setMapParamResponse(paramResponse);
            return configResponseModelBySystemConfigId;
        } catch (NullPointerException e) {
            return new ConfigResponseModel();
        }
    }


    public SystemConfigModel getDetail(String id) throws NotFoundEntityException {
        log.info("Get detail system config Id {}", id);
        return transformer.toModel(service.findById(id));
    }

    public List<SystemConfigModel> getAll() {
        log.info("Get detail of all system config");
        List<SystemConfigModel> systemConfigModels = transformer.toModels(service.findAll());
        log.info("Success");
        return systemConfigModels;
    }


    public List<SystemConfigModel> createSystemConfigs(List<SystemConfigModel> systemConfigModels, String jwtToken) {
        String userName = reader.parse(jwtToken).getUserName();
        List<SystemConfigModel> collect = systemConfigModels.stream().map(e -> {
            setInformationCreate(e, userName);
            return e;
        }).collect(Collectors.toList());
        List<SystemConfig> systemConfigs = transformer.toEntities(collect);
        log.info("Create System Config {} By {}", systemConfigs, userName);
        try {
            return transformer.toModels(service.saveSystemConfig(systemConfigs));
        } catch (Exception e) {
            log.error("Error to create System config");
            log.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    public SystemConfigModel disableSystemConfig(String id, String jwtToken) {
        String userName = reader.parse(jwtToken).getUserName();
        try {
            log.info("Delete System Config Id {} by {}", id, userName);
            return transformer.toModel(service.disableSystemConfig(id, userName));
        } catch (NotFoundEntityException e) {
            log.error("Can't find system config id {}", id);
            return null;
        }
    }


    private void setInformationCreate(SystemConfigModel systemConfigModel, String userName) {
        systemConfigModel.setCreatedBy(userName);
        systemConfigModel.setCreatedDate(LocalDateTime.now());
        systemConfigModel.setUpdatedDate(null);
        systemConfigModel.setUpdatedBy(null);
    }

    public SystemConfigModel updateActiveStatus(String id) throws NotFoundEntityException {
        return transformer.toModel(service.updateActive(id));
    }

    @Transactional
    public SystemConfigModel create(SystemConfigRequest createRequest) throws ValidateException {
        validateLogic(createRequest, null);
        SystemConfig systemConfig = transformer.toSystemConfig(createRequest);
        return transformer.toModel(service.saveSystemConfig(systemConfig));
    }

    @Transactional
    public SystemConfigModel update(String id, SystemConfigRequest systemConfigModel) throws NotFoundEntityException, ValidateException {
        SystemConfig systemConfig = service.get(id).orElseThrow(() -> NotFoundEntityException.of(Message.SYSTEM_CONFIG_NOT_FOUND));
        validateLogic(systemConfigModel, id);
        transformer.partialUpdate(systemConfig, systemConfigModel);
        service.updateSystemConfig(systemConfig);
        return transformer.toModel(systemConfig);
    }

    @Transactional
    public SystemConfigModel delete(String id) throws NotFoundEntityException {
        SystemConfig systemConfig = service.get(id).orElseThrow(() -> NotFoundEntityException.of(Message.SYSTEM_CONFIG_NOT_FOUND));
        service.delete(systemConfig);
        return transformer.toModel(systemConfig);
    }

    private void validateLogic(SystemConfigRequest systemConfigModel, String id) throws ValidateException {
        Optional<SystemConfig> opt = service.findByBaseUrl(systemConfigModel.getBaseUrl(), id);
        if (opt.isPresent()) {
            throw new ValidateException("Base url exists", Collections.singletonList(ErrorDetail.create("baseUrl", "Base url exists")));
        }
    }

}

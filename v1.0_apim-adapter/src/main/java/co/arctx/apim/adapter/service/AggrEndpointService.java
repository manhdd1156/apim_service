package co.arctx.apim.adapter.service;

import co.arctx.apim.adapter.entity.AggrEndpoint;
import co.arctx.apim.adapter.repository.AggrEndpointRepository;
import co.arctx.apim.adapter.utils.Message;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Service
public class AggrEndpointService extends CommonService<AggrEndpoint, String, AggrEndpointRepository> {

    protected AggrEndpointService(AggrEndpointRepository repo) {
        super(repo);
    }


    public List<AggrEndpoint> findAllAggrEndpoint() {
        return all(Sort.Order.asc("endpoint"));
    }

    public AggrEndpoint findAggrEndpointById(String id) throws NotFoundEntityException {
        return getOrElseThrow(id);
    }

    public List<AggrEndpoint> saveAggrEndpoint(List<AggrEndpoint> aggrEndpoints) {
        return save(aggrEndpoints);
    }

    public AggrEndpoint saveAggrEndpoint(AggrEndpoint endpoint) {
        return save(endpoint);
    }

    public AggrEndpoint updateAggrEndpoint(AggrEndpoint aggrEndpoint) throws NotFoundEntityException {
        return updateOnField(aggrEndpoint.getId(), e -> {
            e.setEndpoint(aggrEndpoint.getEndpoint());
            e.setUpdateBy(aggrEndpoint.getUpdateBy());
            e.setUpdatedDate(LocalDateTime.now());
        });
    }

    public AggrEndpoint deleteAggrEndpoint(String id) {
        return deleteIfExisted(id);
    }


    @Override
    protected String notFoundMessage() {
        return Message.AGGR_ENDPOINT_NOT_FOUND;
    }


}

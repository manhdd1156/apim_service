package co.arctx.apim.adapter.enums;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
public enum AuthValueType {
    PASSKEY,
    AUTHORIZATION,
    TOKEN,
}

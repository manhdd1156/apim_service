package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AggrParamRequestModel {

    String id;
    String aggrParamRequest;
    Long aggrEndpointId;
    String parentId;
    String createdBy;
    LocalDateTime createdDate;
    String updateBy;
    LocalDateTime updateDate;
    String objectType;
}

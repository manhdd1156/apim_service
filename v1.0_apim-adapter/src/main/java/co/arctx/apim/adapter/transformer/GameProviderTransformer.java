package co.arctx.apim.adapter.transformer;

import co.arctx.apim.adapter.entity.GameProvider;
import co.arctx.apim.adapter.model.GameProviderModel;
import org.mapstruct.Mapper;

/**
 * @author: Nam Vu
 * Jul 20, 2022
 */
@Mapper(componentModel = "spring")
public interface GameProviderTransformer extends EntitiesMapper<GameProvider, GameProviderModel> {
}

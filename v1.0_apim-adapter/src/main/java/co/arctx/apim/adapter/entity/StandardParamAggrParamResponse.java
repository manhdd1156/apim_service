package co.arctx.apim.adapter.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * @author: Nam Vu
 * Jun 22, 2022
 */

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StandardParamAggrParamResponse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "standard_param_response_id")
    StandardParamResponse standardParamResponse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aggr_param_response_id" )
    AggrParamResponse aggrParamResponse;


}

package co.arctx.apim.adapter.model;

import co.arctx.apim.adapter.entity.SystemConfigGameProvider;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Nam Vu
 * Jul 22, 2022
 */

@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SystemConfigGameProviderModel {
    Long id;
    String systemConfigId;
    String gameProviderId;
    String providerName;

    String gameListId;
    String listGame;

    public SystemConfigGameProviderModel(Long id, String systemConfigId, String gameProviderId, String providerName, String gameListId) {
        this.id = id;
        this.systemConfigId = systemConfigId;
        this.gameProviderId = gameProviderId;
        this.providerName = providerName;
        this.gameListId = gameListId;
    }
}

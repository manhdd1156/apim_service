package co.arctx.apim.adapter.processor;

import co.arctx.apim.adapter.entity.QAggrParamRequest;
import co.arctx.apim.adapter.model.MapDataRequestModel;
import co.arctx.apim.adapter.service.AggrParamService;
import co.arctx.apim.adapter.transformer.AggrParamRequestTransformerImpl;
import co.arctx.core.common.CommonProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Component
@Slf4j
public class AggrParamRequestProcessor extends CommonProcessor<AggrParamService, QAggrParamRequest, AggrParamRequestTransformerImpl> {

    protected AggrParamRequestProcessor(AggrParamService service, AggrParamRequestTransformerImpl transformer) {
        super(QAggrParamRequest.aggrParamRequest1, service, transformer);
    }

    @Transactional
    public Map<String, List<String>> getParamRequest(String endpoint, String aggrEndpointId) {
        HashMap<String, List<String>> paramsMapRequest = new HashMap<>();
        List<MapDataRequestModel> list = service.getAggrParamByEndpointId(endpoint, aggrEndpointId);
        for (MapDataRequestModel data : list) {
            paramsMapRequest.put(data.getStandardParamRequest(),
                    Arrays.asList(
                            data.getAggrParamRequest(),
                            data.getAggrObjectType(),
                            data.getAggrDataType().name()
                    ));
        }
        return paramsMapRequest;
    }

}

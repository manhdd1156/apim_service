package co.arctx.apim.adapter.api.internal;

import co.arctx.apim.adapter.entity.AggrParamRequest;
import co.arctx.apim.adapter.entity.StandardEndpoint;
import co.arctx.apim.adapter.entity.StandardParamAggrParamRequest;
import co.arctx.apim.adapter.entity.StandardParamRequest;
import co.arctx.apim.adapter.model.CreateRequestParam;
import co.arctx.apim.adapter.repository.AggrParaRequestRepository;
import co.arctx.apim.adapter.repository.StandardEndpointRepository;
import co.arctx.apim.adapter.repository.StandardParamAggrParamRequestRepository;
import co.arctx.apim.adapter.repository.StandardParamRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author: Son Tran
 * Jun 29, 2022
 */
@RestController
public class AdapterInternalCreateApi {
    @Autowired
    private StandardEndpointRepository standardEndpointRepository;
    @Autowired
    private StandardParamAggrParamRequestRepository standardParamAggrParamRequestRepository;
    @Autowired
    private AggrParaRequestRepository aggrParaRequestRepository;
    @Autowired
    private StandardParamRequestRepository standardParamRequestRepository;

    @PostMapping("/create/request")
    public void createRequest(@RequestBody List<CreateRequestParam> maps, @RequestParam("id") String standPointId) {
        StandardEndpoint standardEndpoint = standardEndpointRepository.getOne(standPointId);
        maps.stream().forEach(m -> {
            StandardParamAggrParamRequest standardParamAggrParamRequest = new StandardParamAggrParamRequest();
            StandardParamRequest standardParamRequest = new StandardParamRequest();
            standardParamRequest.setCreatedBy("tcson");
            standardParamRequest.setStandardParamRequest(m.getStdRequest());
            standardParamRequest.setStandardEndpointId(standardEndpoint);
            standardParamAggrParamRequest.setStandardParamRequest(standardParamRequestRepository.save(standardParamRequest));
            m.getRequest().stream().forEach(r -> {
                AggrParamRequest aggrParamRequest = new AggrParamRequest();
                aggrParamRequest.setAggrParamRequest(r.getAggrParamRequest());
                aggrParamRequest.setCreatedBy("tcson");
                standardParamAggrParamRequest.setAggrParamRequest(aggrParaRequestRepository.save(aggrParamRequest));
                standardParamAggrParamRequestRepository.save(standardParamAggrParamRequest);
            });

        });

    }
}

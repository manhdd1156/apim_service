package co.arctx.apim.adapter.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Map;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ConfigResponseModel {
    String baseUrl;
    String aggrEndpoint;
    String httpMethod;
    String aggrEndpointId;

    String systemName;

    String token;

    Map<String, List<String>> mapParamsRequest;

    Map<String, List<String>> mapParamResponse;

    public ConfigResponseModel(String baseUrl, String aggrEndpoint, String httpMethod, String aggrEndpointId, String token, String systemName) {
        this.systemName = systemName;
        this.baseUrl = baseUrl;
        this.aggrEndpoint = aggrEndpoint;
        this.httpMethod = httpMethod;
        this.aggrEndpointId = aggrEndpointId;
        this.token = token;
    }

    public ConfigResponseModel(String baseUrl, String aggrEndpoint, String httpMethod, String aggrEndpointId, String systemName) {
        this.baseUrl = baseUrl;
        this.aggrEndpoint = aggrEndpoint;
        this.httpMethod = httpMethod;
        this.aggrEndpointId = aggrEndpointId;
        this.systemName = systemName;
    }
}

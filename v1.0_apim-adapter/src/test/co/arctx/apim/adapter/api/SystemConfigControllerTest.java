package co.arctx.apim.adapter.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import co.arctx.apim.adapter.model.SystemConfigModel;
import co.arctx.apim.adapter.model.request.SystemConfigRequest;
import co.arctx.apim.adapter.processor.SystemConfigProcessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = SystemConfigController.class)
class SystemConfigControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private SystemConfigProcessor processor;

  @BeforeEach
  void setUp() {
    initMocks(this);
  }

  String BASE_URL_API = "/api/v1/adapters/system-config";
  String BASE_URL = "192.168.10.236:9091";

  SystemConfigModel systemConfigModel(){
    SystemConfigModel instance = new SystemConfigModel();
    instance.setId("sc2");
    instance.setSystemName("PariPlay");
    instance.setProviderType("providerType");
    instance.setAuthType("authType");
    instance.setAuthValue("authValue");
    instance.setBaseUrl("192.168.10.236:9091");
    instance.setCreatedBy("DatHV");
    instance.setCreatedDate(LocalDateTime.of(2022, 1, 1, 0, 0, 0));
    instance.setUpdatedBy("DatHV");
    instance.setUpdatedDate(LocalDateTime.of(2022, 1, 1, 0, 0, 0));
    instance.setStatus("1");
    instance.setDescription("Pari system config");
    return instance;
  }

  @Test
  @DisplayName("TEST - updateActiveStatus")
  void testUpdateActiveStatus() throws Exception {
    // Setup
    String ENDPOINT = "/active/status";
    SystemConfigModel systemConfigModel = systemConfigModel();

    when(processor.updateActiveStatus(BASE_URL)).thenReturn(systemConfigModel);

    // Run the test
    mockMvc.perform(put(BASE_URL_API + ENDPOINT)
        .param("baseUrl", BASE_URL))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith("application/json"));

    // Verify the results
    assertEquals("Pari system config", processor.updateActiveStatus(BASE_URL).getDescription());
  }

  @Test
  @DisplayName("TEST - getAll")
  void testGetAll() throws Exception {
    // Setup
    SystemConfigModel systemConfigModel = systemConfigModel();
    SystemConfigModel systemConfigModel1 = systemConfigModel();
    final List<SystemConfigModel> systemConfigModels = Arrays.asList(systemConfigModel, systemConfigModel1);

    when(processor.getAll()).thenReturn(systemConfigModels);

    // Run the test
    mockMvc.perform(get(BASE_URL_API))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith("application/json"));

    // Verify the results
    assertEquals(processor.getAll().get(0).getCreatedBy(), processor.getAll().get(1).getCreatedBy());
  }

  @Test
  @DisplayName("TEST - getById")
  void testGetById() throws Exception {
    // Setup
    String id = "sc2";
    final SystemConfigModel systemConfigModel = systemConfigModel();

    when(processor.getDetail(anyString())).thenReturn(systemConfigModel);

    // Run the test
    mockMvc.perform(get(BASE_URL_API+"/{id}", id))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith("application/json"));

    // Verify the results
    assertEquals(id, systemConfigModel.getId());
  }

  @Test
  @DisplayName("TEST - create")
  void testCreate() throws Exception {
    // Setup
    final SystemConfigRequest systemConfigRequest = new SystemConfigRequest("PariPlay", "authType", "192.168.10.236:9091", "Pari system config");
    final SystemConfigModel systemConfigModel = systemConfigModel();

    when(processor.create(systemConfigRequest)).thenReturn(systemConfigModel);

    // Run the test
    mockMvc.perform(post(BASE_URL_API)
        .content(new ObjectMapper().writeValueAsString(systemConfigRequest))
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith("application/json"));

    // Verify the results
    assertEquals("PariPlay", processor.create(systemConfigRequest).getSystemName());
  }

  @Test
  @DisplayName("TEST - update")
  void testUpdate() throws Exception {
    // Setup
    String id = "sc2";
    final SystemConfigRequest systemConfigRequest = new SystemConfigRequest("PariPlay", "authType", "192.168.10.236:9091", "Pari system config");
    final SystemConfigModel systemConfigModel = systemConfigModel();

    when(processor.update(anyString(), any(SystemConfigRequest.class))).thenReturn(systemConfigModel);

    // Run the test

    mockMvc.perform(put(BASE_URL_API + "/{id}", id)
        .content(new ObjectMapper().writeValueAsString(systemConfigRequest))
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith("application/json"));

    // Verify the results
    assertEquals("PariPlay", processor.update(id, systemConfigRequest).getSystemName());
  }

  @Test
  @DisplayName("TEST - delete")
  void testDelete() throws Exception {
    // Setup
    String id = "sc2";
    final SystemConfigModel systemConfigModel = systemConfigModel();

    when(processor.delete(anyString())).thenReturn(systemConfigModel);

    // Run the test
    mockMvc.perform(delete(BASE_URL_API + "/{id}", id))
        .andDo(print())
        .andExpect(status().isOk());

    // Verify the results
    assertNotNull(processor.delete(id).getId());
  }
}

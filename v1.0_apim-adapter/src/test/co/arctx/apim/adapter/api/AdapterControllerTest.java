package co.arctx.apim.adapter.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import co.arctx.apim.adapter.model.ConfigResponseModel;
import co.arctx.apim.adapter.model.StandardEndpointRequestModel;
import co.arctx.apim.adapter.processor.SystemConfigProcessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = AdapterController.class)
class AdapterControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SystemConfigProcessor processor;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    String BASE_URL_API = "/api/v1/adapters";
    String BASE_URL = "192.168.10.236:9091";

    @Test
    @DisplayName("TEST - getConfig")
    void testGetConfig() throws Exception {
        // Setup
        String ENDPOINT = "/get-config";
        String TOKEN = "sc1";
        String ENDPOINT_SERM = "/Resettlement";

        final StandardEndpointRequestModel endpoint = new StandardEndpointRequestModel(ENDPOINT_SERM, TOKEN);
        final ConfigResponseModel configResponseModel = new ConfigResponseModel();
        configResponseModel.setBaseUrl(BASE_URL);
        configResponseModel.setAuthType("authType");
        configResponseModel.setAggrEndpoint("/Bets");
        configResponseModel.setHttpMethod("httpMethod");

        when(processor.getModelResponse(anyString(), anyString())).thenReturn(configResponseModel);

        // Run the test
        mockMvc.perform(post(BASE_URL_API + ENDPOINT)
                        .content(new ObjectMapper().writeValueAsString(endpoint))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        // Verify the results
        assertEquals("192.168.10.236:9091", processor.getModelResponse(ENDPOINT_SERM, TOKEN).getBaseUrl());
    }
}

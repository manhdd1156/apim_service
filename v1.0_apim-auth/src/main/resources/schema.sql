CREATE Database apim_auth;
use apim_auth;
go
CREATE TABLE `role`
(
    id        BIGINT AUTO_INCREMENT NOT NULL,
    role_name VARCHAR(255)          NULL,
    CONSTRAINT pk_role PRIMARY KEY (id)
);
go
CREATE TABLE role_user
(
    role_id BIGINT NOT NULL,
    user_id BIGINT NOT NULL
);
go
CREATE TABLE user
(
    id          BIGINT AUTO_INCREMENT NOT NULL,
    username    VARCHAR(255)          NULL,
    password    VARCHAR(255)          NULL,
    is_disabled BIGINT                NULL,
    CONSTRAINT pk_user PRIMARY KEY (id)
);

ALTER TABLE user
    ADD CONSTRAINT uc_user_username UNIQUE (username);

ALTER TABLE role_user
    ADD CONSTRAINT fk_role_user_on_role FOREIGN KEY (role_id) REFERENCES `role` (id);

ALTER TABLE role_user
    ADD CONSTRAINT fk_role_user_on_user FOREIGN KEY (user_id) REFERENCES user (id);
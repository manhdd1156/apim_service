package com.arctx.apim.auth.api;

import com.arctx.apim.auth.model.UserModel;
import com.arctx.apim.auth.processor.AuthProcessor;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/users")
public class UserApi {

    private final AuthProcessor processor;

    @ApiOperation(value = "Create User", response = UserModel.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Successfully"), @ApiResponse(code = 500, message = "Server error")})
    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@RequestBody List<@Valid UserModel> userModel) {
        return ResponseEntity.ok(processor.addUser(userModel));
    }


    @ApiOperation(value = "Update User", response = UserModel.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Successfully"), @ApiResponse(code = 500, message = "Server error")})
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody @Valid UserModel userModel, @PathVariable("id") Long id) {
        userModel.setId(id);
        return ResponseEntity.ok(processor.updateUser(userModel));
    }

    @ApiOperation(value = "Enable User", response = UserModel.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Successfully"), @ApiResponse(code = 500, message = "Server error")})
    @PutMapping("/enable/{id}")
    public ResponseEntity<Object> enableUser(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(processor.enableUser(id));
    }

    @ApiOperation(value = "Disable User", response = UserModel.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Successfully"), @ApiResponse(code = 500, message = "Server error")})
    @PutMapping("/disable/{id}")
    public ResponseEntity<Object> disableUser(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(processor.disableUser(id));
    }


}

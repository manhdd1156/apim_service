package com.arctx.apim.auth.processor;

import co.arctx.core.common.CommonProcessor;
import co.arctx.core.exception.UnauthorizedException;
import com.arctx.apim.auth.entity.QUser;
import com.arctx.apim.auth.entity.Role;
import com.arctx.apim.auth.entity.User;
import com.arctx.apim.auth.model.JwtResponse;
import com.arctx.apim.auth.model.UserModel;
import com.arctx.apim.auth.service.AuthService;
import com.arctx.apim.auth.transformer.AuthTransformer;
import com.arctx.apim.auth.utils.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AuthProcessor extends CommonProcessor<AuthService, QUser, AuthTransformer> {
    private JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();

    protected AuthProcessor(AuthService service, AuthTransformer transformer) {
        super(QUser.user, service, transformer);
    }


    public JwtResponse getJwtToken(String username, String password) throws UnauthorizedException {
        JwtResponse respone = new JwtResponse();
        UserDetails userDetails = service.getByUserNameAndPassword(username, password);
        respone.setJwtToken(jwtTokenUtil.generateToken(userDetails));
        respone.setRefreshToken(jwtTokenUtil.generateRefreshToken(userDetails));
        return respone;
    }

    public UserModel addUser(UserModel userModel) {
        User userEntity = transformer.toEntity(userModel);
        List<Role> roles = new ArrayList<>();
        for (Role r : userEntity.getRoles()) {
            Role role = service.getRoleById(r.getId());
            role.setUsers(Arrays.asList(userEntity));
            roles.add(role);
        }
        userEntity.setRoles(roles);
        return transformer.toModel(service.save(userEntity));
    }

    public String getJwtTokenForRefreshToken(String refreshToken) throws UnauthorizedException {
        if (jwtTokenUtil.validateRefreshToken(refreshToken)) {

            return jwtTokenUtil.generateToken(refreshToken);
        }
        throw new UnauthorizedException("", "refresh token is invalid");
    }

    public UserModel getUserById(Long id) {
        return transformer.toModel(service.getUser(id));
    }

    public List<UserModel> getAllUser() {
        try {
            return service.getAllUser().stream().map(transformer::toModel).collect(Collectors.toList());
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }

    public List<UserModel> addUser(List<UserModel> userModel) {
        List<User> collect = userModel.stream().map(u -> {
            List<Role> roles = service.getRoleById(u.getRoles());
            User user = transformer.toEntity(u);
            user.setRoles(roles);
            return user;
        }).collect(Collectors.toList());
        return service.save(collect).stream().map(transformer::toModel).collect(Collectors.toList());
    }

    public UserModel updateUser(UserModel userModel) {
        User user = transformer.toEntity(userModel);
        List<Role> roles = service.getRoleById(userModel.getRoles());
        user.setRoles(roles);
        return transformer.toModel(service.update(user));
    }

    public UserModel enableUser(Long id) {
        User user = service.getUser(id);
        user.setIsDisabled(0L);
        return transformer.toModel(service.update(user));
    }

    public UserModel disableUser(Long id) {
        User user = service.getUser(id);
        user.setIsDisabled(1L);
        return transformer.toModel(service.update(user));
    }


}

package com.arctx.apim.auth.api;

import com.arctx.apim.auth.model.JwtResponse;
import com.arctx.apim.auth.model.LoginReq;
import com.arctx.apim.auth.processor.AuthProcessor;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@AllArgsConstructor
@RequestMapping("/authentication")
public class AuthApi {
    private final AuthProcessor authProcessor;
    @PostMapping
    public ResponseEntity<JwtResponse> auth(@RequestBody LoginReq user, HttpServletResponse response) throws Exception {
        return ResponseEntity.ok(authProcessor.getJwtToken(user.getUsername(),user.getPassword()));
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<String> auth(@RequestBody String refreshToken) throws Exception {
        return ResponseEntity.ok(authProcessor.getJwtTokenForRefreshToken(refreshToken));
    }

}

package com.arctx.apim.auth.service;

import co.arctx.core.common.CommonService;
import co.arctx.core.exception.UnauthorizedException;
import com.arctx.apim.auth.entity.Role;
import com.arctx.apim.auth.entity.User;
import com.arctx.apim.auth.model.UserDetailsCustom;
import com.arctx.apim.auth.repository.AuthRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class AuthService extends CommonService<User, Long, AuthRepository> {
    private final AuthRepository authRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final String CANT_FIND_USER = "Can't not find user";

    AuthService(AuthRepository authRepo) {
        super(authRepo);
        this.authRepository = authRepo;
    }

    @Override
    protected String notFoundMessage() {
        return null;
    }

    public User loadUserByUsername(String username) throws UnauthorizedException {

        List<User> users = authRepository.getUserByUsername(username);
        if (Objects.isNull(users) || users.isEmpty()) {
            throw new UnauthorizedException("username", "username is not found");
        }

        return users.get(0);
    }

    public List<Role> getRoleById(List<Long> id) {
        return authRepository.getRoleByIds(id);
    }

    public UserDetails getByUserNameAndPassword(String username, String password) throws UnauthorizedException {

        User user = loadUserByUsername(username);
        if (!passwordEncoder.matches(password, user.getPassword())) {
            log.error("password is incorrect");
            throw new UnauthorizedException(null, "password or username is incorrect");

        }
        log.info("login is success");
        return new UserDetailsCustom(user);
    }

    public Role getRoleById(Long id) {
        return authRepository.getRole(id);
    }

    @Override
    public List<User> save(List<User> users) {
        return authRepository.saveAll(users);
    }


    public User update(User user) {
        User userDB = authRepository.findById(user.getId()).orElseThrow(() -> new UsernameNotFoundException(CANT_FIND_USER));
        userDB.setUsername(user.getUsername());
        userDB.setPassword(user.getPassword());
        userDB.setRoles(user.getRoles());
        return authRepository.save(userDB);
    }

    public User getUser(Long id) {
        return authRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException(CANT_FIND_USER));
    }

    public List<User> getAllUser() {
        return authRepository.findAll();
    }

    public boolean deleteUser(Long id) {
        User user = authRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException(CANT_FIND_USER));
        try {
            authRepository.delete(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}

package com.arctx.apim.auth.repository;

import co.arctx.core.common.CommonRepository;
import com.arctx.apim.auth.entity.Role;
import com.arctx.apim.auth.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthRepository extends CommonRepository<User,Long> {
    List<User> getUserByUsername(String username);
    @Query("select distinct r from User u left join u.roles r where u.id = :id")
    List<Role> getRoleById(@Param("id") Long id);

    @Query("select distinct r from Role r where r.id =:id")

    Role getRole(@Param("id") Long id);
    @Query("select distinct r from Role  r where r.id in :ids")
    List<Role> getRoleByIds(@Param("ids") List<Long> ids);
}

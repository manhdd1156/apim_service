package com.arctx.apim.auth.transformer;

import co.arctx.core.common.CommonTransformer;
import com.arctx.apim.auth.entity.Role;
import com.arctx.apim.auth.entity.User;
import com.arctx.apim.auth.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AuthTransformer implements CommonTransformer<User, UserModel> {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User toEntity(UserModel model) {

        User user = new User();
        user.setId(model.getId());
        user.setUsername(model.getUsername());
        user.setPassword(passwordEncoder.encode(model.getPassword()));
        user.setRoles(idToRoles(model.getRoles()));
        return user;
    }

    @Override
    public UserModel toModel(User entity) {
        UserModel userDTO = new UserModel();
        userDTO.setUsername(entity.getUsername());
        userDTO.setId(entity.getId());
        userDTO.setRoleNames(rolesToString(entity.getRoles()));
        return userDTO;

    }

    public List<String> rolesToString(List<Role> roles) {
        List<String> roleStrings = new ArrayList<>();
        roles.forEach(r ->
                roleStrings.add(r.getRoleName())
        );
        return roleStrings;
    }

    public List<Role> idToRoles(List<Long> roleIds) {
        List<Role> roles = new ArrayList<>();
        roleIds.forEach(s -> {
            Role role = new Role();
            role.setId(s);
            roles.add(role);
        });
        return roles;
    }

}

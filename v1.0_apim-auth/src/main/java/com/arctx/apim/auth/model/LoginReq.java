package com.arctx.apim.auth.model;

import lombok.Data;

@Data
public class LoginReq {
    private String username;
    private String password;
}

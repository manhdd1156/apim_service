package com.arctx.apim.auth.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.*;

@Slf4j
@Component
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -2550185165626007488L;

    public static final long JWT_TOKEN_VALIDITY = 60L * 60L * 1000L;
    public static final long REFRESH_TOKEN_VALIDITY = 24L * 60L * 60L * 1000L;


    private String serect = "qpfK6gr0E6VngaIKEj84";
    private String serectRefresh = "qpfK6gr0E6VngaIKEj84sa";


    public Claims getAllClaimsFromToken(String token, String serect) throws MalformedJwtException {

        return Jwts.parser().setSigningKey(serect.getBytes()).parseClaimsJws(token).getBody();

    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", userDetails.getUsername());
        List<String> authorities = new ArrayList<>();
        userDetails.getAuthorities().forEach(c -> authorities.add(c.getAuthority()));
        claims.put("authorities", authorities);
        return doGenerateToken(claims);
    }

    public String generateToken(String refreshToken) {
        Claims claims = getAllClaimsFromToken(refreshToken, serectRefresh);
        return doGenerateToken(claims);
    }

    public String generateRefreshToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", userDetails.getUsername());
        List<String> authorities = new ArrayList<>();
        userDetails.getAuthorities().forEach(c ->
                authorities.add(c.getAuthority())
        );
        claims.put("authorities", authorities);
        return doGenerateRefreshToken(claims, serectRefresh);
    }

    private String doGenerateToken(Map<String, Object> claims) {

        return Jwts.builder().setClaims(claims).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS512, serect.getBytes()).compact();
    }

    private String doGenerateRefreshToken(Map<String, Object> claims, String serect) {
        return Jwts.builder().setClaims(claims).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + REFRESH_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS512, serect.getBytes()).compact();
    }

    public boolean validateRefreshToken(String refreshToken) {
        try {
            getAllClaimsFromToken(refreshToken, serectRefresh);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

}
package com.arctx.apim.auth.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserModel {
    private Long id;
    @NotEmpty(message = "username is not empty")
    @NotNull(message = "username is not null")
    private String username;
    @NotNull(message = "password is not null")
    @NotEmpty(message = "password is not empty")
    private String password;
    @NotNull(message = "roles is not null")
    private List<Long> roles;
    private List<String> roleNames;
    @NotNull(message = "isDisabled is not null")
    private Long isDisabled;
}

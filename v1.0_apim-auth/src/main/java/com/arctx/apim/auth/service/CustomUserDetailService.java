package com.arctx.apim.auth.service;

import com.arctx.apim.auth.entity.User;
import com.arctx.apim.auth.model.UserDetailsCustom;
import com.arctx.apim.auth.repository.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomUserDetailService implements UserDetailsService {
    @Autowired
    private AuthRepository authRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<User> users = authRepository.getUserByUsername(username);
        if (!users.isEmpty()) {
            return new UserDetailsCustom(users.get(0));
        }
        throw new UsernameNotFoundException("User details not found for the user : " + username);
    }
}

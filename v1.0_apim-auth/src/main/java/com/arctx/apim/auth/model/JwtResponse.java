package com.arctx.apim.auth.model;

import lombok.Data;

@Data
public class JwtResponse {
    String jwtToken;
    String refreshToken;
}

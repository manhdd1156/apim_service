package com.arctx.apim.auth.api;

import co.arctx.core.exception.UnauthorizedException;
import com.arctx.apim.auth.model.JwtResponse;
import com.arctx.apim.auth.model.LoginReq;
import com.arctx.apim.auth.processor.AuthProcessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class AuthApiTest {

    @Mock
    private AuthProcessor mockAuthProcessor;

    private AuthApi authApiUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        authApiUnderTest = new AuthApi(mockAuthProcessor);
    }

    @Test
    void testAuth1() throws Exception {
        // Setup
        final LoginReq user = new LoginReq();
        user.setUsername("username");
        user.setPassword("password");

        final HttpServletResponse response = new MockHttpServletResponse();
        final JwtResponse jwtResponse = new JwtResponse();
        jwtResponse.setJwtToken("jwtToken");
        jwtResponse.setRefreshToken("refreshToken");
        final ResponseEntity<JwtResponse> expectedResult = new ResponseEntity<>(jwtResponse, HttpStatus.OK);

        // Configure AuthProcessor.getJwtToken(...).
        final JwtResponse jwtResponse1 = new JwtResponse();
        jwtResponse1.setJwtToken("jwtToken");
        jwtResponse1.setRefreshToken("refreshToken");
        when(mockAuthProcessor.getJwtToken("username", "password")).thenReturn(jwtResponse1);

        // Run the test
        final ResponseEntity<JwtResponse> result = authApiUnderTest.auth(user, response);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testAuth1_AuthProcessorThrowsUnauthorizedException() throws Exception {
        // Setup
        final LoginReq user = new LoginReq();
        user.setUsername("username");
        user.setPassword("password");

        final HttpServletResponse response = new MockHttpServletResponse();
        when(mockAuthProcessor.getJwtToken("username", "password")).thenThrow(UnauthorizedException.class);

        // Run the test
        assertThrows(UnauthorizedException.class, () -> authApiUnderTest.auth(user, response));
    }

    @Test
    void testAuth2() throws Exception {
        // Setup
        final ResponseEntity<String> expectedResult = new ResponseEntity<>("result", HttpStatus.OK);
        when(mockAuthProcessor.getJwtTokenForRefreshToken("refreshToken")).thenReturn("result");

        // Run the test
        final ResponseEntity<String> result = authApiUnderTest.auth("refreshToken");

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testAuth2_AuthProcessorThrowsUnauthorizedException() throws Exception {
        // Setup
        when(mockAuthProcessor.getJwtTokenForRefreshToken("refreshToken")).thenThrow(UnauthorizedException.class);

        // Run the test
        assertThrows(UnauthorizedException.class, () -> authApiUnderTest.auth("refreshToken"));
    }
}

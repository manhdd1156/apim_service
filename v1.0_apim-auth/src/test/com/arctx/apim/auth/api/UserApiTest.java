package com.arctx.apim.auth.api;

import com.arctx.apim.auth.model.UserModel;
import com.arctx.apim.auth.processor.AuthProcessor;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class UserApiTest {

    @Mock
    private AuthProcessor mockProcessor;

    private UserApi userApiUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        userApiUnderTest = new UserApi(mockProcessor);
    }

    @Test
    void testCreateUser() {
        // Setup
        final UserModel userModel = new UserModel();
        userModel.setId(0L);
        userModel.setUsername("username");
        userModel.setPassword("password");
        userModel.setRoles(Arrays.asList(0L));
        userModel.setRoleNames(Arrays.asList("value"));
        userModel.setIsDisabled(0L);
        final List<UserModel> UserModel = Arrays.asList(userModel);

        // Configure AuthProcessor.addUser(...).
        final com.arctx.apim.auth.model.UserModel userModel1 = new UserModel();
        userModel1.setId(0L);
        userModel1.setUsername("username");
        userModel1.setPassword("password");
        userModel1.setRoles(Arrays.asList(0L));
        userModel1.setRoleNames(Arrays.asList("value"));
        userModel1.setIsDisabled(0L);
        final List<com.arctx.apim.auth.model.UserModel> userModels = Arrays.asList(userModel1);
        when(mockProcessor.addUser(Arrays.asList(new UserModel()))).thenReturn(userModels);

        // Run the test
        final ResponseEntity<?> result = userApiUnderTest.createUser(UserModel);

        // Verify the results
        Assert.assertNotNull(result);
    }

    @Test
    void testCreateUser_AuthProcessorReturnsNoItems() {
        // Setup
        final UserModel userModel = new UserModel();
        userModel.setId(0L);
        userModel.setUsername("username");
        userModel.setPassword("password");
        userModel.setRoles(Arrays.asList(0L));
        userModel.setRoleNames(Arrays.asList("value"));
        userModel.setIsDisabled(0L);
        final List<UserModel> UserModel = Arrays.asList(userModel);
        when(mockProcessor.addUser(Arrays.asList(new UserModel()))).thenReturn(Collections.emptyList());

        // Run the test
        final ResponseEntity<?> result = userApiUnderTest.createUser(UserModel);

        // Verify the results
    }

    @Test
    void testUpdateUser() {
        // Setup
        final UserModel UserModel = new UserModel();
        UserModel.setId(0L);
        UserModel.setUsername("username");
        UserModel.setPassword("password");
        UserModel.setRoles(Arrays.asList(0L));
        UserModel.setRoleNames(Arrays.asList("value"));
        UserModel.setIsDisabled(0L);

        // Configure AuthProcessor.updateUser(...).
        final com.arctx.apim.auth.model.UserModel userModel = new UserModel();
        userModel.setId(0L);
        userModel.setUsername("username");
        userModel.setPassword("password");
        userModel.setRoles(Arrays.asList(0L));
        userModel.setRoleNames(Arrays.asList("value"));
        userModel.setIsDisabled(0L);
        when(mockProcessor.updateUser(new UserModel())).thenReturn(userModel);

        // Run the test
        final ResponseEntity<?> result = userApiUnderTest.updateUser(UserModel, 0L);

        // Verify the results
    }

    @Test
    void testEnableUser() {
        // Setup
        // Configure AuthProcessor.enableUser(...).
        final UserModel userModel = new UserModel();
        userModel.setId(0L);
        userModel.setUsername("username");
        userModel.setPassword("password");
        userModel.setRoles(Arrays.asList(0L));
        userModel.setRoleNames(Arrays.asList("value"));
        userModel.setIsDisabled(0L);
        when(mockProcessor.enableUser(0L)).thenReturn(userModel);

        // Run the test
        final ResponseEntity<?> result = userApiUnderTest.enableUser(0L);

        // Verify the results
    }

    @Test
    void testDisableUser() {
        // Setup
        // Configure AuthProcessor.disableUser(...).
        final UserModel userModel = new UserModel();
        userModel.setId(0L);
        userModel.setUsername("username");
        userModel.setPassword("password");
        userModel.setRoles(Arrays.asList(0L));
        userModel.setRoleNames(Arrays.asList("value"));
        userModel.setIsDisabled(0L);
        when(mockProcessor.disableUser(0L)).thenReturn(userModel);

        // Run the test
        final ResponseEntity<?> result = userApiUnderTest.disableUser(0L);

        // Verify the results
    }
}

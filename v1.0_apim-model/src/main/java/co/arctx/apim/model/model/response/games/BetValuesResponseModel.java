package co.arctx.apim.model.model.response.games;

import co.arctx.apim.model.model.response.ItemResponseModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;

/**
 * @author: Dat Hoang Van
 * June 23, 2022
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BetValuesResponseModel {
    String totalCount;
    ArrayList<ItemResponseModel> itemResponseModels;
}

package co.arctx.apim.model.model.response.enumerate;

public enum LinkType {
    Redirect,
    Ajax,
    AjaxResponse
}

package co.arctx.apim.model.model.request.notifications;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DetailRequestModel {
    //Common
    String betAmount;
    String winAmount ;
    String description;
    String refundAmount;
    String dealerCards;
    String playerCards;

    //Blackjack
    String position;
    String result;

    //Roulette
    String umber;
    String color;

    //BancaFrancesa
    String betDetails;

    //Baccarat
    String dealerHandValue;
    String playerHandValue;
    BetsRequestModel bets;
    WinningsRequestModel winnings;
}

package co.arctx.apim.model.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @author: Son Tran
 * Jul 05, 2022
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorCommonModel {
    private String errorCode;
    private String code;
    private String error;
    private String description;
    private String message;
}

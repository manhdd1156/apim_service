package co.arctx.apim.model.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Map;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ConfigResponseModel {
    String baseUrl;
    String authType;
    String aggrEndpoint;

    String systemName;
    String httpMethod;

    Map<String, List<String>> mapParamsRequest;

    Map<String, List<String>> mapParamResponse;

    public ConfigResponseModel(String baseUrl, String authType, String aggrEndpoint, String method) {
        this.baseUrl = baseUrl;
        this.authType = authType;
        this.aggrEndpoint = aggrEndpoint;
        this.httpMethod = method;
    }
}

package co.arctx.apim.model.model.request.games;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * June 20, 2022
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LobbyRequestModel {
    //Lobby QT
    String playerId;
    String displayName;
    String currency;
    String country;
    String gender;
    String birthDate;
    String languageCode;
    String mode;
    String device;
    String walletSessionId;
    String gameLaunchTarget;
    String gameTypes;
    String betLimitCode;
    String jurisdiction;
    ConfigRequestModel config;
    UrlsRequestModel urls;
}

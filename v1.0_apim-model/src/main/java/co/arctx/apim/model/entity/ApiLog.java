package co.arctx.apim.model.entity;
// Generated Feb 28, 2018 5:41:20 PM by Hibernate Tools 4.3.1

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "api_log")
public class ApiLog implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "api_url")
    private String apiUrl;
    @Column(name = "create_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;
    @Column(name = "duration")
    private String duration;
    @Column(name = "response_code")
    private int responseCode;
}

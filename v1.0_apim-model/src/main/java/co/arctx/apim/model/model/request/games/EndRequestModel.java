package co.arctx.apim.model.model.request.games;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * June 20, 2022
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EndRequestModel {

    //End PR
    String token;
    String gameCode;
    String playerId;
    String roundId;
    String transactionId;
    int[] transactionConfiguration;
    String roomId;
    String isBingo;
    AccountRequestModel accountRequestModel;
}

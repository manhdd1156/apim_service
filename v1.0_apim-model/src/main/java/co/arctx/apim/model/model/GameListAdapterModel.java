package co.arctx.apim.model.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * @author: Duy Manh
 * Jul 26, 2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameListAdapterModel {
    String id;
    String gameList;
    Long systemProviderId;
}

package co.arctx.apim.model.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: Nam Vu
 * Jul 12, 2022
 */
@Aspect
@Slf4j
@EnableAspectJAutoProxy
public class LogServiceConfig {

    @Around("execution(* co.arctx.apim.model.controller.*.*(..))")
    public void writeLog(ProceedingJoinPoint joinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        log.info("#START {} ", request.getRequestURI());
        Object proceed = joinPoint.proceed();
        log.info("#END {} - Response {}:", request.getRequestURI(), proceed);
    }
}

package co.arctx.apim.model.model.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StandardAggrImageModel {

    private String aggrUrl;
//    private String standardEndpointId;
//    private String aggrEndpointId;
//    private String standardParamResId;
//    private String aggrParamResId;
    private String systemProviderId;
    private String systemId;
    private String providerId;
    private String providerName;
    private String gameCode;


}

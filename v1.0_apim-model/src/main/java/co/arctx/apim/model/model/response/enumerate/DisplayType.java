package co.arctx.apim.model.model.response.enumerate;

public enum DisplayType {
    Notification,
    PopUp
}

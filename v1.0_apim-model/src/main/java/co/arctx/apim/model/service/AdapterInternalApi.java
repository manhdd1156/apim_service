//package co.arctx.apim.model.service;
//
//import co.arctx.core.config.FeignConfig;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import java.util.List;
//import java.util.Map;
//
///**
// * @author: Son Tran
// * Jul 25, 2022
// */
//
//@FeignClient(name = "apim-adapter", configuration = FeignConfig.class)
//@RequestMapping("/internal/api/v1/adapters")
//public interface AdapterInternalApi {
//    @PostMapping("/create-game-list")
//    ResponseEntity<Object> createGameList(@RequestBody Map<String, Object> listGame);
//
//    @GetMapping("/get-game-list")
//    ResponseEntity<Object> getGameList();
//}

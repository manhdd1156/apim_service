package co.arctx.apim.model.service;

import co.arctx.apim.model.ultils.ConvertModelUtils;
import co.arctx.core.exception.RestTemplateException;
import co.arctx.core.model.ResponseModel;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.LinkedHashMap;

/**
 * @author: Son Tran
 * Jun 23, 2022
 */
@Service
@Slf4j
public class HttpService {

    @Autowired
    private ModelService modelService;

    @Autowired
    private RestTemplate restTemplate;

    public LinkedHashMap<String,Object> sendHttpRequest(String targetURL,String method, JSONObject requestBody,HttpHeaders headers) throws RestClientException, RestTemplateException, MismatchedInputException {
        long start = System.currentTimeMillis();
        ResponseModel response = new ResponseModel();
        ResponseEntity<LinkedHashMap> responseEntity = null;
        
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(targetURL);

            String url = builder.toUriString() + ConvertModelUtils.pareToUrl(requestBody);
            if (HttpMethod.GET.name().equalsIgnoreCase(method)) {
                HttpEntity entity = new HttpEntity(headers);

                responseEntity = restTemplate.exchange(url,
                        HttpMethod.GET,
                        entity,
                        LinkedHashMap.class);
            } else {
                headers.setContentType(MediaType.APPLICATION_JSON);
                HttpEntity<String> entity = new HttpEntity(requestBody.toString(),headers);

                responseEntity = restTemplate.postForEntity(builder.toUriString(), entity, LinkedHashMap.class);
            }
            HttpStatus statusCode = responseEntity.getStatusCode();
            if (statusCode.isError()) {
                log.error("sendHttpRequest return errorCode {} with input {} ", statusCode.value(),requestBody);
            }
            response.setCode(statusCode.value());
            LinkedHashMap<String,Object> result = responseEntity.getBody();

            modelService.putLog(targetURL, start, statusCode.value());
            return result;

        } catch (Exception e) {
            modelService.putLog(targetURL, start, 500);
            log.error("callAPI ERROR", e);
            throw e;
        }
    }
}

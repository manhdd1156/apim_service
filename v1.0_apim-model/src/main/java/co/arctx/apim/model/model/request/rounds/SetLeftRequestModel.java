package co.arctx.apim.model.model.request.rounds;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SetLeftRequestModel {
    String playerId;
    String amount;
    String gameCode;
    String token;
    AccountRequestModel accountRequestModel;
}

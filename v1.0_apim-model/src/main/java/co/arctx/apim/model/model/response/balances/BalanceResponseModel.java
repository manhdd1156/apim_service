package co.arctx.apim.model.model.response.balances;

import co.arctx.apim.model.model.response.MessageResponseModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

/**
 * @author: Dat Hoang Van
 * June 27, 2022
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BalanceResponseModel {
    BigDecimal realBalance;
    BigDecimal bonusBalance;
    String currency;
    int roundsLeft;
    String timestamp;
    MessageResponseModel messageResponseModel;
    String brandId;
    String seq;
    int partyId;
    String omegaSessionKey;
    String errorCode;
}

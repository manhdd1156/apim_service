package co.arctx.apim.model.model.request.freerounds;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoveRequestModel {
    //Common
    String bonusId;
    String playerId;

    //Pr
    String countryCode;
    String brandId;
    AccountRequestModel accountRequestModel;

    //Qt
    String authorization;

}

package co.arctx.apim.model.ultils;

import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.MapEntry;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: Son Tran
 * Jun 23, 2022
 */
@Slf4j
public class ConvertModelUtils {
    public static JSONObject requestBlackBox(Map<String, List<String>> mapParams, Object model) {
        JSONObject resultObject = new JSONObject();
        try {
            Field[] allFields = model.getClass().getDeclaredFields();
            for (Field field : allFields) {
                field.setAccessible(true);
                Object value = field.get(model);
                if (value == null)
                    continue;
                List<String> targetParamName = mapParams.get(field.getName());
                if (value.equals("serialVersionUID"))
                    continue;
                if (Objects.nonNull(targetParamName)) {
                    if (value instanceof String) {
                        resultObject.put(targetParamName.get(0), parseObject(value.toString(), targetParamName.get(1)));
                    } else if (value instanceof Collection<?>) {
                        resultObject.put(targetParamName.get(0), ((Collection<?>) value).stream().map(v -> requestBlackBox(mapParams, v)).collect(Collectors.toList()));
                    } else if (value instanceof Object) {
                        resultObject.put(targetParamName.get(0), requestBlackBox(mapParams, value));
                    }
                }
            }
        } catch (Exception e) {
            log.error("Convert object have error :{}", e.getMessage());
            e.printStackTrace();
        }
        return resultObject;
    }
    public static JSONObject requestBlackBox(Map<String, List<String>> mapParams, Map<String,Object> model) {
        JSONObject resultObject = new JSONObject();
        try {
      model.entrySet().stream().forEach( map -> {

                if (map.getValue() == null)
                    return;
                List<String> targetParamName = mapParams.get(map.getKey());
                if (map.equals("serialVersionUID"))
                    return;
                if (Objects.nonNull(targetParamName)) {
                    if (map.getValue() instanceof String||map.getValue() instanceof Integer|| map.getValue() instanceof Long || map.getValue() instanceof Double) {
                        try {
                            resultObject.put(targetParamName.get(0), parseObject(map.getValue().toString(), targetParamName.get(1)));
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }

                    } else if (map.getValue() instanceof Collection<?>) {
                        try {
                            resultObject.put(targetParamName.get(0), ((Collection<?>) map.getValue()).stream().map(v -> requestBlackBox(mapParams, (Map<String, Object>) v)).collect(Collectors.toList()));
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    } else if (map.getValue() instanceof Object) {
                        try {
                            resultObject.put(targetParamName.get(0), requestBlackBox(mapParams, (Map<String, Object>) map.getValue()));
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            });
        } catch (Exception e) {
            log.error("Convert object have error :{}", e.getMessage());
            e.printStackTrace();
        }
        return resultObject;
    }

    public static Map<String, Object> responseBlackBox(Map<String, List<String>> mapParams, Map<String, Object> model) {
        Map<String, Object> result = new HashMap<>();

        try {
            for (Map.Entry<String, Object> entry : model.entrySet()) {
                List<String> keyStandard = mapParams.get(entry.getKey());
                if (Objects.isNull(entry.getValue()) || Objects.isNull(keyStandard))
                    continue;
                if (entry.getValue() instanceof String)
                    result.put(keyStandard.get(0), entry.getValue());
                else if (entry.getValue() instanceof Collection<?>)
                    result.put(keyStandard.get(0), ((Collection<?>) entry.getValue()).stream().map(v -> {

                       if (v instanceof Map) {
                            return responseBlackBox(mapParams, (Map<String, Object>) v);
                        }
                       else
                       {
                           return v;
                       }

                    }).collect(Collectors.toList()));
                else if (
                        entry.getValue() instanceof Number
                )
                    result.put(keyStandard.get(0), entry.getValue());
                else if (
                        entry.getValue() instanceof Boolean
                ) {
                    result.put(keyStandard.get(0), entry.getValue());
                } else if (entry.getValue() instanceof Object)
                    result.put(keyStandard.get(0), responseBlackBox(mapParams, (LinkedHashMap<String, Object>) entry.getValue()));
            }
        } catch (Exception e) {
            log.error("Convert object have error :{}", e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    private static Object parseObject(String value, String objectType) {
        switch (objectType) {

            case "INT":
                return Integer.parseInt(value);
            case "DOUBLE":
                return Double.parseDouble(value);
            case "FLOAT":
                return Float.parseFloat(value);
            case "BOOLEAN":
                return Boolean.parseBoolean(value);
            default:
                return value;
        }
    }

    public static String pareToUrl(JSONObject jsonObject) {
        StringBuilder builder = new StringBuilder("?");
        if (Objects.isNull(jsonObject.names()))
            return null;
        for (int i = 0; i < jsonObject.names().length(); i++) {
            try {
                builder.append(jsonObject.names().getString(i) + "=" + jsonObject.get(jsonObject.names().getString(i)) + "&");
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

        }
        return builder.toString();
    }

    private ConvertModelUtils() {
    }
}

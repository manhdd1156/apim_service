package co.arctx.apim.model.transformer;

import co.arctx.apim.model.entity.ApiLog;
import co.arctx.apim.model.model.ApiLogModel;
import co.arctx.core.common.CommonTransformer;
import org.springframework.stereotype.Component;

/**
 * @author: Son Tran
 * Jun 23, 2022
 */
@Component
public class ModelTransformer implements CommonTransformer<ApiLog, ApiLogModel> {
    @Override
    public ApiLog toEntity(ApiLogModel model) {
        return null;
    }

    @Override
    public ApiLogModel toModel(ApiLog entity) {
        return null;
    }

}

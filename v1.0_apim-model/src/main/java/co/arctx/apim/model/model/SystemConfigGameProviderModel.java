package co.arctx.apim.model.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * @author: Duy Manh
 * Jul 26, 2022
 */

@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SystemConfigGameProviderModel {
    Long id;
    String systemConfigId;
    String gameProviderId;
    String providerName;

    String gameListId;
    String listGame;

}

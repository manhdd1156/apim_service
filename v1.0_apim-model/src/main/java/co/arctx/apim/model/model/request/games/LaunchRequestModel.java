package co.arctx.apim.model.model.request.games;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * June 20, 2022
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LaunchRequestModel {
    //Common
    AccountRequestModel accountRequestModel;
    String gameId; //pr,qt,ss
    String brandId; //pr, ss
    String languageCode; //pr,qt,ss
    String tableCode; //pr,qt
    String playerId; //pr, qt
    String gender; //pr, qt
    String dateOfBirth; //pr, qt
    String currencyCode; //pr,qt
    String countryCode; //pr,qt



    //Launch PR
    String playerIP;
    String playerRegulation;
    String nickname;
    String homeUrl;
    String cashierUrl;
    String realityCheckInterval;
    String historyUrl;
    String affiliateCode;
    String showBuyMorePopup;
    String buyMoreURL;
    String platformType;
    String dontLoadUrlFromParent;
    String loggedInTime;
    String regulationSelfTest;
    String regulationLimits;
    String regulationSelfExclusion;

    //Launch QT
    String authorization;
    String displayName;
    String mode;
    String device;
    String returnUrl;
    String walletSessionId;
    String betLimitCode;
    String jurisdiction;
    String ipAddress;

    //Launch SSW
    String sessionKey;
    String platform;
    String playForReal;
    String isMobile;
    String playForFunCurrency;
}

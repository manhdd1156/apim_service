package co.arctx.apim.model.model.request.games;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * June 20, 2022
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LaunchDemoRequestModel {

    String gameCode;
    String currencyCode;
    String languageCode;
    String homeUrl;
    String cashierUrl;
    String playerIP;
    String financialMode;
    String affiliateCode;
    String ecommerceTicketPrice;
    String platformType;
    String dontLoadUrlFromParent;
    AccountRequestModel accountRequestModel;
}

package co.arctx.apim.model.ultils;

import co.arctx.apim.model.config.RestTemplateHandleError;
import co.arctx.core.utils.DynamicLinkUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author: quangdtptit
 * Jul 07, 2022
 */
@Slf4j
@Component
public final class HttpClientUtil {

    private HttpClientUtil() {
    }

    public static final long CONNECT_TIMEOUT = 60000L;
    public static final long READ_TIMEOUT = 60000L;

    public static String authToken;
    @Value("${softSwiss.authToken}")
    public void setAuthToken(String authToken){
        HttpClientUtil.authToken= authToken;
    }
    public static String casinoServerIp;
    @Value("${fundist.casinoServerIp}")
    public void setCasinoServerIp(String casinoServerIp)
    {
        HttpClientUtil.casinoServerIp =casinoServerIp;
    }
    public static String key;
    @Value("${fundist.key}")
    public void setKey(String key)
    {
        HttpClientUtil.key = key;
    }
    public static String pwd;
    @Value("${fundist.PWD}")
    public void setPwd(String pwd)
    {
        HttpClientUtil.pwd=pwd;
    }

//    @PostConstruct
//    void init() {
//        System.out.println(myBoolean); // you can see injected value on console
//    }

    public static <RP, RQ> ResponseEntity<RP> doRequest(String url, String method, RQ request, Class<RP> clazz) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        return doRequest(url, getHttpMethod(method), null, null, request, clazz);
    }

    public static <RP, RQ> ResponseEntity<RP> doRequest(String url, String method, Map<String, Object> headers, RQ request, Class<RP> clazz) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        return doRequest(url, getHttpMethod(method), headers, null, request, clazz);
    }

    public static <RP, RQ> ResponseEntity<RP> doRequest(String url, String method, Map<String, Object> headers, Map<String, Object> uriVariables, RQ request,
                                                        Class<RP> clazz) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        return doRequest(url, getHttpMethod(method), headers, uriVariables, request, clazz);
    }

    public static <RP> ResponseEntity<RP> doGet(String url, Map<String, Object> headers, Map<String, Object> uriVariables, Class<RP> clazz) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        return doRequest(url, HttpMethod.GET, headers, uriVariables, null, clazz);
    }

    public static <RP, RQ> ResponseEntity<RP> doPost(String url, Map<String, Object> headers, Map<String, Object> uriVariables, RQ request, Class<RP> clazz) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        return doRequest(url, HttpMethod.POST, headers, uriVariables, request, clazz);
    }

    public static <RP, RQ> ResponseEntity<RP> doPut(String url, Map<String, Object> headers, Map<String, Object> uriVariables, RQ request, Class<RP> clazz) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        return doRequest(url, HttpMethod.PUT, headers, uriVariables, request, clazz);
    }

    public static <RP, RQ> ResponseEntity<RP> doDelete(String url, Map<String, Object> headers, Map<String, Object> uriVariables, RQ request, Class<RP> clazz) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        return doRequest(url, HttpMethod.DELETE, headers, uriVariables, request, clazz);
    }


    public static <RP, RQ> ResponseEntity<RP> doRequest(String url, HttpMethod method, Map<String, Object> headers, Map<String, Object> uriVariables,
                                                        RQ request, Class<RP> clazz) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        log.info("=> Request HTTP\n[URI]={}\n[Method]={}\n[Header]={}\n[URI-Variable]={}\n[Body]={}", url, method, headers, uriVariables, request);

        HttpEntity<?> entityRequest = buildHttpEntity(headers, request);
        RestTemplate restTemplate = buildRestTemplate();
        restTemplate.setErrorHandler(new RestTemplateHandleError());
        ResponseEntity<RP> response = restTemplate.exchange(toUrlWithVariable(url, uriVariables), method, entityRequest, clazz);
        log.info("=> Response HTTP\n{}", response);
        return response;

    }

    public static void setSecurity(Map<String,Object> request, Map<String, Object> headers,String url,String systemName) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        if (systemName.equals("Softswiss"))
        {
            final Mac hmac = Mac.getInstance("HmacSHA256");
            hmac.init(new SecretKeySpec(authToken.getBytes("UTF-8"), "HmacSHA256"));
            byte[] signature = hmac.doFinal(request.toString().getBytes());
            String token = Hex.encodeHexString(signature);
            log.info("=> softswiss token = {}", token);
            headers.put("X-REQUEST-SIGN", token);
        }

        if (systemName.equals("Fundist"))
        {
            try {
                String uniqueTID = String.valueOf(request.get("TID"));
                if(Objects.isNull(request.get("TID"))) {
                    uniqueTID = UUID.randomUUID().toString();
                    uniqueTID = uniqueTID.length() > 32 ? uniqueTID.substring(0,31) : uniqueTID;
                    request.put("tid",uniqueTID);
                }
                String urlHash ="";
                if (url.contains("{Key}"))
                    urlHash = url.substring(url.lastIndexOf("}/")+2,url.lastIndexOf("/"));
                urlHash = urlHash + "/"+casinoServerIp+"/"+uniqueTID+"/"+key+"/"+pwd;
                String hash = DatatypeConverter.printHexBinary(MessageDigest.getInstance("MD5").digest(urlHash.getBytes("UTF-8")));
                request.put("hash",hash);
                request.put("key",key);

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }


    }

    private static MultiValueMap<String, String> toMultiValueMap(Map<String, Object> map) {
        if (CollectionUtils.isEmpty(map)) {
            //default header content-type
            map = new HashMap<>();
            map.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        }
        MultiValueMap<String, String> multi = new LinkedMultiValueMap<>();
        map.forEach((k, v) -> multi.put(k, Collections.singletonList(v == null ? null : v.toString())));
        return multi;
    }

    public static URL toURL(String url) {
        try {
            return new URL(url);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new IllegalArgumentException("URL invalid");
        }
    }

    public static String replacePathVariable(String url, Map<String, Object> mapPath) {
        AtomicReference<String> urlPath = new AtomicReference<>(url);
        mapPath.forEach((k, v) -> {
            String temp = urlPath.get();
            urlPath.set(temp.replace("{" + k + "}", String.valueOf(v)));
        });
        return urlPath.get();
    }

    public static HttpMethod getHttpMethod(String method) {
        for (HttpMethod httpMethod : HttpMethod.values()) {
            if (httpMethod.name().equalsIgnoreCase(method.trim())) {
                return httpMethod;
            }
        }
        throw new IllegalArgumentException(String.format("Method %s invalid", method));
    }

    private static RestTemplate buildRestTemplate() {
        return new RestTemplateBuilder()
                .setConnectTimeout(Duration.ofMillis(CONNECT_TIMEOUT))
                .setReadTimeout(Duration.ofMillis(READ_TIMEOUT))
                .build();
    }

    private static boolean isRequestJson(Map<String, Object> headers) {
        if (CollectionUtils.isEmpty(headers)) {
            return true;
        }
        Object value = headers.get(HttpHeaders.CONTENT_TYPE.toLowerCase());
        return Objects.isNull(value) || value.toString().trim().startsWith(MediaType.APPLICATION_JSON_VALUE.toLowerCase());
    }

    private static <RQ> HttpEntity<?> buildHttpEntity(Map<String, Object> headers, RQ request) {
        if (isRequestJson(headers)) {
            return new HttpEntity<>(objToMap(request), toMultiValueMap(headers));
        }
        return new HttpEntity<>(toMultiValueMap(objToMap(request)), toMultiValueMap(headers));
    }

    private static String toUrlWithVariable(String uri, Map<String, Object> uriVariables) {
        if (CollectionUtils.isEmpty(uriVariables)) {
            return uri;
        }
        StringBuilder temp = new StringBuilder("");
        if(!uri.endsWith("/?&")) { // case special ( Fundist system ) end with /?&
            temp.append("?");
        }
        uriVariables.forEach((k, v) -> temp.append(k).append("=").append(v).append("&"));
        return uri + temp.deleteCharAt(temp.length() - 1);
    }

    public static <T> String objToJson(T t) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
            return ow.writeValueAsString(t);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T jsonToObj(String json, Class<T> clazz) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public static <T> T jsonToObjGameList(Map<String, List<String>> mapParams,String json, Class<T> clazz) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            ObjectMapper mapper = new ObjectMapper();
            StringBuffer temp = new StringBuffer("{\"games\":");
            try {
                List<String> moreResponseCommon = new ArrayList<>();
                moreResponseCommon.add("games");
                moreResponseCommon.add("List<Object>");
                mapParams.put("games",moreResponseCommon);
                return mapper.readValue(temp.append(json).append("}").toString(), clazz);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public static <T> Map<String, Object> objToMap(T object) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return objectMapper.convertValue(object, Map.class);
    }

    public static <T> T mapToObj(Map<String, Object> map, Class<T> clazz) {
        return new ObjectMapper().convertValue(map, clazz);
    }



    public static Map<String, Object> getAllHeaders(HttpServletRequest request) {
        Map<String, Object> result = new HashMap<>();
        Enumeration<String> headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String key = headers.nextElement().toLowerCase();
            result.put(key, request.getHeader(key).toLowerCase());
        }
        return result;
    }

    public static HttpClientBuilder builder() {
        return new HttpClientBuilder();
    }

    public static class HttpClientBuilder {
        private String url;

        public HttpClientBuilder input(String url) {
            this.url = url;
            return this;
        }

        public HttpClientBuilder handlePathVariable(Map<String, Object> pathVariable) {
            this.url = HttpClientUtil.replacePathVariable(this.url, pathVariable);
            return this;
        }

        public HttpClientBuilder handleUriVariable(Map<String, Object> uriVariable) {

            this.url = HttpClientUtil.toUrlWithVariable(this.url, uriVariable);
            return this;
        }

        public HttpClientBuilder handleCompleteUrl(String baseUrl) {
            this.url = DynamicLinkUtils.getCompleteRequestUri(this.url, baseUrl);
            return this;
        }

        public String getUrl() {
            return this.url;
        }
    }
}

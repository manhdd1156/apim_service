package co.arctx.apim.model.model.request.notifications;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortugueseRoundDetailRequestModel {
    String smResult;
    String transactionType;
    String createdDate;
    String transactionId;
    String description;

    @JsonProperty("mQLIN")
    String mQLIN;

    String initialReturn;

    @JsonProperty("mQDOB")
    String mQDOB;
    AccountRequestModel accountRequestModel;
}

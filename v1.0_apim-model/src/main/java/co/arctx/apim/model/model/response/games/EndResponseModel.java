package co.arctx.apim.model.model.response.games;

import co.arctx.apim.model.model.response.MessageResponseModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

/**
 * @author: Dat Hoang Van
 * June 23, 2022
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EndResponseModel {
    BigDecimal balance;
    BigDecimal bonusBalance;
    Long transactionId;
    String timestamp;
    MessageResponseModel messageResponseModel;
}

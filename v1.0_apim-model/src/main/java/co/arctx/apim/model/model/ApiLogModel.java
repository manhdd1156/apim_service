package co.arctx.apim.model.model;
// Generated Feb 28, 2018 5:41:20 PM by Hibernate Tools 4.3.1

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiLogModel {

    private Long id;
    private String apiUrl;
    private Date createTime;
    private String duration;
    private int responseCode;
}

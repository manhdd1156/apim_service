package co.arctx.apim.model.processor;

import co.arctx.apim.model.entity.QApiLog;
import co.arctx.apim.model.model.*;
import co.arctx.apim.model.model.dto.CreateStandardAggrImageModel;
import co.arctx.apim.model.model.dto.ImageModel;
import co.arctx.apim.model.model.dto.StandardAggrImageModel;
import co.arctx.apim.model.service.AdapterApiClient;
import co.arctx.apim.model.service.HttpService;
import co.arctx.apim.model.service.MediaApiClient;
import co.arctx.apim.model.service.ModelService;
import co.arctx.apim.model.transformer.ModelTransformer;
import co.arctx.apim.model.ultils.ConvertModelUtils;
import co.arctx.apim.model.ultils.HttpClientUtil;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.common.DataType;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.model.RequestModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author: Son Tran
 * Jun 23, 2022
 */
@Component
@Slf4j
public class ModelProcessor extends CommonProcessor<ModelService, QApiLog, ModelTransformer> {
    @Autowired
    private AdapterApiClient adapterApiClient;

    @Autowired
    private HttpService httpService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private MediaApiClient mediaApiClient;

    private Map<String, Object> mapResponse;

    public ModelProcessor(ModelService service, ModelTransformer transformer) {
        super(QApiLog.apiLog, service, transformer);
    }

    @Value("${softSwiss.gameListPath}")
    private String softSwiss_gameListPath;

    @Value("${config.media.standard.baseUrl}")
    private String baseUrlMedia;

    @Value("${config.media.standard.pathMedia}")
    private String pathMedia;

    @Value("${config.media.standard.cachePath}")
    private String cachePathMedia;

    private Map<String, Object> injectData(Map<String, List<String>> paramRequest, Map<String, Object> body) {
        Map<String, Object> map = new HashMap<>();
        paramRequest.forEach((k, v) -> {
            Object val = body.get(k);
            map.put(v.get(0), val);
        });
        return map;
    }

    private Map<String, List<String>> getByDataType(DataType dataType, Map<String, List<String>> paramRequest) {
        Map<String, List<String>> result = new HashMap<>();
        paramRequest.forEach((k, v) -> {
            if (dataType.name().equals(v.get(2))) {
                result.put(k, v);
            }
        });
        return result;
    }

    public Object executeRequest(String endpoint, String token, RequestModel requestModel) throws NotFoundEntityException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        ConfigResponseModel systemConfig = adapterApiClient.getAdapter(new StandardEndpointRequestModel(endpoint, token,requestModel.getBody()));
        if (Objects.isNull(systemConfig)) {
            log.error("not found endpoint : {} in system", endpoint);
            throw NotFoundEntityException.of("Not found endpoint in system");
        }
        Map<String, List<String>> mapPathVariable = getByDataType(DataType.PATH_VARIABLE, systemConfig.getMapParamsRequest());
        Map<String, List<String>> mapBody = getByDataType(DataType.BODY, systemConfig.getMapParamsRequest());
        Map<String, List<String>> mapUriVariable = getByDataType(DataType.URI_VARIABLE, systemConfig.getMapParamsRequest());
        HttpClientUtil.setSecurity(requestModel.getBody(), requestModel.getHeaderParams(), systemConfig.getAggrEndpoint(), systemConfig.getSystemName());

        String aggrEndPoint = HttpClientUtil.builder()
                .input(systemConfig.getAggrEndpoint())
                .handlePathVariable(injectData(mapPathVariable, requestModel.getBody()))
                .handleUriVariable(injectData(mapUriVariable, requestModel.getBody()))
                .handleCompleteUrl(systemConfig.getBaseUrl())
                .getUrl();
        JSONObject jsonObject = ConvertModelUtils.requestBlackBox(mapBody, requestModel.getBody());


        ResponseEntity<String> response = HttpClientUtil.doRequest(aggrEndPoint, systemConfig.getHttpMethod(), requestModel.getHeaderParams(), jsonObject, String.class);

        if (response.getBody().startsWith("[")) {
            List<Map<String, Object>> listMapResponse = (List<Map<String, Object>>) HttpClientUtil.jsonToObj(response.getBody(), List.class);
            List<Map<String, Object>> objectResponseModel = listMapResponse.stream().map(s -> ConvertModelUtils.responseBlackBox(systemConfig.getMapParamResponse(), s)).collect(Collectors.toList());

            return objectResponseModel;
        }
        if (!response.getBody().startsWith("{")) {
            return response.getBody();
        }
        mapResponse = HttpClientUtil.jsonToObj(response.getBody(), Map.class);

        Object objectResponseModel = ConvertModelUtils.responseBlackBox(systemConfig.getMapParamResponse(), (Map<String, Object>) mapResponse);
        return objectResponseModel;
    }

    public Map<String, Object> getGameList(RequestModel request) {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        List<SystemConfigModel> systemConfigs = adapterApiClient.getSystemConfig();
        Map<String, Object> games = new HashMap<String, Object>();
        ObjectMapper mapper = new ObjectMapper();
        for (SystemConfigModel s : systemConfigs) {


            executor.submit(() -> {
            try {
                Object responseModel = new Object();
                if (s.getSystemName().equals("Softswiss")) {
                    ConfigResponseModel systemConfig = adapterApiClient.getAdapter(new StandardEndpointRequestModel("/Games/List", s.getAggToken(),request.getBody()));
                    Map<String, Object> map = new HashMap<>();
                    File file = new File(softSwiss_gameListPath);
                    if (!file.exists())
                        return;
                    String ymlString = new String(Files.readAllBytes(file.toPath()));
                    Yaml yaml = new Yaml();
                    Object gameList = yaml.load(ymlString);
                    map.put("games", gameList);
                    responseModel = ConvertModelUtils.responseBlackBox(systemConfig.getMapParamResponse(), map);

                } else {
                    responseModel = executeRequest("/Games/List", s.getAggToken(), request);

                }
                List<Map<String, Object>> gameListObject = new ArrayList<>();
                if (responseModel instanceof Collection) {
                    gameListObject = (List<Map<String, Object>>) responseModel;
                } else {
                    Map<String, Object> gameList = (Map<String, Object>) responseModel;
                    gameListObject = (List<Map<String, Object>>) gameList.get("games");
                }

                    if (Objects.isNull(gameListObject))
                        return;
                Map<Object, List<Map<String, Object>>> gameResponse = gameListObject.stream().collect(Collectors.groupingBy(gr -> gr.get("vendor")));


                games.put(s.getId(), gameResponse);
            } catch (RuntimeException e) {
                e.printStackTrace();
            } catch (NotFoundEntityException e) {

            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            } catch (InvalidKeyException e) {
                throw new RuntimeException(e);
            }

            });

        }
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        List<SystemConfigGameProviderModel> listAggProviderDb = adapterApiClient.getSystemProviderAvailable();
        Map<String, Object> processGameListAdapter = games;
        List<GameListAdapterModel> gameListFinal = new ArrayList<>();
        processGameListAdapter.forEach((k, v) -> {
            ((Map<String, Object>) v).forEach((k1, v1) ->
                    listAggProviderDb.forEach(x -> {
                        if (k.equals(x.getSystemConfigId()) && k1.equals(x.getProviderName())) {
                            try {
                                saveUrlImageToServer(x, (List<Map<String, Object>>) v1);

                                gameListFinal.add(new GameListAdapterModel(x.getGameListId(), new ObjectMapper().writeValueAsString(v1), x.getId()));
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                        }
                    })
            );
        });

        adapterApiClient.createGameList(gameListFinal);

        return games;
    }

    //  TODO : case with response common param is image_url
    private void saveUrlImageToServer(SystemConfigGameProviderModel infoImage, List<Map<String, Object>> listObjectGame) {

        List<StandardAggrImageModel> images = new LinkedList<>();
        listObjectGame.forEach(objectGame -> {
            String imageFullPath = "";
            if (objectGame.get("imageFullPath") != null) {
                imageFullPath = String.valueOf(objectGame.get("imageFullPath"));
            }
            StandardAggrImageModel model = StandardAggrImageModel.builder()
                    .aggrUrl(imageFullPath)
                    .systemProviderId(String.valueOf(infoImage.getId()))
                    .systemId(infoImage.getSystemConfigId())
                    .providerId(infoImage.getGameProviderId())
                    .providerName(infoImage.getProviderName())
                    .gameCode(String.valueOf(objectGame.get("gameCode")))
                    .build();
            images.add(model);
        });
        List<ImageModel> imageModels = mediaApiClient.uploadMultipleUrl(new CreateStandardAggrImageModel(images));
        AtomicInteger idx = new AtomicInteger();
        listObjectGame.forEach(objectGame -> {
            String imageFullPath = imageModels.get(idx.get()).getUrl();
            String shortImageUrl = imageFullPath;
            if(imageFullPath != null && imageFullPath.contains(pathMedia)) {
                shortImageUrl = cachePathMedia + imageFullPath.substring(baseUrlMedia.length());
            }
            if (objectGame.get("imageFullPath") != null) {
                objectGame.replace("imageFullPath", imageFullPath);
                objectGame.replace("imageURL", shortImageUrl);
            } else {
                Map<String, Object> objectGameTemp = objectGame;
                if (!Objects.isNull(imageModels.get(idx.get())) && ObjectUtils.isEmpty(imageModels.get(idx.get()))) {
                    objectGameTemp.put("imageFullPath", imageFullPath);

                    objectGameTemp.put("imageURL", shortImageUrl);
                } else {
                    objectGameTemp.put("imageFullPath", null);
                    objectGameTemp.put("imageURL", null);
                }
                listObjectGame.set(idx.get(), objectGameTemp);
            }
            idx.getAndIncrement();
        });
    }

}

package co.arctx.apim.model.model.request.notifications;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortugueseRequestModel {
    String token;
    String gameCategory;
    String roundId;
    String smResult;
    BlackJackRequestModel blackJackRequestModel;
    RouletteRequestModel roulette;
    BaccaratRequestModel baccaratRequestModel;
    BancaFrancesaRequestModel bancaFrancesaRequestModel;
    List<PortugueseRoundDetailRequestModel> portugueseRoundDetailList;
    AccountRequestModel accountRequestModel;
}

package co.arctx.apim.model.model.request.token;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RetrieveRequestModel {
    //Pr
    String token;
    String playerId;
    String gameCode;
    String financialMode;
    AccountRequestModel accountRequestModel;

    //Qt
    String grantType;
    String responseType;
    String username;
    String password;
}

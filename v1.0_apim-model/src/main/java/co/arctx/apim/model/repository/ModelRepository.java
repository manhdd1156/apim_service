package co.arctx.apim.model.repository;

import co.arctx.core.common.CommonRepository;
import co.arctx.apim.model.entity.ApiLog;

public interface ModelRepository extends CommonRepository<ApiLog, String> {

}

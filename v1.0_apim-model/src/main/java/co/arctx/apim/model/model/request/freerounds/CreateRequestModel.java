package co.arctx.apim.model.model.request.freerounds;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateRequestModel {
    //Common
    String playerId;

    //Pr
    String gameCode;
    String bonusId;
    String numberFreeRounds;
    AccountRequestModel accountRequestModel;

    //Qt
    String authorization;
    String txnId;
    String gameId;
    String totalBetValue;
    String roundOptions;
    String currency;
    String promoCode;
    String validityDays;
    String rejectTable;
}

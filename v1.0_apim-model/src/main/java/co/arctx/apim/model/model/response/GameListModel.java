package co.arctx.apim.model.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author: Si Dang
 * Jul 20, 2022
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GameListModel {
    private String name;
    private String gameCode;
    private String provider;
    private List<Map<String, String>> languages;
    private List<String> currencies;
    private List<String> clientTypes;
    private List<String> supportedDevices;
    private List<String> freeSpinDevices;
    private boolean supportsDemo;
    private String description;
    private String category;
    private String payout;
    private boolean hasFreeSpins;


}

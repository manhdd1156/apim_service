package co.arctx.apim.model.model.request.games;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 29, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisplaysReqestModel {
    String balance;
    String name;
    String language;
    String gameHistory;
}

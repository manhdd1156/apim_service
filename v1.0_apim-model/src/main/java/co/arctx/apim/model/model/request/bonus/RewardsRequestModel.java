package co.arctx.apim.model.model.request.bonus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RewardsRequestModel {
    String passKey;
    String contentType;
    String rewardType;
    String rewardTitle;
    String txnId;
    String playerId;
    String amount;
    String currency;
    String created;

}

package co.arctx.apim.model.service;

import co.arctx.apim.model.model.*;
import co.arctx.core.config.FeignConfig;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * @author: Son Tran
 * Jun 23, 2022
 */

@FeignClient( name = "apim-adapter", configuration = FeignConfig.class)

public interface AdapterApiClient {
    @PostMapping("/api/v1/adapters/get-config")
    ConfigResponseModel getAdapter(@RequestBody StandardEndpointRequestModel requestModel) throws NotFoundEntityException;

    @GetMapping("/api/v1/adapters/get-game-provider/{status}")
    List<String> getGameProvider(@PathVariable("status")Integer status);

    @GetMapping("/api/v1/adapters/get-system-config")
    List<SystemConfigModel> getSystemConfig();

    @GetMapping("/internal/api/v1/adapters/get-system-provider-available")
    List<SystemConfigGameProviderModel> getSystemProviderAvailable();

    @PostMapping("/internal/api/v1/adapters/create-game-list")
    Object createGameList(@RequestBody List<GameListAdapterModel> listGame);

    @GetMapping("/internal/api/v1/adapters/get-game-list")
    Object getGameList();
}

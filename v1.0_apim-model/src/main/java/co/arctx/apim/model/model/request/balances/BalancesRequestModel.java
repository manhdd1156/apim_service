package co.arctx.apim.model.model.request.balances;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BalancesRequestModel {

    //Common

    //Pr
    String token;
    String playerId;
    AccountRequestModel accountRequestModel;

    //Qt
    String walletSession;
    String passKey;
    String gameId;

    //Ss
    String seq;
    String callerId;
    String callerPassword;
    String uuid;
    String omegaSessionKey;
    String currency;
    String gameInfoId;
}

package co.arctx.apim.model.service;

import co.arctx.apim.model.model.dto.CreateStandardAggrImageModel;
import co.arctx.apim.model.model.dto.ImageModel;
import co.arctx.apim.model.model.dto.UploadUrlModel;
import co.arctx.core.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name = "apim-media", configuration = FeignConfig.class)
@RequestMapping("/api/v1")
public interface MediaApiClient {

    @PostMapping("/images/upload-url")
    ImageModel uploadUrl(@RequestBody UploadUrlModel model);

    @PostMapping("/standard-aggr-images/upload-url-multiple")
    List<ImageModel> uploadMultipleUrl(@RequestBody CreateStandardAggrImageModel models);

}

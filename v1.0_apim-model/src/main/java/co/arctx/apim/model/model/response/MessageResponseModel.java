package co.arctx.apim.model.model.response;

import co.arctx.apim.model.model.response.enumerate.DisplayType;
import co.arctx.apim.model.model.response.enumerate.MessageType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * @author: Dat Hoang Van
 * June 23, 2022
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MessageResponseModel {
    DisplayType displayType;
    MessageType messageType;
    MessageContentResponseModel messageContentResponseModel;
}

package co.arctx.apim.model.model.request.jackpots;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JackpotRequestModel {
    int jackpotRequestType;
    String currencyCode;
    String jackpotId;
    String vendors;
    AccountRequestModel accountRequestModel;
    String authorization;
    String type;
}

package co.arctx.apim.model.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Map;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StandardEndpointRequestModel {
    String endpoint;
    String token;
    Map<String,Object> body;
}

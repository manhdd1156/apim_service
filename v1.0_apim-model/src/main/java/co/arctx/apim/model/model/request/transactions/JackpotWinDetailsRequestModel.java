package co.arctx.apim.model.model.request.transactions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 22, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JackpotWinDetailsRequestModel {
    String jackpotId;
    String jackpotName;
    String amount;
    String testJackpot;
    String currencyCode;
}

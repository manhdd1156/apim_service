package co.arctx.apim.model.model.request.bets;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResettlementRequestModel {
    String token;
    String betTransactionID;
    String gameCode;
    String roundId;
    String playerId;
    String timestamp;
    AccountRequestModel accountRequestModel;
}

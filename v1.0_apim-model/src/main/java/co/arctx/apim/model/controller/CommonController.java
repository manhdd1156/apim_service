package co.arctx.apim.model.controller;

import co.arctx.apim.model.model.ConfigResponseModel;
import co.arctx.apim.model.model.StandardEndpointRequestModel;
import co.arctx.apim.model.model.request.games.LaunchRequestModel;
import co.arctx.apim.model.model.response.GameListModel;
import co.arctx.apim.model.processor.ModelProcessor;
import co.arctx.apim.model.service.AdapterApiClient;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.model.RequestModel;
import co.arctx.core.model.ResponseModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.configuration.SystemConfiguration;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.ws.Response;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

/**
 * @author: Son Tran
 * Jul 15, 2022
 */
@RestController
@RequestMapping("/**")
public class CommonController {
    @Autowired
    private ModelProcessor modelProcessor;
    @Autowired
    private AdapterApiClient adapterApiClient;


    @PostMapping("/**")

    public ResponseEntity<?> forwardAllEndpoint(HttpServletRequest request,@RequestBody RequestModel requestModel) throws NotFoundEntityException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        String token = String.valueOf(requestModel.getHeaderParams().get("token"));
        String endpoint = StringEscapeUtils.escapeHtml4(request.getRequestURI());
        Object response = modelProcessor.executeRequest(endpoint,token,requestModel);
        return ResponseEntity.ok(response);
    }
    @PostMapping("/update_gameList")
    public ResponseEntity<?> updateGameList(@RequestBody RequestModel requestModel)
    {

        Map<String,Object> response = modelProcessor.getGameList(requestModel);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/gameList")
    public ResponseEntity<?> getGameList()
    {
       return ResponseEntity.ok(adapterApiClient.getGameList());
    }



}

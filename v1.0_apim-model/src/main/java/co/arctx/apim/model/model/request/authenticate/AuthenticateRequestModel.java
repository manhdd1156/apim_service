package co.arctx.apim.model.model.request.authenticate;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticateRequestModel {
    //Common
    String playerId;

    //Pr
    String token;
    String platformType;
    AccountRequestModel accountRequestModel;

    //Qt
    String walletSession;
    String passKey;
    String gameId;
}

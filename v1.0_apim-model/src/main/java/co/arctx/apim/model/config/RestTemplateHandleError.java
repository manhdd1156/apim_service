package co.arctx.apim.model.config;

import co.arctx.apim.model.model.ErrorCommonModel;
import co.arctx.core.exception.RestTemplateException;
import co.arctx.core.model.ErrorDetail;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Scanner;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

/**
 * @author: Si Dang
 * Jul 01, 2022
 */
@Component
public class RestTemplateHandleError extends DefaultResponseErrorHandler {


    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {

        boolean checked = clientHttpResponse.getStatusCode().series() == CLIENT_ERROR
                || clientHttpResponse.getStatusCode().series() == SERVER_ERROR;
        return (checked);
    }


    @SneakyThrows
    @Override
    public void handleError(ClientHttpResponse clientHttpResponse){

            throw new RestTemplateException(toString(clientHttpResponse.getBody()),clientHttpResponse.getRawStatusCode());


    }
    String toString(InputStream inputStream) {
        Scanner s = new Scanner(inputStream).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}

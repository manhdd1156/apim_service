package co.arctx.apim.model.service;


import co.arctx.apim.model.entity.ApiLog;
import co.arctx.apim.model.repository.ModelRepository;
import co.arctx.core.model.RequestModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class ModelService {

    @Autowired
    private ModelRepository repo;

    public void putLog(String apiUrl, long start, int responseCode) {
        long duration = System.currentTimeMillis() - start;
        try {

            String durationStr = String.valueOf(duration);

            ApiLog apilog = new ApiLog();

            apilog.setApiUrl(apiUrl);
            apilog.setDuration(durationStr);
            apilog.setResponseCode(responseCode);
            apilog.setCreateTime(new Date());
            repo.save(apilog);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public <T> T breakModel(RequestModel requestModel, Class<T> modelClass) {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.convertValue(requestModel.getBody(), modelClass);
    }

}

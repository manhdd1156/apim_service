package co.arctx.apim.model.model.request.transactions;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * @author: Si Dang
 * Jun 22, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DebitAndCreditRequestModel {
    String token;
    String gameCode;
    String playerId;
    String roundId;
    String transactionId;
    String amount;
    String endGame;
    String feature;
    String featureId;
    String featureAmount;
    String transactionConfiguration;
    String ticketAmount;
    String jackpotContribution;
    List<JackpotWinDetailsRequestModel> jackpotWinDetailsRequestModels;
    TaxesRequestModel taxesRequestModel;
    AccountRequestModel accountRequestModel;
    String roomId;
    String isBingo;

}

package co.arctx.apim.model.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author: Nam Vu
 * Jun 21, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SystemConfigModel {
    String id;
    @NotNull
    String systemName;
    @NotNull
    String providerType;
    @NotNull
    String aggToken;
    @NotNull
    String authValue;
    @NotNull
    String baseUrl;
    String createdBy;
    LocalDateTime createdDate;
    String updatedBy;
    LocalDateTime updatedDate;
    @NotNull
    @Range(min = 0, max = 2)
    String status;
    @NotNull
    String description;
}

package co.arctx.apim.model.model.request.notifications;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FreeSpinsRequestModel {
    String tournamentId;
    String playerId;
    String gameCode;
    String numberFreeRounds;
    String expirationDate;
    String countryCode;
    String playerRegulation;
    String currencyCode;
    String betLevel;
    String coinValue;
    String lineNumber;
    String usePoints;
    String affiliateCode;
    AccountRequestModel accountRequestModel;
    String branId;
    String token;
}

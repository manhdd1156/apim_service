package co.arctx.apim.model.model.request.transactions;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReconCommitRequestModel {
    AccountRequestModel accountRequestModel;
    String transactionId;
    String balance;
    String refTransactionId;
    String refTransactionDate;
    String extraData;
    String brandId;
}

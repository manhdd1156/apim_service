package co.arctx.apim.model.model.request.transactions;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * @author: Si Dang
 * Jun 22, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditRequestModel {
    //Common
    String playerId;
    String roundId;
    String amount;
    String currency;

    //Pr
    String token;
    String gameCode;
    String transactionId;
    String creditType;
    String endGame;
    String feature;
    String featureId;
    int[] transactionConfiguration;
    List<JackpotWinDetailsRequestModel> jackpotWinDetailsRequestModels;
    String roomId;
    String isBingo;
    AccountRequestModel accountRequestModel;

    //Qt
    String walletSession;
    String passKey;
    String txnType;
    String txnId;
    String bonusType;
    String bonusPromoCde;
    String jpPayout;
    String gameId;
    String device;
    String clientType;
    String clientRoundId;
    String category;
    String created;
    String completed;
    String tableId;

    //Ss
    String seq;
    String callerId;
    String callerPassword;
    String uuid;
    String omegaSessionKey;
    String tranType;
    String timestamp;
    String providerTranId;
    String platformCode;
    String gameTranId;
    String gameInfoId;
    String isFinal;

}

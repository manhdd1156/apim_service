package co.arctx.apim.model.model.request.games;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 21, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryRequestModel {
    //Common
    String playerId;

    //Pr
    String gameCode;
    String roundId;
    String countryCode;
    String currencyCode;
    String affiliateCode;
    String brandId;

    //Qt
    String contentType;
    String authorization;
    String currency;
    String country;
    String gender;
    String dateOfBirth;
    String languageCode;
    String timeZone;

    //Ss

}

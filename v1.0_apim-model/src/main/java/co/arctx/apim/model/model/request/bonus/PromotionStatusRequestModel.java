package co.arctx.apim.model.model.request.bonus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * @author: Si Dang
 * Jun 23, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionStatusRequestModel {
    String passKey;
    String bonusId;
    String playerId;
    List<String> gameIds;
    String totalBetValue;
    String totalPayout;
    List<String> roundOptions;
    String currency;
    String promoCode;
    String status;
    String validityDays;
    String promotedDateTime;
    String claimedDateTime;
    String failedDateTime;
    String completedDateTime;
    String cancelledDateTime;
    String deletedDateTime;
    String expiredDateTime;
    String claimedRoundOption;
    String claimedGameId;
}

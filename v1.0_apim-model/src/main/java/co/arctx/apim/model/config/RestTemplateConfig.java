package co.arctx.apim.model.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author: Si Dang
 * Jul 01, 2022
 */
@Configuration
public class RestTemplateConfig {
    @Bean
    RestTemplate restTemplate()
    {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new RestTemplateHandleError());
        return restTemplate;
    }
}

package co.arctx.apim.model.model.request.transactions;

import co.arctx.apim.model.model.request.AccountRequestModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author: Si Dang
 * Jun 22, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RollBackRequestModel {
    //Common

    String playerId;
    String roundId;
    String amount;
    //Pr
    String token;
    String refTransactionId;
    String gameCode;
    String cancelEntireRound;
    String transactionId;
    String reason;
    String roomId;
    String isBingo;
    AccountRequestModel accountRequestModel;

    //Qt
    String walletSession;
    String passKey;
    String betId;
    String txnId;
    String currency;
    String gameId;
    String device;
    String clientType;
    String clientRoundId;
    String category;
    String created;
    String completed;
    String tableId;

    //Ss
    String seq;
    String callerId;
    String callerPassword;
    String omegaSessionKey;
    String tranType;
    String timestamp;
    String providerTranId;
    String platformCode;
    String gameTranId;
    String gameInfoId;
}

package co.arctx.apim.model.common;

public enum ErrorCode {
    INVALID_REQUEST("001"),
    INVALID_CREDENTIAL("002"),
    UNKNOWN_ERROR("003"),
    LOGIN_FAILD("004");



    private final String code;

    ErrorCode(String code) {
        this.code = code;
    }
}

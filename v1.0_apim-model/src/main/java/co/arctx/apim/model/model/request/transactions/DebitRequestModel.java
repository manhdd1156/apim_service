package co.arctx.apim.model.model.request.transactions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * @author: Si Dang
 * Jun 22, 2022
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DebitRequestModel {

    //common
    String playerId;
    String roundId;
    String currency;
    String amount;
    String gameId;

    String token;
    String transactionId;
    String debitAmount;
    String creditAmount;
    String creditType;
    String endGame;
    String feature;
    String featureId;
    String ticketAmount;
    String roomId;
    String isBingo;
    String jackpotContribution;
    String transactionConfiguration;
    List<JackpotWinDetailsRequestModel> jackpotWinDetailsRequestModel;
    TaxesRequestModel taxesRequestModel;

    String walletSession;
    String passKey;
    String txnType;
    String txnId;
    String bonusBetAmount;
    String bonusType;
    String bonusPromoCde;
    String jpContribution;
    String device;
    String clientType;
    String clientRoundId;
    String category;
    String created;
    String completed;
    String tableId;

    String seq;
    String callerId;
    String callerPassword;
    String omegaSessionKey;
    String tranType;
    String timestamp;
    String providerTranId;
    String platformCode;
    String gameTranId;
    String gameInfoId;
    String isFinal;
}

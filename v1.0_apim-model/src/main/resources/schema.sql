create schema `apim_model_db` default character set utf8 collate utf8_unicode_ci;

use
apim_model_db;

create table api_log
(
    id            int(10) not null,
    api_host      varchar(256) null,
    sys_name      varchar(256) null,
    api_func      varchar(256) null,
    api_endpoint  varchar(256) null,
    create_time   timestamp null,
    duration      int(100) null,
    response_code int(10) null,
    constraint api_log_id_uindex
        unique (id)
);

alter table api_log
    add primary key (id);



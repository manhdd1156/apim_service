# Change log

### Handle http request case endpoint 20 (Notifications) and 23 (Balance)
    - modified: v1.0_apim-model/src/main/java/co/arctx/apim/model/model/request/* (add annotation @JsonIgnoreProperties(ignoreUnknown = true))
    - modified: v1.0_apim-model/src/main/java/co/arctx/apim/model/processor/ModelProcessor.java (add function hande case 23)
    - new file: v1.0_apim-model/src/main/java/co/arctx/apim/model/ultils/HttpClientUtil.java

### Refactor code http client request (2022-07-12 16:00:00)
    - modified: v1.0_apim-model/src/main/java/co/arctx/apim/model/processor/ModelProcessor.java
    - modified: v1.0_apim-model/src/main/java/co/arctx/apim/model/ultils/HttpClientUtil.java
    - new file: v1.0_apim-core/src/main/java/co/arctx/core/common/DataType.java => Add new column work is enum data

## [tag 1.0.1]
### Refactor code controller use executeRequest instead of requestFactory in Processor package
    - modified: all controller file.
    - modified : all test file
## [tag 1.0.2]
### Refactor Dynamic request param

    - deleted:    v1.0_apim-model/src/main/java/co/arctx/apim/model/common/GameList.java
    - deleted:    v1.0_apim-model/src/main/java/co/arctx/apim/model/model/GameModel.java
    - modified:   v1.0_apim-model/CHANGE-LOG.md
    - modified:   v1.0_apim-model/src/main/java/co/arctx/apim/model/processor/ModelProcessor.java
    
### Refactor Add image function into update list game function

    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\controller\CommonController.java
    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\processor\ModelProcessor.java
    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\service\AdapterApiClient.java

    - new file:   v1.0_apim-model\src\main\java\co\arctx\apim\model\model\GameListAdapterModel.java
    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\model\SystemConfigGameProviderModel.java
    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\model\dto\CreateStandardAggrImageModel.java
    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\model\dto\ImageModel.java
    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\model\dto\StandardAggrImageModel.java
    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\model\dto\UploadUrlModel.java
    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\model\dto\UrlMappingDTO.java
    - modified:   v1.0_apim-model\src\main\java\co\arctx\apim\model\service\MediaApiClient.java
### ReFactor update game list and 
- modified: v1.0_apim-model/src/main/resources/application.properties
- modified: v1.0_apim-model/pom.xml
- modified: v1.0_apim-model/src/main/java/co/arctx/apim/model/ultils/HttpClientUtil.java
- modified: v1.0_apim-model/src/main/java/co/arctx/apim/model/processor/ModelProcessor.java
- modified: v1.0_apim-model/src/main/java/co/arctx/apim/model/model/StandardEndpointRequestModel.java
- modified: v1.0_apim-model/src/main/java/co/arctx/apim/model/model/ConfigResponseModel.java
- modified: v1.0_apim-model/src/main/java/co/arctx/apim/model/controller/CommonController.java
- modified: v1.0_apim-media/src/main/resources/application.properties
- modified: v1.0_apim-media/src/main/java/co/arctx/apim/media/transformer/ImageTransformer.java
- modified: v1.0_apim-media/src/main/java/co/arctx/apim/media/processor/StandardAggrImageProcessor.java
- modified: v1.0_apim-core/src/main/java/co/arctx/core/utils/DynamicLinkUtils.java
- modified: v1.0_apim-core/src/main/java/co/arctx/core/exception/RestTemplateException.java
- modified: v1.0_apim-core/src/main/java/co/arctx/core/common/CommonExceptionHandlerAdvice.java
- modified: v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/repository/impl/RepositoryCustomImpl.java
- modified: v1.0_apim-adapter/src/main/java/co/arctx/apim/adapter/model/ConfigResponseModel.java
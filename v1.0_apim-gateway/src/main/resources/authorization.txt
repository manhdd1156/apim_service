# COMMENT
# Domain: ARCTX.CO
# Parent Group: arctx-platform
# Admin: arctx-admin/authorization
# Agent: arctx-agent/authorization

# FORMAT: API|METHOD|ROLES

# MODEL-SERVICE
/model-service/api/v1/model|GET|arctx-admin/authorization,arctx-agent/authorization
#AUTH-SERVICE
/auth-service/test|GET|ADMIN,USER
#ADAPTER-SERVICE
/adapter-service/api/v1/adapters/create-system-configs|POST|ADMIN

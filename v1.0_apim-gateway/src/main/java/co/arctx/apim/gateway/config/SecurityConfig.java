package co.arctx.apim.gateway.config;

import co.arctx.apim.gateway.filter.JwtExceptionHandle;
import co.arctx.apim.gateway.filter.JwtRequestFilter;
import co.arctx.apim.gateway.utils.AuthorizationLoader;
import co.arctx.apim.gateway.utils.AuthorizationMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthorizationLoader authorizationLoader;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);


        http.addFilterBefore(new JwtRequestFilter(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(new JwtExceptionHandle(), JwtRequestFilter.class);

        List<AuthorizationMatcher> authorizationMatchers = authorizationLoader.getAuthorizationMatchers();
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, POST_PERMIT).permitAll()
                .antMatchers(HttpMethod.GET, GET_PERMIT).permitAll()
                .antMatchers(HttpMethod.PUT, PUT_PERMIT).permitAll()
                .antMatchers(HttpMethod.DELETE, DELETE_PERMIT).permitAll();
        for (AuthorizationMatcher matcher : authorizationMatchers) {
            if (matcher == null) {
                continue;
            }
            http.authorizeRequests()
                    .antMatchers(matcher.getMethod(), matcher.getApiUrl()).hasAnyRole(matcher.getRoles());
        }

    }

    private final String[] POST_PERMIT = {

    };

    private final String[] GET_PERMIT = {


    };

    private final String[] PUT_PERMIT = {

    };

    private final String[] DELETE_PERMIT = {

    };


    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/model-service/**","/auth-service/authentication/**");

    }




    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}

package co.arctx.apim.gateway.utils;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpMethod;

@Getter
@Setter
@AllArgsConstructor
public class AuthorizationMatcher {

    private String apiUrl;
    private HttpMethod method;
    private String[] roles;
}

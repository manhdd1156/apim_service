package co.arctx.apim.gateway.filter;

import co.arctx.core.model.ErrorDetail;
import com.google.gson.Gson;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;


@Slf4j
public class JwtExceptionHandle extends OncePerRequestFilter {
    private Gson gson;
    public JwtExceptionHandle()
    {
        gson = new Gson();
    }


    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);

        } catch (IllegalArgumentException e) {
            log.error("Unable to get JWT Token");
            setErrorResponse(HttpStatus.UNAUTHORIZED, response, e, "Unable to get JWT Token");
            e.printStackTrace();
        } catch (MalformedJwtException e) {
            log.error("jwt token is invalid");
            setErrorResponse(HttpStatus.UNAUTHORIZED, response, e, "jwt token is invalid");
            e.printStackTrace();
        } catch (ExpiredJwtException e) {
            log.error("jwt token is expired");
            setErrorResponse(HttpStatus.UNAUTHORIZED, response, e, "jwt token is expired");
            e.printStackTrace();
        } catch (SignatureException e) {
            log.error("jwt token signature is incorrect");
            setErrorResponse(HttpStatus.UNAUTHORIZED, response, e, "jwt token signature is incorrect");
            e.printStackTrace();
        } catch (AuthenticationException e) {
            log.error("jwt token is invalid");
            setErrorResponse(HttpStatus.UNAUTHORIZED, response, e, "jwt token is invalid");
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
            setErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, response, e, null);
        }
    }

    public void setErrorResponse(HttpStatus status, HttpServletResponse response, Throwable ex, String message) {
        response.setStatus(status.value());
        response.setContentType("application/json");
        message = Objects.isNull(message) ? ex.getMessage() : message;
        ErrorDetail errorDetail = ErrorDetail.create("", message);
        try {
            String json = gson.toJson(errorDetail);
            response.getWriter().write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

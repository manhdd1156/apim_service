package co.arctx.apim.gateway.utils;


import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
@Slf4j
public class AuthorizationLoader {

    @Getter
    private List<AuthorizationMatcher> authorizationMatchers;

    public AuthorizationLoader() {
        try (
                InputStream is = getClass().getResourceAsStream("/authorization.txt");
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
        ) {
            authorizationMatchers = br.lines()
                    .filter(validLine)
                    .map(convertLineToAuthorizationMatcher)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            authorizationMatchers = new ArrayList<>(0);
        }
        log.info("\n\n -> Load authorization successful! <-\n\n");
    }

    private Predicate<String> validLine = l -> {
        if (Objects.isNull(l)) {
            return false;
        }
        String line = l.trim();
        return !line.startsWith("#") && !line.isEmpty();
    };

    private Function<String, AuthorizationMatcher> convertLineToAuthorizationMatcher = line -> {
        if (Objects.isNull(line)) {
            return null;
        }
        String[] lines = line.split("\\|");

        if (lines.length != 3) {
            log.warn("Line: `" + line + "` is invalid. Skip!!");
            return null;
        }

        String apiUrl = lines[0];
        if (apiUrl.length() < 1) {
            log.warn("Line: `" + line + "` is invalid. Can not detect ApiUrl. Skip!!");
            return null;
        }

        HttpMethod method = HttpMethod.resolve(lines[1]);
        if (Objects.isNull(method)) {
            log.warn("Line: `" + line + "` is invalid. Can not detect Method. Skip!!");
            return null;
        }

        String[] roles = lines[2].split(",");
        if (roles.length < 1) {
            log.warn("Line: `" + line + "` is invalid. Can not detect Roles. Skip!!");
            return null;
        }

        return new AuthorizationMatcher(apiUrl, method, roles);
    };
}

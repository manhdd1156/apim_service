CREATE database apimp_media;

USE apimp_media;

CREATE TABLE IF NOT EXISTS `folder` (
                                        `id` BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                                        `name` VARCHAR(250) NOT NULL,
    `caption` VARCHAR(250) NOT NULL,
    `parent_id` BIGINT UNSIGNED,
    `visible` BIT DEFAULT b'0',
    `created_time` DATETIME,
    `modified_time` TIMESTAMP,
    KEY `folder_name_idx` (`name`),
    KEY `folder_created_idx` (`created_time`),
    CONSTRAINT `folder_parentId_frk` FOREIGN KEY (`parent_id`)
    REFERENCES `folder` (`id`) ON UPDATE CASCADE
    ) ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `file` (
                                      `id` BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                                      `folder_id` BIGINT UNSIGNED,
                                      `name` VARCHAR(250) NOT NULL,
    `caption` VARCHAR(250) NOT NULL,
    `uploader` VARCHAR(50) NOT NULL,
    `path` VARCHAR(250) NOT NULL,
    `content_type` VARCHAR(50) NOT NULL,
    `iod` VARCHAR(50) NOT NULL,
    `original_name` VARCHAR(250) NOT NULL,
    `extension` CHAR(10) NOT NULL,
    `size` BIGINT UNSIGNED,
    `created_time` DATETIME,
    `modified_time` TIMESTAMP,
    KEY `file_name_idx` (`name`),
    KEY `file_iod_idx` (`iod`),
    CONSTRAINT `file_folderId_frk` FOREIGN KEY (`folder_id`)
    REFERENCES `folder` (`id`) ON UPDATE CASCADE
    ) ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `thumbnail_type` (
                                                `id` BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                                                `name` VARCHAR(250) NOT NULL,
    `path` VARCHAR(250) NOT NULL,
    `width` INT UNSIGNED,
    `height` INT UNSIGNED,
    `created_time` DATETIME,
    `modified_time` TIMESTAMP,
    KEY `thumbnail_name_idx` (`name`)
    ) ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `thumbnail` (
                                           `id` BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                                           `file_id` BIGINT UNSIGNED,
                                           `type_id` BIGINT UNSIGNED,#
                                           `width` INT UNSIGNED,
                                           `height` INT UNSIGNED,
                                           `extension` CHAR(10) NOT NULL,
    `path` VARCHAR(250) NOT NULL,
    `iod` VARCHAR(50) NOT NULL,
    `content_type` VARCHAR(50) NOT NULL,
    `size` BIGINT UNSIGNED,
    `created_time` DATETIME,
    `modified_time` TIMESTAMP,
    KEY `thumbnail_fileId_idx` (`file_id`),
    KEY `thumbnail_typeId_idx` (`type_id`),
    KEY `thumbnail_iod_idx` (`iod`),
    CONSTRAINT `thumbnail_fileId_frk` FOREIGN KEY (`file_id`)
    REFERENCES `file` (`id`) ON UPDATE CASCADE,
    CONSTRAINT `thumbnail_typeId_frk` FOREIGN KEY (`type_id`)
    REFERENCES `thumbnail_type` (`id`) ON UPDATE CASCADE
    ) ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `image` (
                                       `id` BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                                       `name` VARCHAR(128) NOT NULL,
    `caption` VARCHAR(256),
    `uploader` VARCHAR(64) NOT NULL,
    `path` VARCHAR(256) NOT NULL,
    `content_type` VARCHAR(32) NOT NULL,
    `iod` VARCHAR(64) NOT NULL,
    `original_name` VARCHAR(128) NOT NULL,
    `extension` CHAR(10) NOT NULL,
    `size` BIGINT UNSIGNED,
    `created_time` DATETIME NOT NULL,
    `modified_time` TIMESTAMP,
    KEY `image_name_idx` (`name`),
    KEY `image_iod_idx` (`iod`)
    ) ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_general_ci;
package co.arctx.apim.media.controller;

import co.arctx.apim.media.model.FileModel;
import co.arctx.apim.media.processor.FileUploadProcessor;
import co.arctx.core.exception.AccessDeniedException;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.exception.UnexpectedException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@RequestMapping("/api/v1/{folderId}/files")
@RestController
@RequiredArgsConstructor
public class FileUploadApi {

    private final FileUploadProcessor processor;

    @PostMapping
    @ApiOperation(value = "Upload a file to Folder, return FileModel.", response = FileModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Upload a File successfully"),
            @ApiResponse(code = 404, message = "Not found folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FileModel uploadFile(@PathVariable Long folderId, @RequestParam("file") MultipartFile file, HttpServletRequest httpServletRequest) throws NotFoundEntityException, UnexpectedException {
        return processor.upload(folderId, "uploader",true, file);
    }

    @PostMapping("/multiple")
    @ApiOperation(value = "Upload multiple file to Folder, return List<FileModel>.", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Upload multiple File successfully"),
            @ApiResponse(code = 404, message = "Not found folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public List<FileModel> uploadMultipleFiles(@PathVariable Long folderId, @RequestParam("file") MultipartFile[] files, HttpServletRequest httpServletRequest) {
        return Arrays.asList(files).stream()
                .map(file -> {
                    try {
                        return uploadFile(folderId, file, httpServletRequest);
                    } catch (Exception e) {
                        return null;
                    }
                })
                .collect(Collectors.toList());
    }

    @PostMapping("/none-resize")
    @ApiOperation(value = "Upload a file to Folder, return FileModel.", response = FileModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Upload a File successfully"),
            @ApiResponse(code = 404, message = "Not found folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FileModel uploadFileNoneResize(@PathVariable Long folderId, @RequestParam("file") MultipartFile file, HttpServletRequest httpServletRequest) throws AccessDeniedException, UnexpectedException, NotFoundEntityException {
        return processor.upload(folderId, "uploader", false, file);
    }

    @PostMapping("/multiple/none-resize")
    @ApiOperation(value = "Upload multiple file to Folder, return List<FileModel>.", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Upload multiple File successfully"),
            @ApiResponse(code = 404, message = "Not found folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public List<FileModel> uploadMultipleFilesNoneResize(@PathVariable Long folderId, @RequestParam("file") MultipartFile[] files, HttpServletRequest httpServletRequest) {
        return Arrays.asList(files).stream()
                .map(file -> {
                    try {
                        return uploadFileNoneResize(folderId, file, httpServletRequest);
                    } catch (Exception e) {
                        return null;
                    }
                })
                .collect(Collectors.toList());
    }
}


package co.arctx.apim.media.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageModel {

    private Long id; //use for response

    @NotNull
    @Size(min=1, max = 64)
    private String uploader; // required

    @NotNull
    @Size(min=1, max = 128)
    private String name; // required

    @NotEmpty()
    @Size(min=1, max = 256)
    private String caption; // required

    private String path; //use for response
    private String iod; //use for response
    private String extension; //use for response
    private String contentType; //use for response
    private Long created;
    @JsonIgnore
    private MultipartFile multipartFile; //use for upload
    private Long size; // use for response - unit: byte

    private String url; // use for url
}

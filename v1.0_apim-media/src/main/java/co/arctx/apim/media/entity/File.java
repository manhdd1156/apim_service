package co.arctx.apim.media.entity;


import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Entity
@Table
@Getter
@Setter
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long folderId;
    private String name;
    private String caption;
    private String uploader;
    private String path;
    private String contentType;
    private String originalName;
    private String extension;
    private Date createdTime;

    /**
     * id on disk
     */
    private String iod;
    private Long size;

    private @Transient MultipartFile multipartFile;
    private @Transient Folder folder;
    private @Transient List<Thumbnail> thumbnails;

    public boolean isImage() {
        return getContentType() != null && getContentType().split("/")[0].equals("image");
    }

}

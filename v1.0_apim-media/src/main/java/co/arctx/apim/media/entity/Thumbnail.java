package co.arctx.apim.media.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Entity
@Table
@Getter
@Setter
public class Thumbnail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long fileId;
    private Long typeId;
    private @Transient String filePath;
    private String extension;
    private String contentType;
    private String path;
    private String iod;
    private Integer width;
    private Integer height;
    private Long size/*, dimension*/;
    private Date createdTime;

    public String createOutFilepath() {
        return String.join("/", getPath(), getIod());
    }

}

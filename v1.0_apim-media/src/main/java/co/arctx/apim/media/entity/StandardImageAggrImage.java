package co.arctx.apim.media.entity;

import co.arctx.core.utils.HashUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "standard_image_aggr_image")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StandardImageAggrImage {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "standard_url")
    private String standardUrl;

    @Column(name = "aggr_url")
    private String aggrUrl;

//    @Column(name = "standard_endpoint_id")
//    private String standardEndpointId;
//
//    @Column(name = "aggr_endpoint_id")
//    private String aggrEndpointId;
//
//    @Column(name = "standard_param_response_id")
//    private String standardParamResId;
//
//    @Column(name = "aggr_param_response_id")
//    private String aggrParamResId;

    @Column(name = "system_provider_id")
    private String systemProviderId;

    @Column(name = "system_id")
    private String systemId;

    @Column(name = "provider_id")
    private String providerId;

    @Column(name = "provider_name")
    private String providerName;

    @Column(name = "game_code")
    private String gameCode;

    @Column(name = "image_id")
    private Long imageId;

    @Column(name = "hash_value", unique = true)
    private String hashValue;

//    @Transient
//    public String encodeHashValue() {
//        return HashUtil.hashMd5(String.format("%s|%s|%s|%s|%s|%s|%s|%s",
//                this.aggrUrl,
//                this.standardEndpointId,
//                this.aggrEndpointId,
//                this.standardParamResId,
//                this.aggrParamResId,
//                this.systemId,
//                this.providerId,
//                this.gameCode
//        ));
//    }

    @Transient
    public String encodeHashValue() {
        return HashUtil.hashMd5(String.format("%s|%s|%s|%s|%s",
                this.aggrUrl,
                this.systemProviderId,
                this.systemId,
                this.providerId,
                this.gameCode
        ));
    }

}
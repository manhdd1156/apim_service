package co.arctx.apim.media.repository;

import co.arctx.apim.media.entity.Image;
import co.arctx.apim.media.entity.StandardImageAggrImage;
import co.arctx.apim.media.model.StandardAggrImgModel;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface StandardImageAggrImageRepository extends CommonRepository<StandardImageAggrImage, Long> {

    @Query("SELECT sai FROM StandardImageAggrImage sai WHERE sai.hashValue = :hashValue")
    List<StandardImageAggrImage> findByHashValue(String hashValue);

    @Query("SELECT new co.arctx.apim.media.model.StandardAggrImgModel(sai.id, sai.hashValue, i) " +
            "FROM StandardImageAggrImage sai " +
            "JOIN Image i ON i.id = sai.imageId " +
            "WHERE sai.hashValue IN :hashValues")
    List<StandardAggrImgModel> findByHashValue(Collection<String> hashValues);

    @Query("SELECT new co.arctx.apim.media.model.StandardAggrImgModel(sai.id, sai.hashValue, i,sai.aggrUrl, sai.gameCode) " +
            "FROM StandardImageAggrImage sai " +
            "JOIN Image i ON i.id = sai.imageId " +
            "WHERE sai.gameCode = :gameCode")
    StandardAggrImgModel findByGameCode(String gameCode);

//    @Query("SELECT i FROM Image i " +
//            "JOIN StandardImageAggrImage sai ON i.id = sai.imageId " +
//            "WHERE sai.hashValue IN :hashValues")
//    List<Image> findByHashValue(Collection<String> hashValues);
}

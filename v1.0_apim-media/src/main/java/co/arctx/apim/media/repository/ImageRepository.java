package co.arctx.apim.media.repository;

import co.arctx.apim.media.entity.Image;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
public interface ImageRepository extends CommonRepository<Image, Long> {

    Optional<Image> findByIod(String iod);

}

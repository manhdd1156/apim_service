package co.arctx.apim.media.transformer;

import co.arctx.apim.media.entity.Image;
import co.arctx.apim.media.model.ImageModel;
import co.arctx.core.utils.DateUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Component
public class ImageTransformer {

    @Value("${config.media.standard.pathMedia}")
    private String pathMedia;

    @Value("${config.media.standard.baseUrl}")
    private String standardBaseUrlMedia;

    public ImageModel toModel(Image entity) {
        ImageModel model = new ImageModel();

        model.setId(entity.getId());
        model.setName(entity.getName());
        model.setCreated(DateUtils.toMilli(entity.getCreatedTime()));
        model.setExtension(entity.getExtension());
        model.setCaption(entity.getCaption());
        model.setContentType(entity.getContentType());
        model.setPath(entity.getPath());
        model.setIod(entity.getIod());
        model.setSize(entity.getSize());
        model.setUploader(entity.getUploader());

        model.setUrl(String.format(standardBaseUrlMedia + pathMedia + "%s", entity.getIod()));
        return model;
    }

    public Image toEntity(MultipartFile multipartFile) {
        Image image = new Image();

        image.setCreatedTime(LocalDateTime.now());
        image.setUploader("uploader");
        image.setMultipartFile(multipartFile);
        image.setContentType(multipartFile.getContentType());
        image.setSize(multipartFile.getSize());
        image.setName(FilenameUtils.getBaseName(multipartFile.getOriginalFilename()));
        image.setOriginalName(FilenameUtils.getBaseName(multipartFile.getOriginalFilename()));
        image.setExtension(FilenameUtils.getExtension(multipartFile.getOriginalFilename()));

        return image;
    }

}


package co.arctx.apim.media.service;

import co.arctx.apim.media.entity.StandardImageAggrImage;
import co.arctx.apim.media.model.StandardAggrImgModel;
import co.arctx.apim.media.repository.StandardImageAggrImageRepository;
import co.arctx.core.common.CommonService;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class StandardAggrImageService extends CommonService<StandardImageAggrImage, Long, StandardImageAggrImageRepository> implements DisposableBean {

    protected StandardAggrImageService(StandardImageAggrImageRepository repo) {
        super(repo);
    }

    public List<StandardAggrImgModel> findByHashValue(Collection<String> hashValues) {
        return repo.findByHashValue(hashValues);
    }

    public StandardAggrImgModel findByGameCode(String gameCode) {
        return repo.findByGameCode(gameCode);
    }

    @Override
    protected String notFoundMessage() {
        return "Config standard aggr not found";
    }

    @Override
    public void destroy() throws Exception {

    }

}
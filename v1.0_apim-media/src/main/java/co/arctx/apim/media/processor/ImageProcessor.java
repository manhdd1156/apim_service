package co.arctx.apim.media.processor;

import co.arctx.apim.media.config.AppConfig;
import co.arctx.apim.media.entity.FileData;
import co.arctx.apim.media.entity.Image;
import co.arctx.apim.media.entity.QImage;
import co.arctx.apim.media.model.ImageModel;
import co.arctx.apim.media.service.CacheEvictService;
import co.arctx.apim.media.service.FileStorageService;
import co.arctx.apim.media.service.ImageService;
import co.arctx.apim.media.service.StaticFileStorageService;
import co.arctx.apim.media.transformer.ImageTransformer;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.exception.AccessDeniedException;
import co.arctx.core.exception.NoContentException;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.exception.UnexpectedException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import net.bytebuddy.utility.RandomString;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@Log4j2
@Component
public class ImageProcessor extends CommonProcessor<ImageService, QImage, ImageTransformer> implements DisposableBean {

    private final AppConfig appConfig;
    private final CacheEvictService cacheEvictService;
    private final ThumbnailTypeProperties thumbnailTypeProperties;
    private Preferences preferences = Preferences.userNodeForPackage(getClass());
    private static final String CREATE_NEW_FILES_UPLOAD_DIR_BY_DAY_KEY = "create-new-files-upload-dir-by-day-key";

    private ExecutorService executorService;
    private FileStorageService fileStorageService;

    protected ImageProcessor(ImageService service,
                             ImageTransformer transformer,
                             AppConfig appConfig,
                             CacheEvictService cacheEvictService,
                             ThumbnailTypeProperties thumbnailTypeProperties) {
        super(QImage.image, service, transformer);
        this.appConfig = appConfig;
        this.cacheEvictService = cacheEvictService;
        this.thumbnailTypeProperties = thumbnailTypeProperties;
    }

    @PostConstruct
    private void initial() throws UnexpectedException {
        executorService = Executors.newCachedThreadPool();
        resetFileStorageServiceByDay(createNewFilesUploadDirByDay(LocalDateTime.now()));
    }

    private void resetFileStorageServiceByDay(String filesUploadDirByDay) throws UnexpectedException {
        try {
            fileStorageService = new FileStorageService(String.join("/", appConfig.getFileUploadDir(), filesUploadDirByDay));
            preferences.put(CREATE_NEW_FILES_UPLOAD_DIR_BY_DAY_KEY, filesUploadDirByDay);
        } catch (Exception e) {
            throw UnexpectedException.of(null, "");
        }
    }

    private String createNewFilesUploadDirByDay(LocalDateTime date) {
        return date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public ImageModel upload(MultipartFile mFile) throws UnexpectedException, AccessDeniedException {
        Image image = transformer.toEntity(mFile);
        if (! image.isImage()) {
            throw AccessDeniedException.of("File not image");
        }

        String filesUploadDirByDay = createNewFilesUploadDirByDay(image.getCreatedTime());
        if (!filesUploadDirByDay.equals(preferences.get(CREATE_NEW_FILES_UPLOAD_DIR_BY_DAY_KEY, ""))) {
            resetFileStorageServiceByDay(filesUploadDirByDay);
        }

        try {
            Path path = fileStorageService.write(image.getMultipartFile());
            image.setIod(path.getFileName().toString());
            image.setPath(path.getParent().toString());
        } catch(Exception ex) {
            throw UnexpectedException.of(null, "Save image error");
        }

        service.save(image);

        executorService.execute(() -> this.resizeTypesProperties(image));

        return transformer.toModel(image);
    }

    public ImageModel uploadUrl(String url) throws UnexpectedException, IOException {
        URLConnection urlConnection = new URL(url).openConnection();
        urlConnection.setReadTimeout(5000);
        urlConnection.setConnectTimeout(5000);

        try (BufferedInputStream bis = new BufferedInputStream(urlConnection.getInputStream());
             ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            int result = bis.read();
            while(result != -1) {
                buf.write((byte) result);
                result = bis.read();
            }

            StringBuilder fileName = new StringBuilder();
            String ext = FilenameUtils.getExtension(url);

            if (StringUtils.isEmpty(ext)) {
                fileName.append(RandomString.make() + ".jpg");
            } else {
                fileName.append(RandomString.make() + "." + ext);
            }

            MultipartFile multipartFile = new MockMultipartFile(fileName.toString(), fileName.toString(), "image/" + ext, buf.toByteArray());
            return upload(multipartFile);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw UnexpectedException.of(null, "Save image error");
        }
    }

    @Transactional
    public List<ImageModel> uploadMultipleUrl(Collection<String> urls) {
        return urls.parallelStream()
                .map(e -> {
                    try {
                        return this.uploadUrl(e);
                    } catch (Exception ex) {
                        log.error(ex.getMessage(), ex);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private void resizeTypesProperties(Image image) {
        List<ThumbnailTypeProperties.Type> types = thumbnailTypeProperties.getTypes();
        if (CollectionUtils.isEmpty(types)) {
            log.debug("Thumbnail types is empty.");
            return;
        }

        for (ThumbnailTypeProperties.Type type : types) {
            String pathOfThumbnail = String.join("/", image.getPath(), type.toName());
            StaticFileStorageService.initFolder(pathOfThumbnail);

            try {
                Thumbnails.of(image.toPathOnDisk())
                        .size(type.getWidth(), type.getHeight()).outputQuality(1)
                        .toFile(String.join("/", pathOfThumbnail, image.getIod()));
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public FileData getBytes(String iod) throws NotFoundEntityException, NoContentException {
        Image entity = service.getByIodOrElseThrow(iod);
        byte[] bytes = this.retryReadByPath(0,entity.toPathOnDisk());
        cacheEvictService.addKeyToList(iod);

        return FileData.create()
                .bytes(bytes)
                .name(String.join(".", entity.getName(), entity.getExtension()))
                .contentType(entity.getContentType());
    }

    public FileData getBytesOfThumbnail(Integer width, Integer height, String iod) throws NotFoundEntityException, NoContentException {
        Image image = service.getByIodOrElseThrow(iod);
        byte[] bytes = this.retryReadByPath(0, image.toPathOfThumbnailOnDisk(ThumbnailTypeProperties.Type.of(width, height).toName()));
        cacheEvictService.addKeyToList(String.join("-", String.valueOf(width), String.valueOf(height), iod));

        return FileData.create()
                .bytes(bytes)
                .name(String.join(".", image.getName(), image.getExtension()))
                .contentType(image.getContentType());
    }

    private byte[] retryReadByPath(int count, String path) throws NoContentException {
        if (count > 5) {
            throw NoContentException.of("path", "Không thể đọc tập tin theo đường dẫn: " + path);
        }
        byte[] bytes = StaticFileStorageService.readByPath(path);
        if (bytes.length != 0) {
            return bytes;
        }
        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
        return retryReadByPath(++count, path);
    }

    @Override
    public void destroy() {
        executorService.shutdown();
    }

    @ConfigurationProperties(prefix = "thumbnail", ignoreUnknownFields = false)
    @Configuration
    public static class ThumbnailTypeProperties {

        @Getter
        @Setter
        private List<Type> types;

        @Getter @Setter
        @NoArgsConstructor
        public static class Type {
            private Integer width;
            private Integer height;

            private Type(Integer width, Integer height) {
                this.width = width;
                this.height = height;
            }

            private static Type of(Integer width, Integer height) {
                return new Type(width,height);
            }

            public String toName() {
                return String.join("x", String.valueOf(getWidth()), String.valueOf(getHeight()));
            }
        }
    }

}


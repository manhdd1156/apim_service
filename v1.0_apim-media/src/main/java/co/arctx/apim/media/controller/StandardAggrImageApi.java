package co.arctx.apim.media.controller;

import co.arctx.apim.media.model.CreateStandardAggrImageModel;
import co.arctx.apim.media.model.ImageModel;
import co.arctx.apim.media.processor.StandardAggrImageProcessor;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/standard-aggr-images")
@RequiredArgsConstructor
public class StandardAggrImageApi {

    private final StandardAggrImageProcessor processor;

    @PostMapping("/upload-url-multiple")
    @ApiOperation(value = "Upload multiple image.", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Upload multiple image successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public List<ImageModel> uploadMultipleUrl(@Valid @RequestBody CreateStandardAggrImageModel models) {
        return processor.uploadMultipleUrl(models.getImages());
    }

}
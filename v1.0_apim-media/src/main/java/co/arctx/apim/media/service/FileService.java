package co.arctx.apim.media.service;

import co.arctx.apim.media.entity.File;
import co.arctx.apim.media.entity.Thumbnail;
import co.arctx.apim.media.repository.FileRepository;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Service
public class FileService extends CommonService<File, Long, FileRepository> {

    private final ThumbnailService thumbnailService;

    protected FileService(FileRepository repo, ThumbnailService thumbnailService) {
        super(repo);
        this.thumbnailService = thumbnailService;
    }

    public File getByIodOrElseThrow(String iod) throws NotFoundEntityException {
        return repo.findByIod(iod)
                .orElseThrow(NotFoundEntityException.ofSupplier(notFoundMessage()));
    }

    public void removeByFolderId(Long folderId) {
        List<File> files = repo.findByFolderId(folderId);
        if (CollectionUtils.isEmpty(files)) {
            return;
        }

        for (File file : files) {
            this.remove(file);
        }
    }

    public void remove(File entity) {
        thumbnailService.removeByFileId(entity.getId());

        repo.delete(entity);

        StaticFileStorageService.deleteByPath(entity.getPath());
    }

    public void loadThumbnails(File entity) {
        List<Thumbnail> thumbnails = thumbnailService.findByFileId(entity.getId());
        entity.setThumbnails(thumbnails);
    }

    @Override
    protected String notFoundMessage() {
        return "File not found";
    }

}

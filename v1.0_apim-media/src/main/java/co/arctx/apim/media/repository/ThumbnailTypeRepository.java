package co.arctx.apim.media.repository;

import co.arctx.apim.media.entity.ThumbnailType;
import co.arctx.core.common.CommonRepository;

import java.util.Optional;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
public interface ThumbnailTypeRepository extends CommonRepository<ThumbnailType, Long> {

    Optional<ThumbnailType> findByName(String name);

    Optional<ThumbnailType> findByWidthAndHeight(Integer width, Integer height);

}

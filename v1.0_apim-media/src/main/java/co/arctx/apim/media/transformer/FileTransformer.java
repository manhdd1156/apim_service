package co.arctx.apim.media.transformer;

import co.arctx.apim.media.entity.File;
import co.arctx.apim.media.model.FileModel;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Component
public class FileTransformer {

    private final FolderTransformer folderTransformer;
    private final ThumbnailTransformer thumbnailTransformer;

    public FileTransformer(FolderTransformer folderTransformer, ThumbnailTransformer thumbnailTransformer) {
        this.folderTransformer = folderTransformer;
        this.thumbnailTransformer = thumbnailTransformer;
    }

    public FileModel toModel(File entity) {
        FileModel model = new FileModel();

        model.setId(entity.getId());
        model.setName(entity.getName());
        model.setCreated(entity.getCreatedTime().getTime());
        model.setCaption(entity.getCaption());
        model.setExtension(entity.getExtension());
        model.setContentType(entity.getContentType());
        model.setPath(entity.getPath());
        model.setIod(entity.getIod());
        model.setSize(entity.getSize());
        model.setUploader(entity.getUploader());

        if (! CollectionUtils.isEmpty(entity.getThumbnails())) {
            model.setThumbnails(thumbnailTransformer.toModels(entity.getThumbnails()));
        }

        if (entity.getFolder() != null) {
            model.setFolder(folderTransformer.toModel(entity.getFolder()));
        }

        return model;
    }

    public File toEntity(Long folderId, String uploader, MultipartFile multipartFile) {
        File file = new File();
        file.setCreatedTime(new Date());

        file.setFolderId(folderId);
        file.setName(FilenameUtils.getBaseName(multipartFile.getOriginalFilename()));

        file.setCaption("");
        file.setUploader(uploader);

        file.setMultipartFile(multipartFile);
        file.setContentType(multipartFile.getContentType());
        file.setSize(multipartFile.getSize());

        file.setOriginalName(FilenameUtils.getBaseName(multipartFile.getOriginalFilename()));
        file.setExtension(FilenameUtils.getExtension(multipartFile.getOriginalFilename()));

        return file;
    }

}

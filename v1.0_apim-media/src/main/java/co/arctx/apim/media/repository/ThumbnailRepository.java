package co.arctx.apim.media.repository;

import co.arctx.apim.media.entity.Thumbnail;
import co.arctx.core.common.CommonRepository;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
public interface ThumbnailRepository extends CommonRepository<Thumbnail, Long> {

    List<Thumbnail> findByFileId(Long fileId);

    Optional<Thumbnail> findByWidthAndHeightAndIod(Integer width, Integer height, String iod);

    List<Thumbnail> findByTypeId(Long typeId);

    @Transactional
    @Modifying
    void deleteByTypeId(Long id);

}


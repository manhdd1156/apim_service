package co.arctx.apim.media.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThumbnailModel {

    private Long id;
    private Long fileId;
    private Long thumbTypeId;
    private String extension;
    private String contentType;
    private String path;
    private String iod;
    private Integer width;
    private Integer height;
    private Long size/*, dimension*/;
    private Long created;

}
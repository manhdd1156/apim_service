package co.arctx.apim.media.model;

import co.arctx.apim.media.entity.Image;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StandardAggrImgModel {

    private Long id;
    private String hashValue;
    private Image image;

    private String aggUrl;
    private String gameCode;


    public StandardAggrImgModel(Long id, String hashValue, Image image) {
        this.id = id;
        this.hashValue = hashValue;
        this.image = image;
    }

    public StandardAggrImgModel(Long id, String hashValue, Image image, String aggUrl, String gameCode) {
        this.id = id;
        this.hashValue = hashValue;
        this.image = image;
        this.aggUrl = aggUrl;
        this.gameCode = gameCode;
    }
}

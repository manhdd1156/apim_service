package co.arctx.apim.media.controller;

import co.arctx.apim.media.entity.FileData;
import co.arctx.apim.media.model.FileModel;
import co.arctx.apim.media.processor.FileProcessor;
import co.arctx.core.common.CommonQuery;
import co.arctx.core.exception.DuplicatedException;
import co.arctx.core.exception.NoContentException;
import co.arctx.core.exception.NotFoundEntityException;
import io.swagger.annotations.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Size;

import static co.arctx.apim.media.utils.Constants.FILE_CACHE;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@RestController
public class FileApi {

    private final FileProcessor processor;

    public FileApi(FileProcessor processor) {
        this.processor = processor;
    }

    @PutMapping("/api/v1/files/{id}")
    @ApiOperation(value = "Update a File, return FileModel.", response = FileModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Update a File successfully"),
            @ApiResponse(code = 404, message = "Not found file"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FileModel update(@PathVariable Long id, @RequestBody UpdateModel model) throws NotFoundEntityException {
        return processor.update(id, model);
    }

    @GetMapping("/api/v1/files")
    @ApiOperation(value = "Find page of File, return Page<FileModel>.", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Query File successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public Page<FileModel> query(@ModelAttribute QueryModel model) {
        return processor.query(model);
    }

    @PutMapping("/api/v1/files/{id}/move/folders/{folderId}")
    @ApiOperation(value = "Move a File to Folder, return FileModel.", response = FileModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Move a File to Folder successfully"),
            @ApiResponse(code = 404, message = "Not found file"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FileModel move(@PathVariable Long id, @PathVariable Long folderId) throws NotFoundEntityException, DuplicatedException, DuplicatedException {
        return processor.move(id, folderId);
    }

    @DeleteMapping("/api/v1/files/{id}")
    @ApiOperation(value = "Delete a File, return FileModel.", response = FileModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Delete a File successfully"),
            @ApiResponse(code = 404, message = "Not found file"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FileModel remove(@PathVariable Long id) throws NotFoundEntityException {
        return processor.remove(id);
    }

    @GetMapping("/api/v1/files/{iod}")
    @Cacheable(value = FILE_CACHE, key = "#iod")
    @ApiOperation(value = "Find a File by Id on Desk, return Byte[].", response = Byte.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Find a File by Id on Desk successfully"),
            @ApiResponse(code = 404, message = "Not found file"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<?> getBytes(@PathVariable String iod, HttpServletResponse response) throws NotFoundEntityException, NoContentException {
        FileData fileData = processor.loadData(iod);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(fileData.getContentType()));
        response.setHeader("Content-Disposition", "attachment; filename=" + fileData.getName());

        return ResponseEntity.ok().headers(headers).body(fileData.getBytes());
    }

    @Getter
    @Setter
    @ApiModel
    public static class UpdateModel {

        @ApiModelProperty(value = "File Name")
        @Size(min=1, max = 250)
        private String name;

        @ApiModelProperty(value = "File Caption")
        @Size(min=1, max = 500)
        private String caption;

    }

    @Getter @Setter @ApiModel
    public static class QueryModel extends CommonQuery {

        @ApiModelProperty(value = "Folder Id")
        private Long folderId;

    }
}

package co.arctx.apim.media.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Entity
@Table
@Getter
@Setter
public class Folder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String caption;
    private Long parentId;
    private Boolean visible;
    private Date createdTime;

    private @Transient Folder parent;
    private @Transient List<Folder> children;

}

package co.arctx.apim.media.processor;

import co.arctx.apim.media.entity.FileData;
import co.arctx.apim.media.entity.QThumbnail;
import co.arctx.apim.media.entity.Thumbnail;
import co.arctx.apim.media.service.CacheEvictService;
import co.arctx.apim.media.service.StaticFileStorageService;
import co.arctx.apim.media.service.ThumbnailService;
import co.arctx.apim.media.transformer.ThumbnailTransformer;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.exception.NoContentException;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.stereotype.Component;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@Component
public class ThumbnailProcessor extends CommonProcessor<ThumbnailService, QThumbnail, ThumbnailTransformer> {

    private final CacheEvictService cacheEvictService;

    protected ThumbnailProcessor(ThumbnailService service, ThumbnailTransformer transformer, CacheEvictService cacheEvictService) {
        super(QThumbnail.thumbnail, service, transformer);
        this.cacheEvictService = cacheEvictService;
    }

    public FileData loadData(Integer width, Integer height, String iod) throws NotFoundEntityException, NoContentException {
        Thumbnail thumb = service.findByWidthAndHeightAndIod(width, height, iod);

        String path = String.join("/", thumb.getPath(), thumb.getIod());
        byte[] bytes = StaticFileStorageService.readByPath(path);

        if (bytes.length == 0) {
            throw NoContentException.of("width-height-iod", "Can not read file in: " + path);
        }

        cacheEvictService.addKeyToList(String.join("-", String.valueOf(width), String.valueOf(height), iod));

        return FileData.create()
                .bytes(bytes)
                .name(thumb.getIod())
                .contentType(thumb.getContentType());
    }
}


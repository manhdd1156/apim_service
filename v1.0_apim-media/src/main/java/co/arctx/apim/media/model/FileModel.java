package co.arctx.apim.media.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileModel {

    private Long id; //use for response

    @NotNull(message = "{file.folderId.not-null}")
    @Min(value = 1, message = "{file.folderId.min-value}")
    private Long folderId; //required

    @NotEmpty(message = "{file.uploader.not-null}")
    @Size(min=1, max = 50, message = "{file.uploader.size}")
    private String uploader; // required

    @NotEmpty(message = "{file.name.not-null}")
    @Size(min=1, max = 250, message = "{file.name.size}")
    private String name; // required

    @NotEmpty(message = "{file.caption.not-null}")
    @Size(min=1, max = 500, message = "{file.caption.size}")
    private String caption; // required

    private String path; //use for response
    private String iod; //use for response
    private String extension; //use for response
    private String contentType; //use for response
    private Long created;

    @JsonIgnore
    private FolderModel folder; //use for response

    @JsonIgnore
    private MultipartFile multipartFile; //use for upload

    private Long size; // use for response - unit: byte

    //@JsonIgnore
    private List<ThumbnailModel> thumbnails; //use for response

}

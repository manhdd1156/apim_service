package co.arctx.apim.media.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThumbnailTypeModel {

    private Long id; //use for response
    private String name; //use for response
    private Integer width;
    private Integer height;
    private Long created;

}

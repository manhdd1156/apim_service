package co.arctx.apim.media.repository;

import co.arctx.apim.media.entity.Folder;
import co.arctx.core.common.CommonRepository;

import java.util.List;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
public interface FolderRepository extends CommonRepository<Folder, Long> {

    List<Folder> findByParentIdOrderByCreatedTimeAsc(Long parentId);

    List<Folder> findByParentIdIsNullOrderByCreatedTimeAsc();

}
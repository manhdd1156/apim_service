package co.arctx.apim.media.controller;

import co.arctx.apim.media.entity.FileData;
import co.arctx.apim.media.model.ImageModel;
import co.arctx.apim.media.model.UploadUrlModel;
import co.arctx.apim.media.processor.ImageProcessor;
import co.arctx.core.exception.AccessDeniedException;
import co.arctx.core.exception.NoContentException;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.exception.UnexpectedException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static co.arctx.apim.media.utils.Constants.FILE_CACHE;
import static co.arctx.apim.media.utils.Constants.THUMBNAIL_CACHE;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/images")
public class ImageApi {

    private final ImageProcessor processor;

    @GetMapping("/{iod}")
    @Cacheable(value = FILE_CACHE, key = "#iod")
    @ApiOperation(value = "Find a image by Id on Desk, return Byte[].", response = Byte.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Find a image by Id on Desk successfully"),
            @ApiResponse(code = 404, message = "Not found image"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<?> getBytes(@PathVariable String iod) throws NotFoundEntityException, NoContentException {
        FileData fileData = processor.getBytes(iod);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, fileData.getContentType());
        return ResponseEntity.ok().headers(headers).body(fileData.getBytes());
    }

    @GetMapping("/{width}/{height}/{iod}")
    @Cacheable(value = THUMBNAIL_CACHE, key = "T(java.lang.String).format('%d-%d-%s', #width, #height, #iod)")
    @ApiOperation(value = "Find a image thumbnail, return Byte[].", response = Byte.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Find a image thumbnail successfully"),
            @ApiResponse(code = 404, message = "Not found image thumbnail"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<?> getBytesOfThumbnail(@PathVariable Integer width, @PathVariable Integer height, @PathVariable String iod, HttpServletResponse response)
            throws NotFoundEntityException, NoContentException {
        return toResponse(processor.getBytesOfThumbnail(width, height, iod), response);
    }

    private ResponseEntity<?> toResponse(FileData fileData, HttpServletResponse response) {
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.valueOf(fileData.getContentType()));
        response.setHeader("Content-Disposition", "attachment; filename=" + fileData.getName());

        return ResponseEntity.ok().headers(headers).body(fileData.getBytes());
    }

    @PostMapping("/upload")
    @ApiOperation(value = "Upload a image.", response = ImageModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Upload a image successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ImageModel upload(@RequestParam("file") MultipartFile file) throws UnexpectedException, AccessDeniedException {
        return processor.upload(file);
    }

    @PostMapping("/upload/multiple")
    @ApiOperation(value = "Upload multiple images.", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Upload multiple images successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public List<ImageModel> uploadMultipleFiles(@RequestParam("file") MultipartFile[] files) {
        return Arrays.asList(files).stream()
                .map(file -> {
                    try {
                        return upload(file);
                    } catch (Exception e) {
                        return null;
                    }
                })
                .collect(Collectors.toList());
    }

    @PostMapping("/upload-url")
    @ApiOperation(value = "Download a image.", response = ImageModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Download a image successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ImageModel uploadUrl(@RequestBody UploadUrlModel model) throws UnexpectedException, IOException {

        return processor.uploadUrl(model.getUrl());
    }

    @PostMapping("/upload-url-multiple")
    @ApiOperation(value = "Download multiple image.", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Download multiple image successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public List<ImageModel> uploadMultipleUrl(@RequestBody List<UploadUrlModel> models) {
        return models.stream()
                .map(url -> {
                    try {
                        return uploadUrl(url);
                    } catch (Exception e) {
                        return null;
                    }
                })
                .collect(Collectors.toList());
    }

}


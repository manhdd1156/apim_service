package co.arctx.apim.media.config;

import lombok.Getter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Component
public class AppConfig {

    @Getter
    @Value("${file.upload.dir}")
    private String fileUploadDir;

}

package co.arctx.apim.media.utils;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
public class Constants {

    private Constants() {
    }

    public static final int DEFAULT_OFFSET = 0;
    public static final int DEFAULT_LIMIT = 20;

    public static final String FILE_CACHE = "files";
    public static final String THUMBNAIL_CACHE = "thumbnails";

    public static final String THUMBNAIL_ERROR = "thumbnail-error";

}

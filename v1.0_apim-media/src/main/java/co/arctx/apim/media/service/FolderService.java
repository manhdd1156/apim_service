package co.arctx.apim.media.service;

import co.arctx.apim.media.entity.Folder;
import co.arctx.apim.media.repository.FolderRepository;
import co.arctx.core.common.CommonService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Service
public class FolderService extends CommonService<Folder, Long, FolderRepository> {

    protected FolderService(FolderRepository repo) {
        super(repo);
    }

    public List<Folder> findByParentId(Long parentId) {
        return repo.findByParentIdOrderByCreatedTimeAsc(parentId);
    }

    public List<Folder> findRoots() {
        return repo.findByParentIdIsNullOrderByCreatedTimeAsc();
    }

    @Override
    protected String notFoundMessage() {
        return "Folder not found";
    }
}

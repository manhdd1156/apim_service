package co.arctx.apim.media.processor;

import co.arctx.apim.media.entity.QStandardImageAggrImage;
import co.arctx.apim.media.entity.StandardImageAggrImage;
import co.arctx.apim.media.model.CreateStandardAggrImageModel.StandardAggrImageModel;
import co.arctx.apim.media.model.ImageModel;
import co.arctx.apim.media.model.StandardAggrImgModel;
import co.arctx.apim.media.service.StandardAggrImageService;
import co.arctx.apim.media.transformer.ImageTransformer;
import co.arctx.apim.media.transformer.StandardAggrImageTransformer;
import co.arctx.core.common.CommonProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Component
public class StandardAggrImageProcessor extends CommonProcessor<StandardAggrImageService, QStandardImageAggrImage, StandardAggrImageTransformer> {

    @Value("${config.media.standard.baseUrl}")
    private String standardBaseUrlMedia;

    @Value("${config.media.standard.pathMedia}")
    private String pathMedia;

    private final ImageProcessor imageProcessor;

    private final ImageTransformer imageTransformer;

    protected StandardAggrImageProcessor(StandardAggrImageService service, StandardAggrImageTransformer transformer, ImageProcessor imageProcessor, ImageTransformer imageTransformer) {
        super(QStandardImageAggrImage.standardImageAggrImage, service, transformer);
        this.imageProcessor = imageProcessor;
        this.imageTransformer = imageTransformer;
    }

    @Transactional
    public List<ImageModel> uploadMultipleUrl(List<StandardAggrImageModel> images) {
        List<ImageModel> finalResult = new ArrayList<ImageModel>();
        Map<String, StandardAggrImageModel> mapHashInput = images.stream().collect(Collectors.toMap(StandardAggrImageModel::getHashValue, Function.identity(), (standardAggrImageModel, standardAggrImageModel2) -> {return standardAggrImageModel;}));
        List<StandardAggrImgModel> existConfig = service.findByHashValue(mapHashInput.keySet());

        images.forEach(x->{
            AtomicBoolean flag = new AtomicBoolean(false);
            existConfig.forEach(x2-> {
                if(x.getHashValue().equals(x2.getHashValue())) {
                    finalResult.add(imageTransformer.toModel(x2.getImage()));
                    flag.set(true);
                }
            });
            if(!flag.get()) {
                finalResult.add(new ImageModel());
            }
        });
        // mapping url exist
        List<ImageModel> imageExists = existConfig.stream().map(e -> imageTransformer.toModel(e.getImage())).collect(Collectors.toList());
        log.info("Exists images: {}", imageExists);
        List<ImageModel> result = new LinkedList<>(imageExists);
        // upload and mapping url not exist
        existConfig.forEach(e -> mapHashInput.remove(e.getHashValue()));
        List<ImageModel> notExists = new LinkedList<>();
        List<StandardImageAggrImage> entities = new LinkedList<>();
        mapHashInput.forEach((k, v) -> {
            try {
                boolean nextStep = false;
                String aggUrl = v.getAggrUrl();
                if(aggUrl.trim().isEmpty()) {
                    StandardAggrImgModel imageSameGameCode = service.findByGameCode(v.getGameCode());
                    if(!Objects.isNull(imageSameGameCode)) {
                        aggUrl = imageSameGameCode.getAggUrl();
                    }else {
                        nextStep = true;
                    }
                }
                if(!nextStep) {
                    ImageModel imageModel = imageProcessor.uploadUrl(aggUrl);
                    String standardUrl = String.format(standardBaseUrlMedia + pathMedia + "%s", imageModel.getIod());
                    imageModel.setUrl(standardUrl);
                    StandardImageAggrImage entity = StandardImageAggrImage.builder()
                            .aggrUrl(v.getAggrUrl())
                            .standardUrl(standardUrl)
                            .systemProviderId(v.getSystemProviderId())
                            .systemId(v.getSystemId())
                            .providerId(v.getProviderId())
                            .providerName(v.getProviderName())
                            .gameCode(v.getGameCode())
                            .imageId(imageModel.getId())
                            .hashValue(k)
                            .build();
                    notExists.add(imageModel);
                    entities.add(entity);
                    for(int i = 0; i< images.size(); i++) {
                        if(k.equals(images.get(i).getHashValue()) && finalResult.get(i).getId()==null) {
                            finalResult.set(i,imageModel);
                        }
                    }
                }
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        });
        log.info("Not exists images: {}", imageExists);
        service.save(entities);
        result.addAll(notExists);
        return finalResult;
    }

}

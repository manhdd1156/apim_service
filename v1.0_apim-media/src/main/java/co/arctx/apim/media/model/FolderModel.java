package co.arctx.apim.media.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FolderModel {

    private Long id;

    @NotEmpty(message = "{folder.name.not-null}")
    @Size(min=1, max = 250, message = "{folder.name.size}")
    private String name;

    @NotEmpty(message = "{folder.caption.not-null}")
    @Size(min=1, max = 500, message = "{folder.caption.size}")
    private String caption;

    private Long parentId;
    private Boolean visible;
    private Long created;
    private FolderModel parent;
    private List<FolderModel> children;

}

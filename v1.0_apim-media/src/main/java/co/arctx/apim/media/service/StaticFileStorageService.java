package co.arctx.apim.media.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.Stream;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
public interface StaticFileStorageService {

    static byte[] readByPath(String path) {
        try {
            return Files.readAllBytes(Paths.get(path));
        } catch (Exception e) {
            return new byte[0];
        }
    }

    static boolean deleteByPath(String path) {
        try {
            return Files.deleteIfExists(Paths.get(path));
        } catch (Exception e) {
            return false;
        }
    }

    static void deleteFolder(String path) {
        try (Stream<Path> pathStream = Files.walk(Paths.get(path))) {
            pathStream.sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }

    static void initFolder(String pathFolder) {
        Path path = Paths.get(pathFolder);

        if (Files.exists(path)) {
            return;
        }

        try {
            Files.createDirectories(path);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}

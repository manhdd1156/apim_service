package co.arctx.apim.media.controller;

import co.arctx.apim.media.entity.FileData;
import co.arctx.apim.media.processor.ThumbnailProcessor;
import co.arctx.core.exception.NoContentException;
import co.arctx.core.exception.NotFoundEntityException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import static co.arctx.apim.media.utils.Constants.THUMBNAIL_CACHE;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@RestController
@RequestMapping("/api/v1/thumbnails")
public class ThumbnailApi {

    private final ThumbnailProcessor processor;

    public ThumbnailApi(ThumbnailProcessor processor) {
        this.processor = processor;
    }

    @GetMapping("/{width}/{height}/{iod}")
    @Cacheable(value = THUMBNAIL_CACHE, key = "T(java.lang.String).format('%d-%d-%s', #width, #height, #iod)")
    @ApiOperation(value = "Find a Thumbnail, return Byte[].", response = Byte.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Find a Thumbnail successfully"),
            @ApiResponse(code = 404, message = "Not found thumbnail"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ResponseEntity<?> getBytes(@PathVariable Integer width, @PathVariable Integer height, @PathVariable String iod,
                                     HttpServletResponse response) throws NotFoundEntityException, NoContentException {

        FileData fileData = processor.loadData(width, height, iod);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(fileData.getContentType()));
        response.setHeader("Content-Disposition", "attachment; filename=" + fileData.getName());

        return ResponseEntity.ok().headers(headers).body(fileData.getBytes());
    }
}
package co.arctx.apim.media.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import static co.arctx.apim.media.utils.Constants.FILE_CACHE;
import static co.arctx.apim.media.utils.Constants.THUMBNAIL_CACHE;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@Service
public class CacheEvictService {

    private LinkedHashSet<String> cachedKeys = new LinkedHashSet<>();

    public void addKeyToList(String key) {
        this.cachedKeys.add(key);
    }

    public List<String> findKeyByPartialKey(String partialKey) {
        List<String> foundKeys = new ArrayList<>();
        for (String cachedKey : this.cachedKeys) {
            if (cachedKey.contains(partialKey)) {
                foundKeys.add(cachedKey);
            }
        }
        return foundKeys;
    }

    @CacheEvict(value = {FILE_CACHE, THUMBNAIL_CACHE}, key = "#key")
    public void evict(String key) {
        this.cachedKeys.remove(key);
    }
}

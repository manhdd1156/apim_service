package co.arctx.apim.media.service;

import co.arctx.apim.media.entity.ThumbnailType;
import co.arctx.apim.media.repository.ThumbnailTypeRepository;
import co.arctx.core.common.CommonService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Service
public class ThumbnailTypeService extends CommonService<ThumbnailType, Long, ThumbnailTypeRepository> {

    protected ThumbnailTypeService(ThumbnailTypeRepository repo) {
        super(repo);
    }

    public Optional<ThumbnailType> find(ThumbnailType entity) {
        return repo.findByWidthAndHeight(entity.getWidth(), entity.getHeight());
    }

    @Override
    protected String notFoundMessage() {
        return null;
    }
}

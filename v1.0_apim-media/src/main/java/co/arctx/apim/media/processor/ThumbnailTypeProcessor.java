package co.arctx.apim.media.processor;

import co.arctx.apim.media.entity.File;
import co.arctx.apim.media.entity.QThumbnailType;
import co.arctx.apim.media.entity.Thumbnail;
import co.arctx.apim.media.entity.ThumbnailType;
import co.arctx.apim.media.model.CreateThumbnailTypeModel;
import co.arctx.apim.media.model.ThumbnailTypeModel;
import co.arctx.apim.media.model.UpdateThumbnailTypeModel;
import co.arctx.apim.media.service.FileService;
import co.arctx.apim.media.service.StaticFileStorageService;
import co.arctx.apim.media.service.ThumbnailService;
import co.arctx.apim.media.service.ThumbnailTypeService;
import co.arctx.apim.media.task.ThumbnailRecursiveAction;
import co.arctx.apim.media.transformer.ThumbnailTransformer;
import co.arctx.apim.media.transformer.ThumbnailTypeTransformer;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.exception.DuplicatedException;
import co.arctx.core.exception.NotFoundEntityException;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@Log4j2
@Component
public class ThumbnailTypeProcessor extends CommonProcessor<ThumbnailTypeService, QThumbnailType, ThumbnailTypeTransformer> {

    private final ExecutorService executor;
    private final ForkJoinPool forkJoinPool;
    private final FileService fileService;
    private final ThumbnailService thumbnailService;
    private final ThumbnailTransformer thumbnailTransformer;

    protected ThumbnailTypeProcessor(ThumbnailTypeService service, ThumbnailTypeTransformer transformer, FileService fileService, ThumbnailService thumbnailService, ThumbnailTransformer thumbnailTransformer) {
        super(QThumbnailType.thumbnailType, service, transformer);
        this.fileService = fileService;
        this.thumbnailService = thumbnailService;
        this.thumbnailTransformer = thumbnailTransformer;
        executor = Executors.newCachedThreadPool();
        forkJoinPool = ForkJoinPool.commonPool();
    }

    public ThumbnailTypeModel create(CreateThumbnailTypeModel model) throws DuplicatedException {
        ThumbnailType thumbnailType = transformer.toEntity(model);
        if (service.find(thumbnailType).isPresent()) {
            throw DuplicatedException.of("Id", "Thumbnail existed");
        }

        service.save(thumbnailType);

        try {
            StaticFileStorageService.initFolder(thumbnailType.getPath());

            executor.execute(() -> this.resizeAllFiles(thumbnailType));
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        return transformer.toModel(thumbnailType);
    }

    private void resizeAllFiles(ThumbnailType thumbType) {
        Page<File> page = fileService.query(Sort.Order.desc("createdTime"));
        while (page.hasContent()) {
            List<File> images = page.getContent().stream().filter(File::isImage).collect(Collectors.toList());
            forkJoinPool.invoke(new ThumbnailRecursiveAction(thumbnailService, thumbnailTransformer, thumbType, images));

            try {
                Thread.sleep(3000l);
            } catch (InterruptedException e) {
                log.error(e.getMessage(), e);
                Thread.currentThread().interrupt();
            }

            Pageable nextPageable = page.getPageable().next();
            page = fileService.query(nextPageable);
        }
    }

    public ThumbnailTypeModel update(Long id, UpdateThumbnailTypeModel model) throws DuplicatedException, NotFoundEntityException {
        ThumbnailType entity = transformer.toEntityUpdated(service.getOrElseThrow(id), model);
        if (service.find(entity).isPresent()) {
            throw DuplicatedException.of("Id", "Thumbnail đã tồn tại");
        }

        service.save(entity);

        executor.execute(() -> {
            StaticFileStorageService.deleteFolder(entity.getOldPath());
            StaticFileStorageService.initFolder(entity.getPath());

            this.updateThumbnail(entity);
        });

        return transformer.toModel(entity);
    }

    private void updateThumbnail(ThumbnailType type) {
        List<Thumbnail> thumbnails = thumbnailService.findByTypeId(type.getId());
        if (CollectionUtils.isEmpty(thumbnails)) {
            return;
        }

        thumbnails.stream().forEach(thumb -> {
            try {
                File file = fileService.getOrElseThrow(thumb.getFileId());

                thumb.setFilePath(file.getPath());
                thumb.setWidth(type.getWidth());
                thumb.setHeight(type.getHeight());
                thumb.setPath(type.getPath());

                thumbnailService.save(thumb);
                thumbnailService.makeOnDisk(thumb);
            } catch (NotFoundEntityException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

    public ThumbnailTypeModel delete(Long id) throws NotFoundEntityException {
        ThumbnailType entity = service.getOrElseThrow(id);

        thumbnailService.deleteByType(entity);
        service.delete(entity);

        return transformer.toModel(entity);
    }

    public List<ThumbnailTypeModel> getAll() {
        List<ThumbnailType> all = service.all(Sort.Order.desc("createdTime"));
        return transformer.toModels(all);
    }
}

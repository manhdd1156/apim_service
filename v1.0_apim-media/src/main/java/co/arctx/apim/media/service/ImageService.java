package co.arctx.apim.media.service;

import co.arctx.apim.media.entity.Image;
import co.arctx.apim.media.repository.ImageRepository;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.stereotype.Service;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Service
public class ImageService extends CommonService<Image, Long, ImageRepository> {

    protected ImageService(ImageRepository repo) {
        super(repo);
    }

    public Image getByIodOrElseThrow(String iod) throws NotFoundEntityException {
        return repo.findByIod(iod)
                .orElseThrow(NotFoundEntityException.ofSupplier(notFoundMessage()));
    }

    @Override
    protected String notFoundMessage() {
        return "Image not found";
    }

}

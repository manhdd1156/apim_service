package co.arctx.apim.media.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Getter
@Setter
@ApiModel
public class UpdateThumbnailTypeModel {

    @ApiModelProperty(value = "Width")
    @Min(value = 1)
    private Integer width;

    @ApiModelProperty(value = "Height")
    @Min(value = 1)
    private Integer height;

}
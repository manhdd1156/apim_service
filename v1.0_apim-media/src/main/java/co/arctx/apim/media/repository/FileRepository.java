package co.arctx.apim.media.repository;

import co.arctx.apim.media.entity.File;
import co.arctx.core.common.CommonRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
public interface FileRepository extends CommonRepository<File, Long> {

    Optional<File> findByIod(String ios);
    List<File> findByFolderId(Long folderId);

}

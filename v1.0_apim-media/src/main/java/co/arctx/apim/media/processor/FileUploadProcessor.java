package co.arctx.apim.media.processor;

import co.arctx.apim.media.config.AppConfig;
import co.arctx.apim.media.entity.*;
import co.arctx.apim.media.model.FileModel;
import co.arctx.apim.media.service.*;
import co.arctx.apim.media.transformer.FileTransformer;
import co.arctx.apim.media.transformer.ThumbnailTransformer;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.exception.UnexpectedException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.prefs.Preferences;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@Log4j2
@Component
public class FileUploadProcessor extends CommonProcessor<FileService, QFile, FileTransformer> implements DisposableBean {

    private final AppConfig appConfig;
    private final FolderService folderService;
    private final ThumbnailService thumbnailService;
    private final ThumbnailTransformer thumbnailTransformer;
    private final ThumbnailTypeService thumbnailTypeService;

    private Preferences preferences = Preferences.userNodeForPackage(getClass());
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private static final String CREATE_NEW_FILES_UPLOAD_DIR_BY_DAY_KEY = "create-new-files-upload-dir-by-day-key";

    private ExecutorService executorService;
    private FileStorageService fileStorageService;

    protected FileUploadProcessor(FileService service, FileTransformer transformer, AppConfig appConfig, FolderService folderService, ThumbnailTypeService thumbnailTypeService, ThumbnailService thumbnailService, ThumbnailTransformer thumbnailTransformer) {
        super(QFile.file, service, transformer);
        this.appConfig = appConfig;
        this.folderService = folderService;
        this.thumbnailTypeService = thumbnailTypeService;
        this.thumbnailService = thumbnailService;
        this.thumbnailTransformer = thumbnailTransformer;
    }

    @PostConstruct
    private void initial() throws UnexpectedException {
        executorService = Executors.newCachedThreadPool();
        resetFileStorageServiceByDay(createNewFilesUploadDirByDay(Calendar.getInstance().getTime()));
    }

    private void resetFileStorageServiceByDay(String filesUploadDirByDay) throws UnexpectedException {
        try {
            fileStorageService = new FileStorageService(String.join("/", appConfig.getFileUploadDir(), filesUploadDirByDay));
            preferences.put(CREATE_NEW_FILES_UPLOAD_DIR_BY_DAY_KEY, filesUploadDirByDay);
        } catch (Exception e) {
            throw UnexpectedException.of("","Create storage folder fail");
        }
    }

    private String createNewFilesUploadDirByDay(Date date) {
        return dateFormat.format(date);
    }

    public FileModel upload(Long folderId, String uploader, boolean isResize, MultipartFile mFile) throws UnexpectedException, NotFoundEntityException {
        Folder folder = folderService.getOrElseThrow(folderId);

        File file = transformer.toEntity(folderId, uploader, mFile);
        file.setFolder(folder);

        String filesUploadDirByDay = createNewFilesUploadDirByDay(file.getCreatedTime());
        if (!filesUploadDirByDay.equals(preferences.get(CREATE_NEW_FILES_UPLOAD_DIR_BY_DAY_KEY, ""))) {
            resetFileStorageServiceByDay(filesUploadDirByDay);
        }

        try {
            Path path = fileStorageService.write(file.getMultipartFile());
            file.setIod(path.getFileName().toString());
            file.setPath(path.toString());
        } catch(Exception ex) {
            throw UnexpectedException.of("","Create storage folder fail");
        }

        service.save(file);

        if (file.isImage() && isResize) {
            executorService.execute(() -> this.resizeAllTypes(file));
        }

        service.loadThumbnails(file);
        return transformer.toModel(file);
    }

    private void resizeAllTypes(File file) {
        List<ThumbnailType> thumbnailTypes = thumbnailTypeService.all(Sort.Order.desc("createdTime"));
        if (CollectionUtils.isEmpty(thumbnailTypes)) {
            return;
        }

        thumbnailTypes.stream().forEach(thumbType -> {
            Thumbnail thumb = thumbnailService.save(thumbnailTransformer.toEntity(file, thumbType));
            thumbnailService.makeOnDisk(thumb);
        });
    }

    @Override
    public void destroy() throws Exception {
        executorService.shutdown();
    }

}

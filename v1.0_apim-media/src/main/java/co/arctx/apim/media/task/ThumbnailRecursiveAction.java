package co.arctx.apim.media.task;

import co.arctx.apim.media.entity.File;
import co.arctx.apim.media.entity.Thumbnail;
import co.arctx.apim.media.entity.ThumbnailType;
import co.arctx.apim.media.service.ThumbnailService;
import co.arctx.apim.media.transformer.ThumbnailTransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
public class ThumbnailRecursiveAction extends RecursiveAction {

    private static final long serialVersionUID = -5148913041009844161L;

    private static final int THRESHOLD = 10;

    private transient ThumbnailService service;
    private transient ThumbnailTransformer transformer;
    private transient List<File> files;
    private ThumbnailType type;

    public ThumbnailRecursiveAction(ThumbnailService service, ThumbnailTransformer transformer, ThumbnailType type, List<File> files) {
        this.files = files;
        this.service = service;
        this.transformer = transformer;
        this.type = type;
    }

    @Override
    protected void compute() {
        if (files.size() > THRESHOLD) {
            ForkJoinTask.invokeAll(createSubTasks());
        } else {
            processing(files);
        }
    }

    private List<ThumbnailRecursiveAction> createSubTasks() {
        List<ThumbnailRecursiveAction> subTasks = new ArrayList<>();

        subTasks.add(new ThumbnailRecursiveAction(service, transformer, type, files.subList(0, files.size() / 2)));
        subTasks.add(new ThumbnailRecursiveAction(service, transformer, type, files.subList(files.size() / 2, files.size())));

        return subTasks;
    }

    private void processing(List<File> files) {
        files.stream().forEach(file -> {
            Thumbnail thumb = service.save(transformer.toEntity(file, type));
            service.makeOnDisk(thumb);
        });
    }

}

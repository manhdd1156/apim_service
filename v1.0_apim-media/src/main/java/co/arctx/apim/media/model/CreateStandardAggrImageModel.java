package co.arctx.apim.media.model;

import co.arctx.core.utils.HashUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateStandardAggrImageModel {

    @Valid
    @NotEmpty(message = "images not empty")
    public List<@NotNull(message = "element not null") StandardAggrImageModel> images;

    @Getter
    @Setter
    public static class StandardAggrImageModel {

        @NotNull(message = "aggrUrl not null")
        private String aggrUrl;
//        @NotBlank(message = "standardEndpointId not blank")
//        private String standardEndpointId;
//        @NotBlank(message = "aggrEndpointId not blank")
//        private String aggrEndpointId;
//        @NotBlank(message = "standardParamResId not blank")
//        private String standardParamResId;
//        @NotBlank(message = "aggrParamResId not blank")
//        private String aggrParamResId;

        @NotBlank(message = "systemProviderId not blank")
        private String systemProviderId;
        @NotBlank(message = "systemId not blank")
        private String systemId;
        @NotBlank(message = "providerId not blank")
        private String providerId;
        @NotBlank(message = "providerName not blank")
        private String providerName;
        @NotBlank(message = "gameCode not blank")
        private String gameCode;

//        @JsonIgnore
//        public String getHashValue() {
//            return HashUtil.hashMd5(String.format("%s|%s|%s|%s|%s|%s|%s|%s",
//                    this.aggrUrl,
//                    this.standardEndpointId,
//                    this.aggrEndpointId,
//                    this.standardParamResId,
//                    this.aggrParamResId,
//            ));
//        }
        @JsonIgnore
        public String getHashValue() {
            return HashUtil.hashMd5(String.format("%s|%s|%s|%s|%s|%s",
                    this.aggrUrl,
                    this.systemProviderId,
                    this.systemId,
                    this.providerId,
                    this.providerName,
                    this.gameCode
            ));
        }
    }

}

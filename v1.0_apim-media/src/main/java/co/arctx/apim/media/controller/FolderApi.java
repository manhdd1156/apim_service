package co.arctx.apim.media.controller;

import co.arctx.apim.media.model.FolderModel;
import co.arctx.apim.media.processor.FolderProcessor;
import co.arctx.core.exception.DuplicatedException;
import co.arctx.core.exception.InputInvalidException;
import co.arctx.core.exception.NotFoundEntityException;
import io.swagger.annotations.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@RestController
public class FolderApi {

    private final FolderProcessor processor;

    public FolderApi(FolderProcessor processor) {
        this.processor = processor;
    }

    @PostMapping("/api/v1/folders")
    @ApiOperation(value = "Create a folder, return FolderModel.", response = FolderModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Create a Folder successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FolderModel create(@RequestBody @Valid CreateModel model) throws NotFoundEntityException {
        return processor.create(model);
    }

    @PostMapping("/api/v1/{parentId}/folders")
    @ApiOperation(value = "Create a sub folder, return FolderModel.", response = FolderModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Create a sub Folder successfully"),
            @ApiResponse(code = 404, message = "Not found parent folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FolderModel create(@PathVariable Long parentId, @RequestBody @Valid FolderApi.CreateModel model, HttpServletRequest httpServletRequest) throws NotFoundEntityException {
        return processor.createSub(parentId, model);
    }

    @PutMapping("/api/v1/folders/{id}")
    @ApiOperation(value = "Update a Folder, return FolderModel.", response = FolderModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Update a Folder successfully"),
            @ApiResponse(code = 404, message = "Not found folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FolderModel update(@PathVariable Long id, @RequestBody @Valid UpdateModel model) throws NotFoundEntityException {
        return processor.update(id, model);
    }

    @PutMapping("/api/v1/folders/{id}/move/{parentId}")
    @ApiOperation(value = "Move a Folder, return FolderModel.", response = FolderModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Move a Folder successfully"),
            @ApiResponse(code = 404, message = "Not found folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FolderModel move(@PathVariable Long id, @PathVariable Long parentId) throws NotFoundEntityException, InputInvalidException, DuplicatedException {
        return processor.move(id, parentId);
    }

    @PutMapping("/api/v1/folders/{id}/change-to-root")
    @ApiOperation(value = "Change a Folder to root, return FolderModel.", response = FolderModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Change a Folder to root successfully"),
            @ApiResponse(code = 404, message = "Not found folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FolderModel changeToRoot(@PathVariable Long id) throws NotFoundEntityException {
        return processor.changeToRoot(id);
    }

    @PutMapping("/api/v1/folders/{id}/disable")
    @ApiOperation(value = "Disable a Folder, return FolderModel.", response = FolderModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Disable a Folder successfully"),
            @ApiResponse(code = 404, message = "Not found folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public FolderModel disable(@PathVariable Long id) throws NotFoundEntityException {
        return processor.disable(id);
    }

    @DeleteMapping("/api/v1/folders/{id}")
    @ApiOperation(value = "Delete a Folder, return FolderModel.", response = FolderModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Delete a Folder successfully"),
            @ApiResponse(code = 404, message = "Not found folder"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public void remove(@PathVariable Long id) {
        processor.remove(id);
    }

    @GetMapping("/api/v1/folders/all")
    @ApiOperation(value = "Find all folder root, return List<FolderModel>", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Find all folder root successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public List<FolderModel> getRoots() {
        return processor.getRoots();
    }

    @Getter
    @Setter
    @ApiModel
    public static class CreateModel {

        @ApiModelProperty(value = "Folder Name")
        @NotEmpty
        @Size(min=1, max = 250)
        private String name;

        @ApiModelProperty(value = "Folder Caption")
        @NotEmpty
        @Size(min=1, max = 500)
        private String caption;
    }

    @Getter @Setter @ApiModel
    public static class UpdateModel {

        @ApiModelProperty(value = "Folder Name")
        @Size(min=1, max = 250)
        private String name;

        @ApiModelProperty(value = "Folder Caption")
        @Size(min=1, max = 500)
        private String caption;
    }
}

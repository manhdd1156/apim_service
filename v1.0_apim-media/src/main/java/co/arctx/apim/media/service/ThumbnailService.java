package co.arctx.apim.media.service;

import co.arctx.apim.media.entity.Thumbnail;
import co.arctx.apim.media.entity.ThumbnailType;
import co.arctx.apim.media.repository.ThumbnailRepository;
import co.arctx.apim.media.utils.Constants;
import co.arctx.core.common.CommonService;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.utils.JsonMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.log4j.Log4j2;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.rmi.UnexpectedException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Log4j2
@Service
public class ThumbnailService extends CommonService<Thumbnail, Long, ThumbnailRepository> implements DisposableBean {

    private final FileStorageService<Thumbnail> errorStorageService;
    private final ScheduledExecutorService scheduledExecutorService;

    protected ThumbnailService(ThumbnailRepository repo) throws UnexpectedException {
        super(repo);
        errorStorageService = new FileStorageService<>(Constants.THUMBNAIL_ERROR, new TypeReference<Thumbnail>() {});
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleWithFixedDelay(this::reMakeOnDisk, 1, 5, TimeUnit.MINUTES);
    }

    public void removeByFileId(Long fileId) {
        List<Thumbnail> thumbnails = this.findByFileId(fileId);
        if (CollectionUtils.isEmpty(thumbnails)) {
            return;
        }

        thumbnails.stream().forEach(this::remove);
    }

    public List<Thumbnail> findByFileId(Long fileId) {
        return repo.findByFileId(fileId);
    }

    public List<Thumbnail> findByTypeId(Long typeId) {
        return repo.findByTypeId(typeId);
    }

    private void remove(Thumbnail entity) {
        repo.delete(entity);
        StaticFileStorageService.deleteByPath(String.join("/", entity.getPath(), entity.getIod()));
    }

    public Thumbnail findByWidthAndHeightAndIod(Integer width, Integer height, String iod) throws NotFoundEntityException {
        return repo.findByWidthAndHeightAndIod(width, height, iod)
                .orElseThrow(NotFoundEntityException.ofSupplier("width/height/iod", "Not found entity"));
    }

    public void deleteByType(ThumbnailType thumbnailType) {
        repo.deleteByTypeId(thumbnailType.getId());
    }

    private void reMakeOnDisk() {
        try {
            errorStorageService.list().forEach(this::reMakeOnDisk);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void reMakeOnDisk(Thumbnail thumbnail) {
        boolean isOk = makeOnDisk(thumbnail);
        if (isOk) {
            errorStorageService.delete(String.valueOf(thumbnail.getId()));
        }
    }

    public boolean makeOnDisk(Thumbnail thumb) {
        try {
            Thumbnails.of(thumb.getFilePath())
                    .size(thumb.getWidth(), thumb.getHeight()).outputQuality(1)
                    .toFile(thumb.createOutFilepath());

            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            onFailure(thumb);
            return false;
        }
    }

    private void onFailure(Thumbnail thumbnail) {
        try {
            Path write = errorStorageService.write(thumbnail, String.valueOf(thumbnail.getId()));
            log.info("Make on disk successful thumbnail: {}, path: {}", JsonMapper.write(thumbnail), write.toString());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            //TODO notification to administrator
        }
    }

    @Override
    protected String notFoundMessage() {
        return "Thumbnail not found";
    }

    @Override
    public void destroy() throws Exception {
        scheduledExecutorService.shutdown();
    }
}

package co.arctx.apim.media.transformer;

import co.arctx.apim.media.entity.File;
import co.arctx.apim.media.entity.Thumbnail;
import co.arctx.apim.media.entity.ThumbnailType;
import co.arctx.apim.media.model.ThumbnailModel;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Component
public class ThumbnailTransformer {

    public List<ThumbnailModel> toModels(List<Thumbnail> entities) {
        return entities.parallelStream()
                .map(converterModel)
                .collect(Collectors.toList());
    }

    private Function<Thumbnail, ThumbnailModel> converterModel = entity -> {
        ThumbnailModel model = new ThumbnailModel();

        model.setId(entity.getId());
        model.setFileId(entity.getFileId());
        model.setContentType(entity.getContentType());
        model.setExtension(entity.getExtension());
        model.setWidth(entity.getWidth());
        model.setHeight(entity.getHeight());
        model.setPath(entity.getPath());
        model.setIod(entity.getIod());
        model.setSize(entity.getSize());

        return model;
    };

    public Thumbnail toEntity(File file, ThumbnailType thumbType) {
        Thumbnail thumb = new Thumbnail();

        thumb.setCreatedTime(new Date());
        thumb.setFileId(file.getId());
        thumb.setFilePath(file.getPath());

        thumb.setExtension(file.getExtension());
        thumb.setContentType(file.getContentType());
        thumb.setIod(file.getIod());

        thumb.setTypeId(thumbType.getId());
        thumb.setWidth(thumbType.getWidth());
        thumb.setHeight(thumbType.getHeight());
        thumb.setPath(thumbType.getPath());

        return thumb;
    }

}

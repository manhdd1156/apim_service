package co.arctx.apim.media.controller;

import co.arctx.apim.media.model.CreateThumbnailTypeModel;
import co.arctx.apim.media.model.ThumbnailTypeModel;
import co.arctx.apim.media.model.UpdateThumbnailTypeModel;
import co.arctx.apim.media.processor.ThumbnailTypeProcessor;
import co.arctx.core.exception.DuplicatedException;
import co.arctx.core.exception.NotFoundEntityException;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */

@RequestMapping("/api/v1/thumbnails/types")
@RestController
public class ThumbnailTypeApi {

    private final ThumbnailTypeProcessor processor;

    public ThumbnailTypeApi(ThumbnailTypeProcessor processor) {
        this.processor = processor;
    }

    @PostMapping
    @ApiOperation(value = "Create a type of thumbnail, return ThumbnailTypeModel.", response = ThumbnailTypeModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Create a type of thumbnail successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ThumbnailTypeModel create(@RequestBody CreateThumbnailTypeModel model) throws DuplicatedException {
        return processor.create(model);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update a type of thumbnail, return ThumbnailTypeModel.", response = ThumbnailTypeModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Update a type of thumbnail successfully"),
            @ApiResponse(code = 404, message = "Not found thumbnail"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ThumbnailTypeModel update(@PathVariable Long id, @RequestBody UpdateThumbnailTypeModel model) throws DuplicatedException, NotFoundEntityException {
        return processor.update(id, model);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a type of thumbnail, return ThumbnailTypeModel.", response = ThumbnailTypeModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Delete a type of thumbnail successfully"),
            @ApiResponse(code = 404, message = "Not found thumbnail"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public ThumbnailTypeModel delete(@PathVariable Long id) throws NotFoundEntityException {
        return processor.delete(id);
    }

    @GetMapping("/all")
    @ApiOperation(value = "Find a type of thumbnail, return List<ThumbnailTypeModel>.", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Find a type of thumbnail successfully"),
            @ApiResponse(code = 500, message = "Server error")
    })
    public List<ThumbnailTypeModel> all() {
        return processor.getAll();
    }
}


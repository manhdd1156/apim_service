package co.arctx.apim.media.transformer;

import co.arctx.apim.media.controller.FolderApi;
import co.arctx.apim.media.entity.Folder;
import co.arctx.apim.media.model.FolderModel;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Component
public class FolderTransformer {

    public Folder toEntity(FolderApi.CreateModel model) {
        Folder entity = new Folder();

        entity.setCreatedTime(new Date());
        entity.setVisible(true); //defautl true when create
        entity.setName(model.getName());
        entity.setCaption(model.getCaption());

        return entity;
    }

    public FolderModel toModel(Folder folder) {
        return walk(folder);
    }

    private FolderModel walk(Folder entity) {
        FolderModel model = converter.apply(entity);

        if (entity.getParent() != null) {
            model.setParent(converter.apply(entity.getParent()));
        }

        if (CollectionUtils.isEmpty(entity.getChildren())) {
            return model;
        }

        List<FolderModel> children = new LinkedList<>();

        for (Folder childEntity : entity.getChildren()) {
            FolderModel childModel = walk(childEntity);
            children.add(childModel);
        }

        model.setChildren(children);

        return model;
    }

    private Function<Folder, FolderModel> converter = entity -> {
        FolderModel model = new FolderModel();

        model.setId(entity.getId());
        model.setCreated(entity.getCreatedTime().getTime());
        model.setName(entity.getName());
        model.setCaption(entity.getCaption());
        model.setParentId(entity.getParent() == null ? null : entity.getParent().getId());
        model.setVisible(entity.getVisible());

        return model;
    };

    public Folder toSubEntity(Long parentId, FolderApi.CreateModel model) {
        Folder entity = new Folder();

        entity.setCreatedTime(new Date());
        entity.setParentId(parentId);
        entity.setVisible(true);
        entity.setName(model.getName());
        entity.setCaption(model.getCaption());

        return entity;
    }

}

package co.arctx.apim.media.transformer;

import co.arctx.apim.media.config.AppConfig;
import co.arctx.apim.media.entity.ThumbnailType;
import co.arctx.apim.media.model.CreateThumbnailTypeModel;
import co.arctx.apim.media.model.ThumbnailTypeModel;
import co.arctx.apim.media.model.UpdateThumbnailTypeModel;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Component
public class ThumbnailTypeTransformer {

    private final AppConfig appConfig;

    public ThumbnailTypeTransformer(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    public List<ThumbnailTypeModel> toModels(List<ThumbnailType> entities) {
        return entities.parallelStream()
                .map(converterModel)
                .collect(Collectors.toList());
    }

    public ThumbnailTypeModel toModel(ThumbnailType entity) {
        return converterModel.apply(entity);
    }

    public ThumbnailType toEntity(CreateThumbnailTypeModel model) {
        ThumbnailType entity = new ThumbnailType();

        entity.setCreatedTime(new Date());
        entity.setWidth(model.getWidth());
        entity.setHeight(model.getHeight());
        entity.setName(entity.createDefaultName());
        entity.setPath(String.join("/", appConfig.getFileUploadDir(), entity.getName()));

        return entity;
    }

    public ThumbnailType toEntityUpdated(ThumbnailType entity, UpdateThumbnailTypeModel model) {
        if (model.getWidth() != null) {
            entity.setWidth(model.getWidth());
        }

        if (model.getHeight() != null) {
            entity.setHeight(model.getHeight());
        }

        entity.setName(entity.createDefaultName());
        entity.setPath(String.join("/", appConfig.getFileUploadDir(), entity.getName()));
        entity.setOldPath(entity.getPath());

        return entity;
    }

    private Function<ThumbnailType, ThumbnailTypeModel> converterModel = entity -> {
        ThumbnailTypeModel model = new ThumbnailTypeModel();

        model.setId(entity.getId());
        model.setName(entity.getName());
        model.setWidth(entity.getWidth());
        model.setHeight(entity.getHeight());
        model.setCreated(entity.getCreatedTime().getTime());

        return model;
    };

}


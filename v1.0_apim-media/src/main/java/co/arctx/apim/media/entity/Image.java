package co.arctx.apim.media.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Entity
@Table
@Getter
@Setter
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String caption;
    private String uploader;
    private String path;
    private String contentType;
    private String originalName;
    private String extension;
    private LocalDateTime createdTime;
    @Column(insertable = false, updatable = false)
    private LocalDateTime modifiedTime;
    private String iod;
    private Long size;

    private @Transient MultipartFile multipartFile;

    public boolean isImage() {
        return getContentType() != null && getContentType().split("/")[0].equals("image");
    }

    public String toPathOnDisk() {
        return String.join("/", getPath(), getIod());
    }

    public String toPathOfThumbnailOnDisk(String thumbnail) {
        return String.join("/", getPath(), thumbnail, getIod());
    }

}

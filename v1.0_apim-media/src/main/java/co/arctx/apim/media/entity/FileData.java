package co.arctx.apim.media.entity;

import lombok.Getter;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Getter
public class FileData {

    private byte[] bytes;
    private String name;
    private String contentType;

    private FileData() {}

    public static FileData create() {
        return new FileData();
    }

    public FileData bytes(byte[] bytes) {
        this.bytes = bytes;
        return this;
    }

    public FileData name(String name) {
        this.name = name;
        return this;
    }

    public FileData contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }
}


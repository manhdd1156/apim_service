package co.arctx.apim.media.processor;

import co.arctx.apim.media.controller.FolderApi;
import co.arctx.apim.media.entity.Folder;
import co.arctx.apim.media.entity.QFolder;
import co.arctx.apim.media.model.FolderModel;
import co.arctx.apim.media.service.FileService;
import co.arctx.apim.media.service.FolderService;
import co.arctx.apim.media.transformer.FolderTransformer;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.exception.DuplicatedException;
import co.arctx.core.exception.InputInvalidException;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@Component
public class FolderProcessor extends CommonProcessor<FolderService, QFolder, FolderTransformer> {

    private final FileService fileService;

    protected FolderProcessor(FolderService service, FolderTransformer transformer, FileService fileService) {
        super(QFolder.folder, service, transformer);
        this.fileService = fileService;
    }

    public FolderModel create(FolderApi.CreateModel model) throws NotFoundEntityException {
        Folder entity = service.save(transformer.toEntity(model));

        this.getParent(entity);
        this.getChildren(entity);

        return transformer.toModel(entity);
    }

    public FolderModel createSub(Long parentId, FolderApi.CreateModel model) throws NotFoundEntityException {
        service.getOrElseThrow(parentId, "Thư mục cha không tồn tại");

        Folder subFolder = transformer.toSubEntity(parentId, model);
        service.save(subFolder);
        return transformer.toModel(subFolder);
    }

    private void getChildren(Folder entity) {
        entity.setChildren(service.findByParentId(entity.getId()));

        for (Folder child : entity.getChildren()) {
            getChildren(child);
        }
    }

    private void getParent(Folder entity) throws NotFoundEntityException {
        if (entity.getParentId() == null) {
            return;
        }
        Folder parent = service.getOrElseThrow(entity.getParentId(), "Không tìm thấy thư mực cha có id: " + entity.getParentId());

        entity.setParent(parent);
    }

    public FolderModel update(Long id, FolderApi.UpdateModel model) throws NotFoundEntityException {
        Folder entity = service.getOrElseThrow(id);

        if (model.getName() != null) {
            entity.setName(model.getName());
        }
        if (model.getCaption() != null) {
            entity.setCaption(model.getCaption());
        }

        service.save(entity);
        return transformer.toModel(entity);
    }

    public FolderModel move(Long id, Long parentId) throws NotFoundEntityException, InputInvalidException, DuplicatedException {
        if (id.longValue() == parentId.longValue()) {
            throw new InputInvalidException("Id", "Không thể chuyển thư mục đến chính nó");
        }

        Folder fromFolder = service.getOrElseThrow(id);
        Folder toFolder = service.getOrElseThrow(parentId);

        validateFromFolderIsSubOfToFolder(fromFolder, toFolder);
        validateFromFolderIsParentOfToFolder(fromFolder, toFolder);

        fromFolder.setParentId(parentId);
        fromFolder.setParent(toFolder);

        service.save(fromFolder);
        return transformer.toModel(fromFolder);
    }

    private void validateFromFolderIsParentOfToFolder(Folder fromFolder, Folder toFolder) throws DuplicatedException {
        if (toFolder.getParentId() != null && toFolder.getParentId().longValue() == fromFolder.getId().longValue()) {
            throw DuplicatedException.of("ParentId", "Không thể chuyển thư mục cha tới thư mục con");
        }

        toFolder.setChildren(service.findByParentId(toFolder.getId()));
        for (Folder childFolder : toFolder.getChildren()) {
            validateFromFolderIsParentOfToFolder(toFolder, childFolder);
        }
    }

    private void validateFromFolderIsSubOfToFolder(Folder fromFolder, Folder toFolder) throws DuplicatedException {
        if (fromFolder.getParentId() != null && fromFolder.getParentId().longValue() == toFolder.getId().longValue()) {
            throw DuplicatedException.of("subFolderId", "Không thể chuyển thư mục con tới thư mục cha");
        }
    }

    public FolderModel changeToRoot(Long id) throws NotFoundEntityException {
        Folder folder = service.getOrElseThrow(id);
        folder.setParentId(null);

        service.save(folder);
        return transformer.toModel(folder);
    }

    public FolderModel disable(Long id) throws NotFoundEntityException {
        Folder folder = service.getOrElseThrow(id);
        this.getChildren(folder);
        this.disable(folder);
        return transformer.toModel(folder);
    }

    private void disable(Folder folder) {
        folder.setVisible(false);
        service.save(folder);

        if (CollectionUtils.isEmpty(folder.getChildren())) {
            return;
        }
        for (Folder f : folder.getChildren()) {
            disable(f);
        }
    }

    public void remove(Long id) {
        List<Folder> children = service.findByParentId(id);
        for (Folder child : children) {
            remove(child.getId());
        }

        fileService.removeByFolderId(id);

        service.deleteIfExisted(id);
    }

    public List<FolderModel> getRoots() {
        List<Folder> roots = service.findRoots();

        return roots.stream().map(f -> {
            this.getChildren(f);
            return transformer.toModel(f);
        }).collect(Collectors.toList());
    }
}

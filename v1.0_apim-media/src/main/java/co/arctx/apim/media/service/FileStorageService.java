package co.arctx.apim.media.service;

import co.arctx.core.utils.JsonMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.UnexpectedException;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
public class FileStorageService<E> implements StaticFileStorageService {

    private Logger logger;
    private String path;
    private Path rootLocation;
    private TypeReference<E> typeReference;

    public FileStorageService(final String folder) throws UnexpectedException {
        this.logger = LoggerFactory.getLogger(getClass());

        this.path = "data" + File.separatorChar
                + (folder.charAt(folder.length() - 1) == File.separatorChar ? folder : folder + File.separatorChar);

        createRootLocation(this.path);
    }

    public FileStorageService(final String folder, TypeReference<E> typeReference) throws UnexpectedException {
        this(folder);
        this.typeReference = typeReference;
    }

    private void createRootLocation(String path) throws UnexpectedException {
        rootLocation = Paths.get(path);

        initFolder(rootLocation);
    }

    public void initFolder(Path path) throws UnexpectedException {
        if (Files.exists(path)) {
            return;
        }

        try {
            Files.createDirectories(path);
        } catch (IOException e) {
            throw new UnexpectedException(e.getMessage());
        }
    }

    public Path write(E entity, String fileName) throws IOException {
        Path p = Paths.get(this.path + fileName);
        Files.write(p, JsonMapper.writeAsBytes(entity));
        return p;
    }

    public Stream<E> list() throws IOException {
        return Files.list(Paths.get(this.path))
                .filter(Files::isRegularFile)
                .map(this::read);
    }

    public E read(Path path) {
        try {
            byte[] result = Files.readAllBytes(path);
            E entity = JsonMapper.read(result, typeReference);
            if (entity == null) {
                throw new UnexpectedException("Cannot convert bytes to type " + typeReference);
            }
            return entity;
        } catch (Exception e) {
            logger.error("Read Data from path: " + path + " error. Exception: " + e.getMessage(), e);
            return null;
        }
    }

    public Path write(MultipartFile file) throws UnexpectedException {
        try {
            Path p = Paths.get(this.path + createFileName(file));

            Files.write(p, file.getBytes());

            return p;
        } catch (IOException e) {
            throw new UnexpectedException(e.getMessage());
        }
    }

    private String createFileName(MultipartFile file) {
        return String.join(".", UUID.randomUUID().toString(), getExtension(file));
    }

    private String getExtension(MultipartFile file) {
        return getExtension(file.getOriginalFilename());
    }

    private String getExtension(String fileName) {
        if (fileName == null) {
            return null;
        }

        return fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0
                ? fileName.substring(fileName.lastIndexOf(".")+1)
                : "";
    }

    public byte[] read(String fileName) {
        try {
            return Files.readAllBytes(Paths.get(path + fileName));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new byte[0];
        }
    }

    //@Override
    public boolean delete(String fileName) {
        try {
            Path p = Paths.get(this.path, fileName);
            return Files.deleteIfExists(p);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    public boolean isExisted(String fileName) {
        File[] files = new File(path).listFiles();
        return Arrays.stream(files).anyMatch(file -> file.getName().equals(fileName));
    }

    public Resource loadAsResource(String fileName) throws UnexpectedException {
        try {
            Resource resource = new UrlResource(load(fileName).toUri());
            return (resource.exists() || resource.isReadable()) ? resource : null;
        } catch (Exception e) {
            throw new UnexpectedException(e.getMessage());
        }
    }

    public Path load(String fileName) {
        return rootLocation.resolve(fileName);
    }

    public Stream<Path> loadAll() throws UnexpectedException {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(p -> !p.equals(this.rootLocation))
                    .map(this.rootLocation::relativize);
        } catch (IOException e) {
            throw new UnexpectedException(e.getMessage());
        }
    }

}

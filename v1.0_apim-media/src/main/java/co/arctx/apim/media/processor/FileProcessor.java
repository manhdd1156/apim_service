package co.arctx.apim.media.processor;

import co.arctx.apim.media.controller.FileApi;
import co.arctx.apim.media.entity.File;
import co.arctx.apim.media.entity.FileData;
import co.arctx.apim.media.entity.Folder;
import co.arctx.apim.media.entity.QFile;
import co.arctx.apim.media.model.FileModel;
import co.arctx.apim.media.service.CacheEvictService;
import co.arctx.apim.media.service.FileService;
import co.arctx.apim.media.service.FolderService;
import co.arctx.apim.media.service.StaticFileStorageService;
import co.arctx.apim.media.transformer.FileTransformer;
import co.arctx.core.common.CommonProcessor;
import co.arctx.core.exception.DuplicatedException;
import co.arctx.core.exception.NoContentException;
import co.arctx.core.exception.NotFoundEntityException;
import co.arctx.core.utils.QueryDslUtils;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;


/**
 * @author: Binh Nguyen
 * Jul 07, 2022
 */
@Component
public class FileProcessor extends CommonProcessor<FileService, QFile, FileTransformer> {

    private final FolderService folderService;
    private final CacheEvictService cacheEvictService;

    protected FileProcessor(FileService service, FileTransformer transformer, FolderService folderService, CacheEvictService cacheEvictService) {
        super(QFile.file, service, transformer);
        this.folderService = folderService;
        this.cacheEvictService = cacheEvictService;
    }

    public FileData loadData(String iod) throws NotFoundEntityException, NoContentException {
        File entity = service.getByIodOrElseThrow(iod);

        byte[] bytes = StaticFileStorageService.readByPath(entity.getPath());
        if (bytes.length == 0) {
            throw NoContentException.of("Iod", "Can not read file in path: " + entity.getPath());
        }

        cacheEvictService.addKeyToList(iod);

        return FileData.create()
                .bytes(bytes)
                .name(String.join(".", entity.getName(), entity.getExtension()))
                .contentType(entity.getContentType());
    }

    public FileModel update(Long id, FileApi.UpdateModel model) throws NotFoundEntityException {
        File file = service.getOrElseThrow(id);

        if (model.getName() != null) {
            file.setName(model.getName());
        }
        if (model.getCaption() != null) {
            file.setCaption(model.getCaption());
        }

        service.save(file);
        service.loadThumbnails(file);
        return transformer.toModel(file);
    }

    public Page<FileModel> query(FileApi.QueryModel model) {
        Page<File> page = service.query(buildCondition(model), model.getPage(), model.getSize(), QueryDslUtils.desc(Q.createdTime));
        return page.map(transformer::toModel);
    }

    private Predicate buildCondition(FileApi.QueryModel model) {
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(Q.folderId.eq(model.getFolderId()));
        condition.and(QueryDslUtils.buildKeywordPredicate(model.getKeyword(), Q.name, Q.caption, Q.uploader));
        return condition;
    }

    public FileModel move(Long fileId, Long folderId) throws NotFoundEntityException, DuplicatedException {
        File file = service.getOrElseThrow(fileId);
        Folder folder = folderService.getOrElseThrow(folderId);

        if (file.getFolderId().equals(folder.getId())) {
            throw DuplicatedException.of("FileId", "File existed");
        }

        file.setFolderId(folder.getId());
        file.setFolder(folder);

        service.save(file);
        service.loadThumbnails(file);
        return transformer.toModel(file);
    }

    public FileModel remove(Long id) throws NotFoundEntityException {
        File file = service.getOrElseThrow(id);

        service.remove(file);

        for (String cachedKey : cacheEvictService.findKeyByPartialKey(file.getIod())) {
            cacheEvictService.evict(cachedKey);
        }

        service.loadThumbnails(file);
        return transformer.toModel(file);
    }
}


package co.arctx.apim.media.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author: Binh Nguyen
 * Jul 06, 2022
 */
@Entity
@Table
@Getter
@Setter
public class ThumbnailType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String path;
    private Integer width;
    private Integer height;
    private Date createdTime;

    private @Transient String oldPath;

    public String createDefaultName() {
        StringBuilder thumbName = new StringBuilder();

        thumbName.append("thumbnail")
                .append("-")
                .append(this.getWidth())
                .append("x")
                .append(this.getHeight());

        return thumbName.toString();
    }

}

# Change log

## [tag 1.0.1]
- Add new variable List<String> sysIds; in RequestModel


## [tag 1.0.2]
- fix CommonExceptionHandlerAdvice
- fix RestTemplateException
- fix ErrorDetail
- fix ResponseModel
package co.arctx.core.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@ApiModel
@Getter
@Setter
public class CommonQuery {

    @ApiModelProperty(required = true, value = "Page index", example = "0")
    protected Integer page;

    @ApiModelProperty(required = true, value = "Number item per one page", example = "20")
    protected Integer size;

    protected Long fromDate, toDate;

    @ApiModelProperty(value = "Keyword for query")
    protected String keyword;
}

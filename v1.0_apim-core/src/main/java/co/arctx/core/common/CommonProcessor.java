package co.arctx.core.common;

import lombok.extern.slf4j.Slf4j;
import co.arctx.core.exception.AccessDeniedException;
import co.arctx.core.exception.DuplicatedException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@Slf4j
public abstract class CommonProcessor<S, QE, T> {

    protected final QE Q;

    protected final S service;
    protected final T transformer;
    private final Map<String, String> lockedKeys;

    protected CommonProcessor(QE q, S service, T transformer) {
        Q = q;
        this.service = service;
        this.transformer = transformer;
        lockedKeys = new ConcurrentHashMap<>();
    }

    protected void unlock(String key) {
        lockedKeys.remove(key);
    }

    protected void lock(String key) throws AccessDeniedException {
        tryLock(key, 0);
    }

    private void tryLock(String key, int count) throws AccessDeniedException {
        if (count > 300) {
            throw AccessDeniedException.of("System is processing your request.");
        }
        try {
            synchronized (this) {
                if (lockedKeys.containsKey(key)) {
                    throw DuplicatedException.of("Duplicated key: " + key);
                }
                lockedKeys.put(key, key);
            }
        } catch (DuplicatedException e) {
            log.error(e.getMessage());
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException ex) {
                log.error(ex.getMessage(), e);
            }
            tryLock(key, ++count);
        }
    }

}

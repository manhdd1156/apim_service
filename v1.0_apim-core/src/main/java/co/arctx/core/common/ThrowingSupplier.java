package co.arctx.core.common;

import java.util.function.Supplier;

/**
 * @author: Binh Nguyen
 * May 22, 2022
 */

@FunctionalInterface
public interface ThrowingSupplier<T, E extends Exception> {

    T get() throws E;

    static <T,  E extends Exception> Supplier<T> getThrow(ThrowingSupplier<T, E> throwingSupplier) {
        return () -> {
            try {
                return throwingSupplier.get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

}

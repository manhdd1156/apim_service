package co.arctx.core.common;

public enum DataType {
    BODY, FORM_DATA, URI_VARIABLE, PATH_VARIABLE
}
package co.arctx.core.common;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public class Constant {

    public static final int DEFAULT_OFFSET = 0;
    public static final int DEFAULT_LIMIT = 20;

    public static final int BAD_REQUEST = 400;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
    public static final int CONFLICT = 409;
    public static final int NO_CONTENT = 204;
    public static final int INTERNAL_SERVER_ERROR = 500;

    public static final String INTERNAL_ERROR_MESSAGE = "ERROR IS NOT SPECIFIED. PLEASE CONTACT SYSTEM ADMIN!";

    public static final String HTTP_METHOD_POST = "POST";
    public static final String HTTP_METHOD_GET = "GET";
    public static final String HTTP_METHOD_PUT = "PUT";

    public static final String DATA_DIR_ROOT = "data";

    public static final String PATTERN_EMAIL = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";
    public static final String PATTERN_PHONE_NUMBER = "^0([35789]|2[48])[0-9]{8}$";

    public static class ResponseCode {
        public static int OK = 200;
        public static int UNDEFINED = 999;
    }
}

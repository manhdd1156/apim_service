package co.arctx.core.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@NoRepositoryBean
public interface CommonRepository<E, ID> extends JpaRepository<E, ID>, QuerydslPredicateExecutor<E> {
}

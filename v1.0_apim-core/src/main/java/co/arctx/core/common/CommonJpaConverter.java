package co.arctx.core.common;

import com.fasterxml.jackson.core.type.TypeReference;
import co.arctx.core.utils.JsonMapper;

import javax.persistence.AttributeConverter;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public class CommonJpaConverter <E> implements AttributeConverter<E, String> {

    private TypeReference<E> typeReference;

    public CommonJpaConverter(TypeReference<E> typeReference) {
        this.typeReference = typeReference;
    }

    public String convertToDatabaseColumn(E meta) {
        return meta == null ? null : JsonMapper.write(meta);
    }

    public E convertToEntityAttribute(String dbData) {
        return dbData == null ? null : JsonMapper.read(dbData, this.typeReference);
    }
}

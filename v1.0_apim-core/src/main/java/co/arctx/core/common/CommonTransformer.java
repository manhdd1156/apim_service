package co.arctx.core.common;

import co.arctx.core.utils.DateUtils;

import java.time.LocalDateTime;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public interface CommonTransformer<E, M> {

    E toEntity(M model);
    M toModel(E entity);

    default Long map(LocalDateTime localDateTime) {

        return localDateTime == null ? null : DateUtils.toMilli(localDateTime);
    }

    default LocalDateTime map(Long value) {
        return LocalDateTime.now();
    }
}

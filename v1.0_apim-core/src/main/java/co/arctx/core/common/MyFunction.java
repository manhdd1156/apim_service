package co.arctx.core.common;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public interface MyFunction<T, R> {

    R apply(T t) throws Exception;
}

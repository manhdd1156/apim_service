package co.arctx.core.common;

import java.util.function.Function;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@FunctionalInterface
public interface ThrowingFunction<T, R, E extends Throwable> {
    R apply(T t) throws E;

    static <T, R, E extends Exception> Function<T, R> unChecked(ThrowingFunction<T, R, E> function) {
        return t -> {
            try {
                return function.apply(t);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        };
    }
}

package co.arctx.core.common;

import com.querydsl.core.types.Predicate;
import co.arctx.core.exception.NotFoundEntityException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import static co.arctx.core.common.Constant.DEFAULT_LIMIT;
import static co.arctx.core.common.Constant.DEFAULT_OFFSET;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public abstract class CommonService<E, ID, R extends CommonRepository<E, ID>> {

    protected final R repo;

    protected CommonService(R repo) {
        this.repo = repo;
    }

    public E save(E entity) {
        repo.save(entity);
        return entity;
    }

    public List<E> save(List<E> entities) {
        repo.saveAll(entities);
        return entities;
    }

    public Optional<E> get(ID id) {
        return repo.findById(id);
    }

    public E getOrElseThrow(ID id) throws NotFoundEntityException {
        return get(id)
                .orElseThrow(NotFoundEntityException.ofSupplier(notFoundMessage()));
    }

    public E getOrElseThrow(ID id, String message) throws NotFoundEntityException {
        return get(id)
                .orElseThrow(NotFoundEntityException.ofSupplier(message));
    }

    public E delete(E entity) {
        repo.delete(entity);
        return entity;
    }

    public E deleteIfExisted(ID id) {
        Optional<E> optional = get(id);
        if (!optional.isPresent()) {
            return null;
        }
        E entity = optional.get();
        return delete(entity);
    }

    public Page<E> query(Pageable pageable) {
        return repo.findAll(pageable);
    }

    public Page<E> query(Sort.Order...orders) {
        return query(null , orders);
    }

    public Page<E> query(Integer offset, Integer limit, Sort.Order...orders) {
        return query(null, offset, limit, orders);
    }

    public Page<E> query(Predicate condition, Sort.Order...orders) {
        return query(condition, null, null, orders);
    }

    public Page<E> query(Predicate condition, Integer offset, Integer limit, Sort.Order...orders) {
        if (offset == null) {
            offset = DEFAULT_OFFSET;
        }
        if (limit == null) {
            limit = DEFAULT_LIMIT;
        }
        Pageable pageable = PageRequest.of(offset, limit, Sort.by(orders));
        return condition != null ? repo.findAll(condition, pageable) : repo.findAll(pageable);
    }

    public List<E> all(Sort.Order...orders) {
        return all(null, orders);
    }

    public List<E> all(Predicate condition, Sort.Order...orders) {
        List<E> result = new LinkedList<>();

        Page<E> page = query(condition, orders);
        while (page.hasContent()) {
            result.addAll(page.getContent());

            Pageable nextPageable = page.getPageable().next();
            if (condition != null) {
                page = repo.findAll(condition, nextPageable);
            } else {
                page = repo.findAll(nextPageable);
            }
        }
        return result;
    }

    public E updateOnField(ID id, Consumer<E> fieldConsumer) throws NotFoundEntityException {
        E entity = getOrElseThrow(id);
        return update(entity, fieldConsumer);
    }
    public E update(E entity, Consumer<E> fieldConsumer) {
        fieldConsumer.accept(entity);
        save(entity);
        return entity;
    }

    protected abstract String notFoundMessage();
}

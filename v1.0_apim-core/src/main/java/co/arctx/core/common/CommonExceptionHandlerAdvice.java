package co.arctx.core.common;

import co.arctx.core.exception.*;
import co.arctx.core.model.ErrorDetail;
import co.arctx.core.model.ResponseModel;
import io.jsonwebtoken.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.MethodNotAllowedException;

import javax.persistence.NoResultException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static co.arctx.core.common.Constant.INTERNAL_ERROR_MESSAGE;
import static org.springframework.http.HttpStatus.*;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@ControllerAdvice
@Slf4j
public class CommonExceptionHandlerAdvice {

    @ExceptionHandler(RestTemplateException.class)
    public ResponseEntity<?> RestTemplate(RestTemplateException e) {
        return ResponseEntity.status(e.getStatus()).body(e.getErrorDetail());
    }

    @ExceptionHandler(MethodNotAllowedException.class)
    public ResponseEntity<?> methodNotAllowed(MethodNotAllowedException e) {
        ErrorDetail errorDetail = ErrorDetail.create(null, e.getMessage());
        return ResponseEntity.status(METHOD_NOT_ALLOWED).body(errorDetail);
    }

    @ExceptionHandler(HttpMediaTypeException.class)
    public ResponseEntity<?> unsupportedMediaType(HttpMediaTypeException e) {
        ErrorDetail errorDetail = ErrorDetail.create(null, e.getMessage());
        return ResponseEntity.status(UNSUPPORTED_MEDIA_TYPE).body(errorDetail);
    }

    @ExceptionHandler(TimeoutException.class)
    public ResponseEntity<?> requestTimeout(TimeoutException e) {
        ErrorDetail errorDetail = ErrorDetail.create(null, e.getMessage());
        return ResponseEntity.status(REQUEST_TIMEOUT).body(errorDetail);
    }

    @ExceptionHandler(NoResultException.class)
    public ResponseEntity<?> noResultException(NoResultException exception) {
        exception.printStackTrace();
        ErrorDetail body = ErrorDetail.create(null, exception.getMessage());
        return ResponseEntity.status(NOT_FOUND).body(body);
    }

    @ExceptionHandler(NotFoundEntityException.class)
    @ResponseStatus(NOT_FOUND)
    public @ResponseBody ErrorDetail notFound(NotFoundEntityException e) {
        return ErrorDetail.create(e.getErrorField(), e.getMessage());
    }

    @ExceptionHandler(DuplicatedException.class)
    @ResponseStatus(CONFLICT)
    public @ResponseBody ErrorDetail conflict(DuplicatedException e) {
        return ErrorDetail.create(e.getErrorField(), e.getMessage());
    }
//
//    @ExceptionHandler(AccessDeniedException.class)
//    @ResponseStatus(value = FORBIDDEN)
//    public @ResponseBody ErrorDetail forbidden(AccessDeniedException e) {
//        e.printStackTrace();
//        return ErrorDetail.create(e.getErrorField(), e.getMessage());
//    }

    @ExceptionHandler(NoContentException.class)
    @ResponseStatus(value = NO_CONTENT)
    public @ResponseBody ErrorDetail noContent(NoContentException e) {
        return ErrorDetail.create(e.getErrorField(), e.getMessage());
    }

    @ExceptionHandler(InputInvalidException.class)
    @ResponseStatus(NOT_ACCEPTABLE)
    public @ResponseBody ErrorDetail notAcceptable(InputInvalidException e) {
        return ErrorDetail.create(e.getErrorField(), e.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public @ResponseBody ErrorDetail methodArgumentNotValid(MethodArgumentNotValidException e) {
        String field = null, message = null;
        if (e.getBindingResult() != null) {
            BindingResult bindingResult = e.getBindingResult();
            message = bindingResult.getFieldError().getDefaultMessage();
            field = bindingResult.getFieldError().getField();
        }
        log.error("Message: {}, Field: {}", message, field);
        return ErrorDetail.create(field, message);
    }

    @ExceptionHandler(RuntimeException.class)
    public @ResponseBody ResponseEntity<List<ErrorDetail>> internalUnchecked(RuntimeException e) {
        log.error(e.getMessage(), e);
        Exception cause = (Exception) e.getCause();
        if (cause instanceof ValidateException) {
            ValidateException validateException = (ValidateException) cause;
            return new ResponseEntity<>(validateException.getErrorDetails(), BAD_REQUEST);
        }
        String error = cause != null ? cause.getMessage() : INTERNAL_ERROR_MESSAGE;
        return new ResponseEntity<>(Collections.singletonList(ErrorDetail.create(null, error)), INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public @ResponseBody ErrorDetail internalChecked(Exception e) {
        log.error(e.getMessage(), e);
        return ErrorDetail.create(null, INTERNAL_ERROR_MESSAGE);
    }

    @ExceptionHandler(ValidateException.class)
    @ResponseStatus(BAD_REQUEST)
    public @ResponseBody List<ErrorDetail> validateException(ValidateException e) {
        return e.getErrorDetails();
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<?> handleUnAuthorizedException(UnauthorizedException exception) {
//        Map<String,Object> body = new HashMap<>();
//        body.put("timestamp", LocalDateTime.now());
//        body.put("message",exception.getMessage());

        ErrorDetail body = ErrorDetail.create(exception.getErrorField(), exception.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(body);

    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> acessDeniedException(org.springframework.security.access.AccessDeniedException exception) {


        ErrorDetail body = ErrorDetail.create(null, exception.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(body);

    }

    @ExceptionHandler(SignatureException.class)
    public ResponseEntity<?> signatureException(SignatureException exception) {
        ErrorDetail body = ErrorDetail.create(null, exception.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(body);
    }


}

package co.arctx.core.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import co.arctx.core.entity.UserAuthentication;
import co.arctx.core.exception.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@Slf4j
@Component
public class JwtReader {

    private final JwtConfig jwtConfig;

    public JwtReader(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    public UserAuthentication getAuth(HttpServletRequest request) throws AccessDeniedException {
        return of(request)
                .orElseThrow(AccessDeniedException.ofSupplier("token", "You do not have permissions to access this function"));
    }

    public Optional<UserAuthentication> of(HttpServletRequest request) {
        return Optional.ofNullable(read(request));
    }

    public UserAuthentication read(HttpServletRequest request) {
        String bearerToken = request.getHeader(jwtConfig.getHeader());

        if(Objects.isNull(bearerToken) || !bearerToken.startsWith(jwtConfig.getPrefix())) {
            return null;
        }

        return parse(bearerToken);
    }

    public UserAuthentication parse(String bearerToken) {
        String token = bearerToken.replace(jwtConfig.getPrefix(), "");

        log.info("Token: {}", token);
        Claims claims = Jwts.parser()
                .setSigningKey(jwtConfig.getSecret().getBytes())
                .parseClaimsJws(token)
                .getBody();

        String username = claims.getSubject();
        log.info("Username: {}, Expire time: {}", username, claims.getExpiration());
        if (username == null) {
            return null;
        }

        String vndIdToken = claims.get("token", String.class);
        String fullName = claims.get("fullName", String.class);
        String email = claims.get("email", String.class);
        String phone = claims.get("phone", String.class);

        List<String> authorities = claims.get("authorities", List.class);
        List<String> groupNames = claims.get("groupNames", List.class);

        return new UserAuthentication(username, fullName, email, phone,
                authorities, groupNames, vndIdToken);
    }

    public Authentication loadSpringAuth(HttpServletRequest request) {
        UserAuthentication ua = read(request);

        if (Objects.isNull(ua)) {
            return null;
        }

        return new UsernamePasswordAuthenticationToken(ua.getUserName(), null, ua.getAuthorities().stream()
                .map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
    }
}

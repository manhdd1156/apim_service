package co.arctx.core.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Logger;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import co.arctx.core.exception.*;
import co.arctx.core.utils.JsonMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static co.arctx.core.common.Constant.*;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@Configuration
public class FeignConfig {
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return new FeignErrorDecoder();
    }
}

@Slf4j
class FeignErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String s, Response response) {
        String error = null, errorField = null;
        ObjectMapper mapper = new ObjectMapper();
        ExceptionMessage exceptionMessage = null;
        try {
            exceptionMessage = mapper.readValue(response.body().asInputStream(), ExceptionMessage.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (exceptionMessage != null) {
            errorField = exceptionMessage.getErrorField();
            error = exceptionMessage.getErrorMessage();
        }
        switch (response.status()) {
            case BAD_REQUEST:
                return InputInvalidException.of(errorField, error);

            case NOT_FOUND:
                return NotFoundEntityException.of(errorField, error);

            case FORBIDDEN:
                return AccessDeniedException.of(errorField, error);

            case CONFLICT:
                return DuplicatedException.of(errorField, error);

            case NO_CONTENT:
                return NoContentException.of(errorField, error);

            case INTERNAL_SERVER_ERROR:
            default:
                return UnexpectedException.of(error == null ? "Unknown" : error);
        }
    }
}

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
class ExceptionMessage {
    int status;
    @JsonProperty(value = "errorMessage")
    String errorMessage;
    String errorField;
    String timestamp, details;
}

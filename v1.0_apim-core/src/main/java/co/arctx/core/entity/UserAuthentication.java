package co.arctx.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@Getter
@Setter
@NoArgsConstructor
public class UserAuthentication {

    private String userName, fullName, email, phone;
    private List<String> authorities, groupNames;
    private String token;

    public UserAuthentication(String userName, String fullName, String email, String phone, List<String> authorities, List<String> groupNames, String token) {
        this.userName = userName;
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.authorities = authorities;
        this.groupNames = groupNames;
        this.token = token;
    }
}

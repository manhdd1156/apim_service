package co.arctx.core.model;

import lombok.*;
import co.arctx.core.common.EnumText;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class EnumDefaultModel<K, V> {

    private final K code;
    private final V text;

    public static <K, V> EnumDefaultModel<K, V> of(K code, V text) {
        return new EnumDefaultModel<>(code, text);
    }

    public static <E extends Enum & EnumText> EnumDefaultModel<String, String> of(E e) {
        return of(e.name(), e.getText());
    }
}

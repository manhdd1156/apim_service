package co.arctx.core.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import co.arctx.core.common.Constant.ResponseCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class ResponseModel<T> {

    private int code;
    private String description;
    private Object a;
    private T data;

    private ResponseModel(int code, String description, T data) {
        this.code = code;
        this.description = description;
        this.data = data;
    }

    public static <T> ResponseModel<T> ok(T data, String description) {
        return new ResponseModel<>(ResponseCode.OK, description, data);
    }

    public static <T> ResponseModel<T> error(int code, String description, T data) {
        return new ResponseModel<>(code, description, data);
    }

    public static <T> ResponseModel<T> error(int code, String description) {
        return error(code, description, null);
    }

    @Override
    public String toString() {
        return "ResponseModel{" +
                "code=" + code +
                ", description='" + description + '\'' +
                ", data=" + data +
                '}';
    }
}

package co.arctx.core.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: Binh Nguyen
 * May 22, 2022
 */

@Getter @Setter
public class RequestModel {

    private Map<String, Object> headerParams = new HashMap<>();
    private Map<String, Object> body = new HashMap<>();



}


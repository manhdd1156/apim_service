package co.arctx.core.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

@Getter
@Setter
@NoArgsConstructor
public class ErrorDetail {

    private String errorField, errorMessage;
    private Integer code;

    private ErrorDetail(String errorField, String errorMessage, Integer code) {
        this.errorField = errorField;
        this.errorMessage = errorMessage;
        this.code = code;
    }

    public static ErrorDetail create(String errorField, String error) {
        return new ErrorDetail(errorField, error,null );
    }
    public static ErrorDetail create(String errorField, String error,Integer code) {
        return new ErrorDetail(errorField, error, code);
    }

//    @Override
//    public String toString() {
//        return "ErrorDetail{" +
//                "errorField:'" + errorField + '\'' +
//                ", errorMessage:'" + errorMessage + '\'' +
//                '}';
//    }

    @Override
    public String toString() {
        return "ErrorDetail{" +
                "errorField='" + errorField + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}

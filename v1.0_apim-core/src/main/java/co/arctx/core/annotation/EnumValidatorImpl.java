package co.arctx.core.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public class EnumValidatorImpl implements ConstraintValidator<EnumValidator, String> {

    private Enum[] enumArr;

    @Override
    public void initialize(EnumValidator constraintAnnotation) {
        Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClass();
        enumArr = enumClass.getEnumConstants();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Arrays.stream(enumArr)
                .filter(e -> e.name().equals(value))
                .findAny().isPresent();
    }
}

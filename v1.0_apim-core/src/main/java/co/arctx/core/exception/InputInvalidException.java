package co.arctx.core.exception;

import lombok.Getter;

/**
 * @author: Binh Nguyen
 * May 17, 2022
 */

public class InputInvalidException extends Exception {

    private static final long serialVersionUID = 3040416053638688121L;

    @Getter
    private String errorField;

    public static InputInvalidException of(String message) {
        return of(null, message);
    }

    public static InputInvalidException of(String errorField, String message) {
        return new InputInvalidException(errorField, message);
    }

    public InputInvalidException(String errorField, String message) {
        super(message);
        this.errorField = errorField;
    }
}

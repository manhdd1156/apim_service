package co.arctx.core.exception;

import lombok.Getter;

import java.util.function.Supplier;

/**
 * @author: Binh Nguyen
 * May 17, 2022
 */

public class AccessDeniedException extends Exception{

    private static final long serialVersionUID = -3700782354442444944L;

    @Getter
    private String errorField;

    private AccessDeniedException(String errorField, String message) {
        super(message);
        this.errorField = errorField;
    }

    public static AccessDeniedException of(String errorField, String message) {
        return new AccessDeniedException(errorField, message);
    }

    public static AccessDeniedException of(String message) {
        return of(null, message);
    }

    public static Supplier<AccessDeniedException> ofSupplier(String errorField, String message) {
        return () -> of(errorField, message);
    }

    public static Supplier<AccessDeniedException> ofSupplier(String message) {
        return ofSupplier(null,message);
    }
}

package co.arctx.core.exception;

import lombok.Getter;

import java.util.function.Supplier;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public class UserNotAllowAttemptException extends Exception {

    @Getter
    private String errorField;

    private UserNotAllowAttemptException(String errorField, String message) {
        super(message);
        this.errorField = errorField;
    }

    public static UserNotAllowAttemptException of(String message) {
        return of(null, message);
    }

    public static UserNotAllowAttemptException of(String errorField, String message) {
        return new UserNotAllowAttemptException(errorField, message);
    }

    public static Supplier<UserNotAllowAttemptException> ofSupplier(String message) {
        return ofSupplier(null, message);
    }

    public static Supplier<UserNotAllowAttemptException> ofSupplier(String errorField, String message) {
        return () -> of(errorField, message);
    }

    public static void throwNow(String errorField, String message) throws UserNotAllowAttemptException {
        throw of(errorField, message);
    }
}
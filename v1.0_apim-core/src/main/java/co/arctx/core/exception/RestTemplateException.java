package co.arctx.core.exception;

import co.arctx.core.model.ErrorDetail;
import co.arctx.core.model.ResponseModel;
import lombok.Getter;
import lombok.Setter;

import java.util.function.Supplier;

/**
 * @author: Si Dang
 * Jul 01, 2022
 */

public class RestTemplateException extends RuntimeException{
    private static final long serialVersionUID = -718639735490655218L;

 @Getter
    @Setter
    private String errorDetail;
 @Setter
    @Getter
    private int status;
 public RestTemplateException(String errorDetail, int status)
 {
     super(errorDetail);
     this.errorDetail = errorDetail;
     this.status =status;
 }


}

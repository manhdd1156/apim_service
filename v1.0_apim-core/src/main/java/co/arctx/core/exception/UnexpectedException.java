package co.arctx.core.exception;

import lombok.Getter;

import java.util.function.Supplier;

/**
 * @author: Binh Nguyen
 * May 17, 2022
 */

public class UnexpectedException extends Exception {

    private static final long serialVersionUID = -7033879774043509730L;

    @Getter
    private String errorField;

    private UnexpectedException(String errorField, String message) {
        super(message);
        this.errorField = errorField;
    }

    public static UnexpectedException of(String message) {
        return of(null, message);
    }

    public static UnexpectedException of(String errorField, String message) {
        return new UnexpectedException(errorField, message);
    }

    public static Supplier<UnexpectedException> ofSupplier(String message) {
        return ofSupplier(null, message);
    }

    public static Supplier<UnexpectedException> ofSupplier(String errorField, String message) {
        return () -> of(errorField, message);
    }

    public static void throwNow(String errorField, String message) throws UnexpectedException {
        throw of(errorField, message);
    }
}


package co.arctx.core.exception;

import lombok.Getter;
import co.arctx.core.model.ErrorDetail;

import java.util.List;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public class ValidateException extends Exception {

    @Getter
    private List<ErrorDetail> errorDetails;

    public ValidateException(List<ErrorDetail> errorDetails) {
        this(null, errorDetails);
    }

    public ValidateException(String msg, List<ErrorDetail> errorDetails) {
        super(msg);
        this.errorDetails = errorDetails;
    }

}

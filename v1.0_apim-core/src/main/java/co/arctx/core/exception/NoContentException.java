package co.arctx.core.exception;

import lombok.Getter;

import java.util.function.Supplier;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public class NoContentException extends Exception {

    @Getter
    private String errorField;

    private NoContentException(String errorField, String message) {
        super(message);
        this.errorField = errorField;
    }

    public static NoContentException of(String errorField, String message) {
        return new NoContentException(errorField, message);
    }

    public static NoContentException of(String message) {
        return of(null, message);
    }

    public Supplier<NoContentException> ofSupplier(String errorField, String message) {
        return () -> of(errorField, message);
    }

    public Supplier<NoContentException> ofSupplier(String message) {
        return ofSupplier(null,message);
    }
}


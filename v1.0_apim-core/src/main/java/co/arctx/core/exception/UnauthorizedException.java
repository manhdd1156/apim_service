package co.arctx.core.exception;

import lombok.Getter;

import java.util.function.Supplier;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public class UnauthorizedException extends Exception {

    @Getter
    private String errorField;

    public UnauthorizedException(String errorField, String message) {
        super(message);
        this.errorField = errorField;
    }

    public static UnauthorizedException of(String message) {
        return of(null, message);
    }

    public static UnauthorizedException of(String errorField, String message) {
        return new UnauthorizedException(errorField, message);
    }

    public static Supplier<UnauthorizedException> ofSupplier(String message) {
        return ofSupplier(null, message);
    }

    public static Supplier<UnauthorizedException> ofSupplier(String errorField, String message) {
        return () -> of(errorField, message);
    }

    public static void throwNow(String errorField, String message) throws UnauthorizedException {
        throw of(errorField, message);
    }
}

package co.arctx.core.utils;

import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author: Duy Manh
 * Jane 24, 2022
 */

public class DynamicLinkUtils {

    public static String getCompleteRequestUri(String endpointUri, String apiPrefix) {
        Pattern regex = Pattern.compile("^[0-9]");


        apiPrefix = !regex.matcher(apiPrefix).find() ? apiPrefix : "http://"+apiPrefix;
        String formattedUri = (endpointUri.startsWith("/")) ? endpointUri : "/" + endpointUri;
        formattedUri = (formattedUri.startsWith(apiPrefix)) ? formattedUri : apiPrefix + formattedUri;

        if(formattedUri.contains("/?&"))
            return formattedUri;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(formattedUri);

        return builder.toUriString();
    }

}

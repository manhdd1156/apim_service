package co.arctx.core.utils;


import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author: Duy Manh
 * July 25, 2022
 */
@Service
@Slf4j
public class HashUtil {

    public static String hashMd5(String format) {
        String hashMd5 = null;
        try {
            hashMd5 = DatatypeConverter.printHexBinary(
                    MessageDigest.getInstance("MD5").digest(format.getBytes("UTF-8"))).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return hashMd5;
    }

}

package co.arctx.core.utils;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import co.arctx.core.common.CommonQuery;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import static co.arctx.core.common.Constant.DEFAULT_LIMIT;
import static co.arctx.core.common.Constant.DEFAULT_OFFSET;

/**
 * @author: Binh Nguyen
 * May 21, 2022
 */

public class QueryDslUtils {

    private QueryDslUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static String getFieldName(Path<?> fieldExpression) {
        return fieldExpression.getMetadata().getName();
    }

    public static Sort.Order desc(Path<?> fieldExpression) {
        return Sort.Order.desc(getFieldName(fieldExpression));
    }

    public static Sort.Order asc(Path<?> fieldExpression) {
        return Sort.Order.asc(getFieldName(fieldExpression));
    }

    public static Predicate buildDateRangePredicate(Long fromDate, Long toDate, DateTimePath<LocalDateTime> fieldExpression) {
        if (fromDate == null && toDate == null) {
            return null;
        }

        if (fromDate != null && toDate != null) {
            return fieldExpression.between(DateUtils.atStart(fromDate), DateUtils.atEnd(toDate));
        }
        if (toDate != null) {
            return fieldExpression.loe(DateUtils.atEnd(toDate));
        }
        if (fromDate != null) {
            return fieldExpression.goe(DateUtils.atStart(fromDate));
        }

        return null;
    }

    public static Predicate buildKeywordPredicate(String keyword, StringPath... fieldExpressions) {
        if (StringUtils.isEmpty(keyword)) {
            return null;
        }
        BooleanBuilder subCondition = new BooleanBuilder();
        for (StringPath fieldExpression : fieldExpressions) {
            subCondition.or(fieldExpression.contains(keyword));
        }
        return subCondition;
    }

    public static Predicate buildNumberRangePredicate(BigDecimal min, BigDecimal max, NumberPath<BigDecimal> fieldExpression) {
        if (min == null && max == null) {
            return null;
        }

        if (min != null && max != null) {
            return fieldExpression.between(min, max);
        }

        if (max != null) {
            return fieldExpression.loe(max);
        }

        if (min != null) {
            return fieldExpression.goe(min);
        }

        return null;
    }

    public static void pageQueryStandard(CommonQuery query) {
        if (Objects.isNull(query.getPage())) {
            query.setPage(DEFAULT_OFFSET);
        }
        if (Objects.isNull(query.getSize())) {
            query.setSize(DEFAULT_LIMIT);
        }
    }
}
